###############################################
# TestBsigConfigAlfa.py:
#
# Unit Test for BsigConfigAlfa.py
#

import sys
sys.path.insert(0, 'C:\\Users\\erware\\BSIG\\BSIGMapper')

import unittest
from BsigConfigAlfa import BsigConfigAlfa

#Path to application directory
mainDirPath = 'C:/Users/erware/BSIG/'
#Path to BsigConfig
bsigConfigPath = mainDirPath + 'BsigMapper/BsigConfig/'
#Path to Input Directory 
bsigInputPath = mainDirPath + 'BsigMapper/1MdlcInput/'
#Path to Output Directory 
bsigOutputPath = mainDirPath + 'BsigMapper/Output/'

        
###############################################
# Unit Tests
class TestBsigConfigAlfa(unittest.TestCase):

    # Unit Tests: read_config_file()
    #Success Case
    def test_read_config_file_pass(self):

        testObj = BsigConfigAlfa()
        configInfo = testObj.read_config_file( bsigInputPath + "MdlcRequest.json" )
        
        #Test Model
        self.assertEqual(configInfo['Model'], 'FairValue')
        #Test Destination Path
        self.assertEqual(configInfo['DestPath'], 'C:/Users/erware/BSIG/BsigMapper/Output/')
        #Test Quarter and Year
        self.assertEqual(configInfo['Quarters'][0]['Year'], '2017')
        self.assertEqual(configInfo['Quarters'][0]['Quarter'], 'Q4')
        
    #Fail Case Case
    def test_read_config_file_fail(self):

        testObj = BsigConfigAlfa()
        configInfo = testObj.read_config_file( bsigInputPath + "MdlcRequest.json" )
        
        #Test Model
        self.assertNotEqual(configInfo['Model'], 'Test_Model')
        #Test Destination Path
        self.assertNotEqual(configInfo['DestPath'], 'Test_Out')
        #Test Quarter and Year
        self.assertNotEqual(configInfo['Quarters'][0]['Year'], '1901')
        self.assertNotEqual(configInfo['Quarters'][0]['Quarter'], 'Q1')

    #Wrong File Case
    def test_read_config_file_null(self):

        testObj = BsigConfigAlfa()
        self.assertIsNone(testObj.read_config_file( bsigInputPath + "null.json" ))


    # Unit Tests: get_inforce_file_list()
    #Success Case
    def test_get_inforce_file_list_pass(self):
        
        #Invalid input test
        testObj = BsigConfigAlfa()
        
        fileList = testObj.get_inforce_file_list('FairValue')
        
        self.assertEqual(fileList['files'][0]['Name'], 'P65_DIASPL_CQIssues')
        self.assertEqual(fileList['files'][0]['QueryPath'], 'QueryLibrary/P65_DIASPL_CQIssues.sql')
    
    #Invalid input case
    def test_get_inforce_file_list_invalid(self):
        #Invalid input test
        testObj = BsigConfigAlfa()
        self.assertEqual(testObj.get_inforce_file_list('invalid'), None)




if __name__ == '__main__':
    unittest.main()


