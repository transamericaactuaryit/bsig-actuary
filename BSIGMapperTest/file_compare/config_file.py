# -*- coding: utf-8 -*-
###############################################
# config_file.py:
#
# Configuration file for BSIG infoce creation application contains
# static configuration information specific to the deployment
# enviornment.

import os
import sys
sys.path.insert(0, '.')

################################################
# This is the location which will appear on the report, 
# not the actual location of the files. 
reported_current_location = '5 - BSI - Actuarial/Sample AIL2 Files/Ail2 files for Q218'
reported_future_location = '5 - BSI - Actuarial/BSIG_Test_Data/'
reported_output_location = '5 - BSI - Actuarial/BSIG_Test_Data'
#

# Enable to TRUE for normalized data set comparison
normalize_data = True
# Enable to TRUE to sort data sets by policy number
sort_data = True
# Enable to send email notification after file send
send_email = False
# Attach summary file to email
send_summary = False
# Enable rounding to 2 decimal places on column sums
round_sum = True
# Drop Duplicates from future data set
drop_future_duplicates = True
# Drop Duplicates from current future set
drop_current_duplicates = True
# Enable filed by field compare for current state vs future state
field_compare_current_vs_future = True
# Enable miss list analysis
get_miss_list = True
# Enable column compare option
column_compare = False
# Print out a normalized future state inforce file
produce_normalized_inforce = False

# Name of column to compare
column_name = 'ck_Char1'

# List of emails to send results email to
email_recipients = '<usr>@transamerica.com'

# Output file directory
output_directory = 'C:/<path to dir>/file_compare/output'

# Future State Input Directory
input_future_directory = 'C:/<path to dir>/file_compare/input_BSIG'

# Current State Input Directory
current_input_directory = 'C:/<path to dir>/file_compare/input_Q218'

# Column List File, file which is used to describe all the known columns in the inforce file
inforce_file_info = 'inforce_file_info.csv'

#Command to execute on spark
#spark2-submit --num-executors 2 --executor-cores 2 --driver-memory 10G --executor-memory 2G --conf spark.port.maxRetries=50 --conf "spark.yarn.executor.memoryOverhead=4048" BsigMapper.py