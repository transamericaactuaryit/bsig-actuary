# -*- coding: utf-8 -*-
"""
Created on Mon Jun  4 14:45:50 2018

@author: BSIG Group
"""


import pandas as pd
import numpy as np
import time
import datetime
import smtplib
import csv
import re
import os
import sys

from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart 

from config_file import email_recipients, send_summary
from config_file import current_input_directory, input_future_directory, output_directory
from config_file import reported_current_location, reported_future_location, reported_output_location
from config_file import normalize_data, sort_data, send_email, inforce_file_info, round_sum
from config_file import column_compare, field_compare_current_vs_future, get_miss_list
from config_file import drop_future_duplicates, drop_current_duplicates, produce_normalized_inforce


#allows for very large CSV files
csv.field_size_limit(1000000000)

# Executes file compare tool on list of files
def execute_file_compare_list():

    current_state_dir = current_input_directory + os.sep
    future_state_dir = input_future_directory + os.sep
    
    output_file_dir = output_directory
    extention_type = '.csv'
    
    # Get execution time stamp
    run_time_stamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M-%S')   
    
    #output_files_path = output_file_dir + os.sep
    summary_file_path = str(output_file_dir + os.sep + 'result_summary_' + run_time_stamp + '.csv')
    run_log_file_path = output_file_dir + os.sep + 'run_log.txt'
    file_not_found_list = []
    file_found_list = []
    
    # Output File Setup
    # Create output directory if it is not already created
    try: 
        os.makedirs(output_file_dir)
    except OSError:
        if not os.path.isdir(output_file_dir):
            raise
    
    run_log = open(run_log_file_path, 'w')
    summary_file = open(summary_file_path, 'w')
    run_date = get_string_time(time.time())
    
    # Build list of future state files
    future_state_files = build_input_file_list(future_state_dir)
    
    if len(future_state_files) == 0:
        run_log.write('ERROR: Future State File Directory is empty')
        run_log.close()
        summary_file.write('ERROR: Future State File Directory is empty')
        summary_file.close()
        return 
    
    # Build list of current state files
    current_state_files = build_input_file_list(current_state_dir)
    
    if len(current_state_files) == 0:
        run_log.write('ERROR: Current State File Directory is empty')
        run_log.close()
        summary_file.wrote('ERROR: Current State File Directory is empty')
        summary_file.close()
        return
        
    # Exceute file compare for all files in directory
    for file in future_state_files:
        
        run_log.write('Execute File Compare: ' + file + '\n' )
        
        future_state_file_path = future_state_files[file]
        
        if 'PolicyID' in file:
            run_log.write('Skipping policyID file\n' )
            run_log.write('-------\n')
            continue
        
        try:
            current_state_file_path = current_state_files[file]  
            file_found_list.append(future_state_file_path)
            
            output_file_path = output_directory + os.sep + file + '_output' + extention_type
            output_miss_list_path = output_directory + os.sep + file + '_miss_list' + extention_type
        except:
            run_log.write('Warning: Future state file match could not be established for file: ' + future_state_file_path + '\n')
            file_not_found_list.append(future_state_file_path)
            run_log.write('-------\n')
            continue

        try:
            
            run_log.write('Future State File: ' + future_state_file_path + '\n')
            run_log.write('Current State File: ' + future_state_file_path + '\n')
            
            # Execute the file compare analysis            
            start_compare = time.time()
            field_compare_result = file_compare(file, future_state_file_path, current_state_file_path, output_file_path, 
                                                output_miss_list_path, reported_current_location, reported_future_location)
            stop_compare = time.time()
            
            if field_compare_result ==  None:
                run_log.write('Warning field compare result is empty, Future: ' + future_state_file_path + ', Current: ' + current_state_file_path + '\n')
                run_log.write('------\n')
                continue
            
            if len(field_compare_result) == 0:
                run_log.write('Warning field compare result is empty, Future: ' + future_state_file_path + ', Current: ' + current_state_file_path + '\n')
                run_log.write('------\n')
                continue
            
            # Update result summary
            write_summary_result(file, future_state_file_path, current_state_file_path, run_date, 
                                 field_compare_result, summary_file, reported_current_location, reported_future_location)
            run_log.write('Finished File Compare, Run Time: ' + str(compute_delta_time(start_compare, stop_compare)) + '\n') 
            
            print('Finished File Compare, File: ' + future_state_file_path)
        except Exception as e:
            run_log.write('Warning: Exception caught in file compare, Future state: ' + future_state_file_path + ', Current State: ' + current_state_file_path + '\n')
            print(e)            
            run_log.write('-------\n')
            continue
        
        run_log.write('-------\n')

    summary_file.close()   
    run_log.close()
     
        
    # Create and send an email notification
    if send_email == True:
        send_email_data(email_recipients, file, reported_current_location, reported_future_location, 
                        future_state_file_path, current_state_file_path, reported_output_location, summary_file_path)
        print('Send email notification')    
        
    

# Main portion of the file compare tool
def file_compare(file_name, future_state_file_path, current_state_file_path, output_file_path, 
                 output_miss_list_path, reported_current_location, reported_future_location):

    column_miss_list = {}
    policy_drop_list = {}
    invalid_lines_list = {}
    non_modified_metrics = {}
    start_time = time.time()
    
    print('Files to compare')
    print('Future State: ' + future_state_file_path)
    print('Current State: ' + current_state_file_path)

    print("This is a file compare Tool, start time: " + get_string_time(start_time))
    
    # Read in inforce file data file and extract column list
    inforce_column_list = import_inforce_file_column_list('input_column_key.csv')
    
    # Read in files to be compared
    # Import current state inforce/AIL2 file
    print('Start current state inforce file import')
    start_mark = time.time()
    current_state, current_state_invalid_lines_list = import_inforce_file(current_state_file_path, inforce_column_list, True, 'current')
    #current_state, current_state_invalid_lines_list = import_inforce_file(current_state_file_path, inforce_column_list, False, 'current')
    # Check if there is future state data present
    
    if len(current_state) == 0:
        write_missing_data_file(future_state_file_path, current_state_file_path, output_file_path)
        return None
        
    invalid_lines_list['current_state_bad_ascii'] = current_state_invalid_lines_list['invalid_ascii']
    invalid_lines_list['column_mismatch_current'] = current_state_invalid_lines_list['column_mismatch_list']

    stop_mark = time.time()
    print('current state inforce file consumed, total time: ' + str(compute_delta_time(start_mark, stop_mark)))
    
    # get inforce column list
    #inforce_column_list = list(current_state.columns.get_values())
    
    # Import future state inforce/AIL2 file
    print('Start future state inforce file import')
    start_mark = time.time()
    future_state, future_state_invalid_lines_list = import_inforce_file(future_state_file_path, inforce_column_list, True, 'future')
    #future_state, future_state_invalid_lines_list = import_inforce_file(future_state_file_path, inforce_column_list, False, 'future')
    # Check if there is future state data present
    if len(future_state) == 0:
        write_missing_data_file(future_state_file_path, current_state_file_path, output_file_path)
        return None
        
    #verify_columns(current_state, future_state)
        
    invalid_lines_list['future_state_bad_ascii'] = future_state_invalid_lines_list['invalid_ascii']
    invalid_lines_list['column_mismatch_future'] = future_state_invalid_lines_list['column_mismatch_list']

    stop_mark = time.time()
    print('future state inforce file consumed, total time: ' + str(compute_delta_time(start_mark, stop_mark)))   

    # Non modified lengths
    non_modified_metrics['future_pol_count'] = len(future_state)
    non_modified_metrics['current_pol_count'] = len(current_state)
    
    # Exeute column list vs column list analysis
    column_miss_list = column_mismatch_check(current_state, future_state)


    if drop_future_duplicates == True:
        # Drop duplicates from the future state frame
        future_state_pol_list = future_state['policynumber']
        future_state_duplicates = future_state_pol_list[future_state_pol_list.duplicated(keep=False)]
        future_state_duplicate_pol_list = future_state_duplicates.drop_duplicates(keep='first', inplace=False)
        future_state = future_state.drop_duplicates(subset='policynumber', keep='first', inplace=False)
        future_state = future_state.reset_index(drop=True)
    
    if drop_current_duplicates == True:
        # Drop duplicates from the current state frame
        current_state_pol_list = current_state['policynumber']
        current_state_duplicates = current_state_pol_list[current_state_pol_list.duplicated(keep=False)]
        current_state_duplicate_pol_list = current_state_duplicates.drop_duplicates(keep='first', inplace=False)
        current_state = current_state.drop_duplicates(subset='policynumber', keep='first', inplace=False)
        current_state = current_state.reset_index(drop=True)    
    
    non_modified_metrics['future_duplicates_count'] = len(future_state_duplicate_pol_list)

    # Execute field by field compare
    if field_compare_current_vs_future == True:
        start_mark = time.time()
        field_compare_result, miss_list = field_by_field_compare(current_state, future_state)
        # Drop extra column used for field by field compare
        future_state = future_state.drop(['NewIndex'], axis=1)
        stop_mark = time.time()

        print('Field by Field value compare: Done, total time: ' + str(compute_delta_time(start_mark, stop_mark)))
        

    start_mark = time.time()
    # Build list of policy mismatches for both data sets
    current_state_extras = current_state[ current_state["policynumber"].isin(future_state['policynumber']) == False ]
    future_state_extras = future_state[ future_state["policynumber"].isin(current_state['policynumber']) == False ]
    
    # Get policy count metrics
    non_modified_metrics['future_mismatch_count'] = len(future_state_extras)
    non_modified_metrics['current_mismatch_count'] = len(current_state_extras)
    
    # Create the miss list analysis files
    if field_compare_current_vs_future == True:
        # Create the policy number vs column mismatches
        write_miss_list(file_name, miss_list, output_miss_list_path, inforce_column_list, field_compare_result, non_modified_metrics)        
    
    # Record a list of missmatched Policy Numbers
    policy_drop_list['future_dropped'] = future_state_extras['policynumber']
    policy_drop_list['current_dropped'] = current_state_extras['policynumber']
    
    # If doing normalized analysis extra rows will be dropped from the data set
    if normalize_data == True:
        print('Normalizing data sets')
        current_state = current_state.drop(current_state_extras.index.values)
        future_state = future_state.drop(future_state_extras.index.values)
    
    # Sort data sets by Policy Number
    if sort_data == True:
        print('Sorting by policy number')
        current_state = current_state.sort_values(by=['policynumber'])      
        future_state = future_state.sort_values(by=['policynumber'])
    
    # Reset the index of the data sets for ease of analysis
    current_state = current_state.reset_index(drop=True)
    future_state = future_state.reset_index(drop=True)

    stop_mark = time.time()
    print('Find policy number mismatches, total time: ' + str(compute_delta_time(start_mark, stop_mark)))   

    # Execute file metrics on file one
    start_mark = time.time()
    current_state_result = data_metrics(current_state)
    stop_mark = time.time()
    print('current state metrics: Done, total time: ' + str(compute_delta_time(start_mark, stop_mark)))
    
    # Execute file metrics on file one
    start_mark = time.time()
    future_state_result = data_metrics(future_state)
    stop_mark = time.time() 
    print('future state metrics: Done, total time: ' + str(compute_delta_time(start_mark, stop_mark)))
    
    # Execute file metrics compare
    start_mark = time.time()
    metrics_result = file_metrics_compare(current_state_result, future_state_result)
    stop_mark = time.time()
    print('file metrics compare: Done, total time: ' + str(compute_delta_time(start_mark, stop_mark)))
    
    # Write output to metrics results file
    #write_metrics_file(current_state_result, future_state_result, metrics_result, field_compare_result, inforce_column_list)
    
  
    
    # Write output to metrics results file
    start_mark = time.time()
    write_metrics_file(current_state_result, future_state_result, metrics_result, non_modified_metrics,
                       inforce_column_list, policy_drop_list, invalid_lines_list, column_miss_list,
                       field_compare_result, future_state_duplicates, file_name, future_state_file_path, 
                       current_state_file_path, output_file_path, output_miss_list_path, reported_current_location, 
                       reported_future_location)
    stop_mark = time.time()
    print('file metrics compare: Done, total time: ' + str(compute_delta_time(start_mark, stop_mark)))
    
    stop_time = time.time()
    print("Finished file compare, total time: " + str(compute_delta_time(start_time, stop_time)))
    
    ##########################################
    # Trim off miss list policy numbers for testing    
    
    output_file_modification(file_name, future_state, 'FS')
    output_file_modification(file_name, current_state, 'CS')
    
    # Print normalized inforce file for further testing
    if produce_normalized_inforce == True:
        future_state = future_state.rename(columns={'ck_plan' : '//ck_plan'})    
        future_state.to_csv(file_name + '_FS_normalized' + '.ail2', sep = '\t', index = False, header = True)
        
        # Current state normalized file
        current_state = future_state.rename(columns={'ck_plan' : '//ck_plan'})    
        current_state.to_csv(file_name + '_CS_normalized' + '.ail2', sep = '\t', index = False, header = True)

    # Return field compare for anaysis summary output
    #return field_compare_result
    return None

# Function which executs custom output file modification used for building a subset inforce file 
# for testing ALFA
def output_file_modification(file_name, data_frame, file_type):
    
    # Read in miss list to drop
    policy_drop_list = pd.read_csv('test_data/P65_ML_IVC_spo_mode.csv')
 
    # Build drop list list of policy numbers
    drop_list = policy_drop_list['PolicyNum']

    # Drop policies which are mismatched
    data_frame = data_frame.drop(drop_list.index.values)

    # Print the test file
    data_frame = data_frame.rename(columns={'ck_plan' : '//ck_plan'})   
    data_frame.to_csv('test_data' + os.sep + file_name + '_' + file_type + '_SpoMode_droped' + '.ail2', sep = '\t', index = False, header = True)


# Function which opens included inforce file info document and extracts the column list
# of the inforce file being analized
def import_inforce_file_column_list(inforce_file_config_doc):
    
    try:
         column_list = []
         inforce_data = pd.read_csv(inforce_file_config_doc)
    except Exception as e:
        print("Error: Failed to open inforce file configuration document")
        print(e)
        raise

    for value in inforce_data['Name Key']:
        value.replace('\n', '')
        column_list.append(value)
        
    return column_list

            
# Function will open and read in an inforce file (future or current state) and
# return a pandas data set ready for analysis
def import_inforce_file(file_name, input_column_list, use_file_columns, file_type):
    
    re_match_string = '^[0-9A-Za-z\.\t\-]*$'  
    invalid_data = {}
    invalid_lines = []
    column_mismatch_result = []

    # Establish which type of file is being created
    if 'current' in file_type:
        file_type_loc = 'Current State'
    elif 'future' in file_type:
        file_type_loc = 'Future State'
    else:
        print('WARNING: Failed to establish file type during data set creation, file_type: ' + file_type)
        raise
    
    try:
        in_file = open(file_name, 'r')
        lines = in_file.readlines()
        in_file.close()
    except Exception as e:
        print("Error: Failed to open Future State Inforce File")
        print(e)
        raise
                
    parsed_data = []
    for line in lines:
        # If line is not empty continue
        if len(line.strip()) > 0:
            if '//' in line:
                # Find the column line
                if ('PolicyNumber' in line) or ('policynumber' in line):
                    column_list_local = []
                    line = line.replace('//', '')
                    line = line.replace('\n', '')
                    line = line.replace('\r', '')
                    line = line.lower()
                    column_list_local = line.split('\t')
  
                    for i in range(len(input_column_list)):
                        # Test input key against inforce file value
                        try:
                            if column_list_local[i] != input_column_list[i].lower():
                                column_mismatch_result.append(column_list_local[i])
                        except Exception as e:
                            print(e)
                            print("WARNING: Field mismatch in column list key!!!, key file field: " + str(input_column_list[i]))
                            print("Please address column key list issues before file compare can be executed")
                            column_mismatch_result.append(input_column_list[i])
                            pass
                continue
            else:
                reg_check = bool(re.match(re_match_string, line))
                if reg_check == False:
                    invalid_lines.append(line)
                
                line = line.replace('\n', '')
                line = line.replace('\r', '')
                parsed_data.append(line.split('\t'))

    # scrub new line characters from column list
    if use_file_columns == True:
        data_frame = pd.DataFrame(parsed_data, columns=column_list_local)
    else:
        # Trim off any columns greater than the data set
        if len(column_list_local) < len(input_column_list):
            for i in range(len(column_list_local), len(input_column_list)):
                del(input_column_list[len(column_list_local)])
        elif len(column_list_local) > len(input_column_list):
            for i in range(len(input_column_list), len(column_list_local)):
                del(column_list_local[len(input_column_list)])
        else:
            pass
        
        # Create the data frame
        data_frame = pd.DataFrame(parsed_data, columns=input_column_list)
    
    # Replace all blanks with nan value
    data_frame[data_frame == ''] = np.nan

    invalid_data['invalid_ascii'] = invalid_lines
    invalid_data['column_mismatch_list'] = column_mismatch_result

    return data_frame, invalid_data
    
# Function writes a warning to the output file and no real data because one of the input files
# Contained zero lines
def write_missing_data_file(future_state_file_path, current_state_file_path, output_file_path):
    
    output = open(output_file_path, 'w')
    output.write('WARNING: one of the input files contained zero data!\n')
    
    output.write('Future State: ' + future_state_file_path + '\n')    
    output.write('Current State: ' + current_state_file_path + '\n')
    
    output.close()

# Function to determine column name diffrences and postion between the two data sets
def column_mismatch_check(current_state, future_state):
    
    result = {}
    current_miss_list = []
    future_miss_list = []    
    
    current_columns = current_state.columns.values.tolist()
    future_columns = future_state.columns.values.tolist()
    
    # Build list of columns which exist in current state
    for col_name in current_columns:
        if col_name not in future_columns:
            print('Warning: Column mismatch: current state column name not found in future state file: ' + col_name)
            current_miss_list.append(col_name)
            
    # Build list of columns which exist in future state
    for col_name in future_columns:
        if col_name not in current_columns:
            print('Warning: Column mismatch: future state column name not found in current state file ' + col_name)
            future_miss_list.append(col_name)     

    result['future_column_mismatch_list'] = future_miss_list
    result['current_column_mismatch_list'] = current_miss_list
    
    return result

# Function creates a file contaning policy numbers, column, and values which dont match,
# This is built up during the field by field compare
def write_miss_list(file_name, miss_list, output_miss_list_path, inforce_column_list, field_compare_result, non_modified_metrics):

    # to allow for correct string compares
    #newIndex = future_state['policynumber']
    #newIndex = newIndex.str.lower()    
    #future_state['NewIndex'] = newIndex
    #future_state = future_state.set_index(['NewIndex'])
    output_count = 0

    list_blank = False

    miss_list_dir_path = output_directory + os.sep + 'miss_lists'
    miss_list_sub_dir_path = miss_list_dir_path + os.sep + file_name
    
    # Create individual files each with missmatch data inside

    # Create output directorys if it is not already created
    try: 
        os.makedirs(miss_list_dir_path)
    except OSError:
        if not os.path.isdir(miss_list_dir_path):
            raise

    try: 
        os.makedirs(miss_list_sub_dir_path)
    except OSError:
        if not os.path.isdir(miss_list_sub_dir_path):
            raise

    # Create miss list summary file
    write_summary = True
    summary_file = open(miss_list_sub_dir_path + os.sep + file_name + '_summary' + '.csv', 'w')
    
    # Add file compare metrics to summary file and non modified metrics
    # Non normalized policy metrics
    summary_file.write('Future State Total Policy Count: ' + str(non_modified_metrics['future_pol_count']) + '\n')
    summary_file.write('Future State Total Mismatched Policy Count: ' + str(non_modified_metrics['future_mismatch_count']) + '\n')
    summary_file.write('Future State Duplicates Count: ' + str(non_modified_metrics['future_duplicates_count']) + '\n')
    summary_file.write('Current State Total Policy Count: ' + str(non_modified_metrics['current_pol_count']) + '\n')
    summary_file.write('Current State Total Mismatched Policy Count: ' + str(non_modified_metrics['current_mismatch_count']) + '\n')    
    summary_file.write('\n')  
    
    # Row Metrics
    summary_file.write('Type,Miss Count,Match Count,Total,Match Percent,Miss Percent\n')  
    summary_file.write('Row Metrics Summary (Current vs Future),' +
                 str(field_compare_result['rowMissTotal']) + ',' +
                 str(field_compare_result['rowMatchTotal']) + ',' + 
                 str(field_compare_result['rowTotal']) + ',' + 
                 str(field_compare_result['rowMatchPercent']) + ',' +  
                 str(field_compare_result['rowMissPercent']) + '\n')
    summary_file.flush()
    summary_file.write('Field Metrics Summary (Current vs Future),' +
                 str(field_compare_result['fieldMissTotal']) + ',' +
                 str(field_compare_result['fieldMatchTotal']) + ',' + 
                 str(field_compare_result['fieldTotal']) + ',' +
                 str(field_compare_result['fieldMatchPercent']) + ',' +
                 str(field_compare_result['fieldMissPercent']) + '\n')
                 
    summary_file.write('\n')  
    summary_file.write('Column,PolicyNum,current,future\n')  

    # Create the miss list data objects and set index to column
    df_miss_list = pd.DataFrame(miss_list, columns=['PolicyNum', 'column', 'current', 'future'])    
    df_miss_list = df_miss_list.set_index('column')      
    
    for column in inforce_column_list:

        try:
            list_blank = False
            miss_per_column = df_miss_list.loc[column]
        except:
            list_blank = True
            
        # If we have data in the list write the list to a file
        if list_blank == False:
            # Update summary file
            write_summary = True
            
            # Create a new file for each column with mismatches
            specific_list_name = miss_list_sub_dir_path + os.sep + file_name + '_' + column + '.csv'    
            column_miss_list = open(specific_list_name, 'w')
            column_miss_list.write('PolicyNum,current,future\n')        
            
            try:
                # For each mismatched column fill in the file
                for row in miss_per_column.iterrows():
                    column_miss_list.write(row[1]['PolicyNum'] + ',' + str(row[1]['current']) + ',' + str(row[1]['future']) + '\n')

                    # Update summary list
                    if write_summary == True:
                        summary_file.write(column + ',' + row[1]['PolicyNum'] + ',' + str(row[1]['current']) + ',' + str(row[1]['future']) + '\n')
                        write_summary = False

                    # Limit the lenght of the output file to first n rows                    
                    output_count = output_count + 1
                    if output_count > 100:
                        output_count = 0
                        break
            except:
                print('Only One row returned: ' + column)
                column_miss_list.write(miss_per_column['PolicyNum'] + ',' + str(miss_per_column['current'])\
                                        + ',' + str(miss_per_column['future']) + '\n')
                 # Update summary list
                if write_summary == True:
                    summary_file.write(column + ',' + miss_per_column['PolicyNum'] + ',' + str(miss_per_column['current'])\
                                        + ',' + str(miss_per_column['future']) + '\n')
                    write_summary = False
                pass
            
            # Close the file
            column_miss_list.close()
            
    summary_file.close()
        

# Function updates a results summary files containing metrics information from each file executed
def write_summary_result(file_name, future_state_file_path, current_state_file_path, run_date, 
                         field_compare_result, summary_file, reported_current_location, reported_future_location):
    
    reported_current_path = reported_current_location + os.sep + file_name
    reported_future_path = reported_future_location + os.sep + file_name
    
    # Row Metrics
    summary_file.write('C State File,F State File,Type,Compared Date,Miss Count,Match Count,Total (current state),Match Percent,Miss Percent\n')  
    summary_file.write(reported_current_path + ',' + reported_future_path + ',Row,' + run_date + ',' +
                 str(field_compare_result['rowMissTotal']) + ',' +
                 str(field_compare_result['rowMatchTotal']) + ',' + 
                 str(field_compare_result['rowTotal']) + ',' + 
                 str(field_compare_result['rowMatchPercent']) + ',' +  
                 str(field_compare_result['rowMissPercent']) + '\n')
    summary_file.flush()
    summary_file.write(reported_current_path + ',' + reported_future_path + ',Field,' + run_date + ',' +
                 str(field_compare_result['fieldMissTotal']) + ',' +
                 str(field_compare_result['fieldMatchTotal']) + ',' + 
                 str(field_compare_result['fieldTotal']) + ',' +
                 str(field_compare_result['fieldMatchPercent']) + ',' +
                 str(field_compare_result['fieldMissPercent']) + '\n')
    summary_file.flush()

    summary_file.write('\n')

# Function which creates an output metrics file based on data passed in from 
# a metrics compare results object.

# Inputs: object which is the result of file_metrics_compare()
#         object which is the result of field_by_field_compare()
# results file is a CSV
#def write_metrics_file(current_state_result, future_state_result, metrics_result, field_compare_result, inforce_column_list):
def write_metrics_file(current_state_result, future_state_result, metrics_result, non_modified_metrics,
                       inforce_column_list, policy_drop_list, invalid_lines_list, column_miss_list,
                       field_compare_result, future_state_duplicates, file_name, future_state_file_path, 
                       current_state_file_path, output_file_path, output_miss_list_path, reported_current_location, 
                       reported_future_location):
                           
    limit_length = 100
    limit_count = 0

    # Open output file to record metrics    
    output = open(output_file_path, 'w')
    
    output.write('Inforce File Compare tool\n')
    output.write('Future State: ' + reported_future_location + os.sep + file_name + '\n')
    output.write('Current State: ' + reported_current_location + os.sep + file_name + '\n')
    # add new line
    output.write('\n')

    # Current state Vs Future State Metrics
    output.write('Current state vs Future state metrics (Field by Field Comparison)\n')
    # Row Metrics
    output.write('Row Miss Count,Row Match Count,Total Rows (current state),Row Match Percent,Row Miss Percent\n')  
    output.write(str(field_compare_result['rowMissTotal']) + ',' +
                 str(field_compare_result['rowMatchTotal']) + ',' + 
                 str(field_compare_result['rowTotal']) + ',' + 
                 str(field_compare_result['rowMatchPercent']) + ',' +  
                 str(field_compare_result['rowMissPercent']) + '\n')
  
    # Field Metrics
    output.write('Field Miss Count,Field Match Count,Total Field (current state),Field Match Percent,Field Miss Percent\n')  
    output.write(str(field_compare_result['fieldMissTotal']) + ',' +
                 str(field_compare_result['fieldMatchTotal']) + ',' + 
                 str(field_compare_result['fieldTotal']) + ',' +
                 str(field_compare_result['fieldMatchPercent']) + ',' +
                 str(field_compare_result['fieldMissPercent']) + '\n')
                 
    # add new line
    output.write('\n')  
     
    # Non normalized policy metrics
    output.write('Future State Total Policy Count: ' + str(non_modified_metrics['future_pol_count']) + '\n')
    output.write('Future State Total Mismatched Policy Count: ' + str(non_modified_metrics['future_mismatch_count']) + '\n')
    output.write('Future State Duplicates Count: ' + str(non_modified_metrics['future_duplicates_count']) + '\n')
    output.write('Current State Total Policy Count: ' + str(non_modified_metrics['current_pol_count']) + '\n')
    output.write('Current State Total Mismatched Policy Count: ' + str(non_modified_metrics['current_mismatch_count']) + '\n')
    
    # add new line
    output.write('\n')          
    output.write('NOTE: Data listed below has been normalized and is for DATA VALIDATION ONLY not file metrics\n')
    
    # add new line
    output.write('\n')
    # Individual file metrics
    # Future State
    output.write('Future State File Metrics\n')
    output.write('Row Count: ' + str(future_state_result['rowCount']) + '\n')
    output.write('Column Count: ' + str(future_state_result['columnCount']) + '\n')
    output.write('Total Field Count (non NAN fields): ' +  str(future_state_result['totalFieldCount']) + '\n')    
    # add new line
    output.write('\n')
    # Current State
    output.write('Current State File Metrics\n')
    output.write('Row Count: ' + str(current_state_result['rowCount']) + '\n')
    output.write('Column Count: ' + str(current_state_result['columnCount']) + '\n')
    output.write('Total Field Count (non NAN fields): ' +  str(current_state_result['totalFieldCount']) + '\n')
    
    # add new line
    output.write('\n')
    
    # Column Count and Sum metrics
    
    output.write('Column, Future State Count, Current State Count, Counts Match, Future State Sum, Current State Sum, Sum Match\n')
    for value in inforce_column_list:
        
        try:
            # Column Name
            output.write(str(value) + ',')
            # File One Count
            output.write(str(future_state_result['column_counts'][value + '_count']) + ',')
            # File Two Count
            output.write(str(current_state_result['column_counts'][value + '_count']) + ',')
            # Counts Match
            output.write(str(metrics_result['columnCountsResult'][value + '_count_match']) + ',')
            
            if value + '_sum' in current_state_result['column_sum']:
                # File One Sum
                output.write(str(future_state_result['column_sum'][value + '_sum']) + ',')
                # File Two Sum
                output.write(str(current_state_result['column_sum'][value + '_sum']) + ',')
                # Sum Match
                output.write(str(metrics_result['columnSumResult'][value + '_sum_match']) + ',')
            else:
                # File One Sum
                output.write('N/A,')
                # File Two Sum
                output.write('N/A,')
                # Sum Match
                output.write('N/A,')
        except:
            output.write('Write Error')
            pass
        
        output.flush()
            
        output.write('\n')
        
    #New Line
    output.write('\n')
    
    # Output Future State vs config file column mismatches
    output.write('Column Name Mismatch:\n')
    if len(invalid_lines_list['column_mismatch_future']) > 0:
        output.write('Current State:\n')
        for i in range(len(invalid_lines_list['column_mismatch_future'])):
            output.write(str(invalid_lines_list['column_mismatch_future'][i]) + '\n')

            
    if len(invalid_lines_list['column_mismatch_current']) > 0:
        output.write('Future State:\n')
        for i in range(len(invalid_lines_list['column_mismatch_current'])):
            output.write(str(invalid_lines_list['column_mismatch_current'][i]) + '\n')
    
    #New Line
    output.write('\n')
        
    # Output policies which were present in the future state file but not in the current
    # state file
    output.write('Policies which were present in the Future State file but not in the Current State\n')
    for value in policy_drop_list['future_dropped']:
        output.write(str(value) + '\n')

        # Limit lenght of output when applicable
        limit_count = limit_count + 1
        if limit_count > limit_length:
            limit_count = 0
            break
    #New Line
    output.write('\n')
    
    # Output policies which were present in the current state file but not in the future
    # state file
    output.write('Policies which were present in the Current State file but not in the Future State\n')
    for value in policy_drop_list['current_dropped']:
        output.write(str(value) + '\n')
        
        # Limit lenght of output when applicable
        limit_count = limit_count + 1
        if limit_count > limit_length:
            limit_count = 0
            break
    #New Line
    output.write('\n')
    
    # Output lines which contained invalid ascii characters in the future state file
    output.write('List of input lines with Invalid characters: Future State\n')
    if len(invalid_lines_list['future_state_bad_ascii']) > 0:
        
        # Output Column List Lables
        for value in inforce_column_list:
            output.write(str(value) + ',')
  
        #New Line
        output.write('\n')        
        
        for value in invalid_lines_list['future_state_bad_ascii']:
            out_line = value.split('\t')
            for item in out_line:
                output.write(str(item) + ',')
        
            # Limit lenght of output when applicable
            limit_count = limit_count + 1
            if limit_count > limit_length:
                limit_count = 0
                break
    
            #New Line
            output.write('\n')
    
    # Output lines which contained invalid ascii characters in the current state file
    output.write('List of input lines with Invalid characters: Current State\n')
    if len(invalid_lines_list['current_state_bad_ascii']) > 0:
        # Output Column List Lables
        for value in inforce_column_list:
            output.write(str(value) + ',')
  
        #New Line
        output.write('\n')        
        
        for value in invalid_lines_list['current_state_bad_ascii']:
            out_line = value.split('\t')
            for item in out_line:
                output.write(str(item) + ',')
                
            # Limit lenght of output when applicable
            limit_count = limit_count + 1
            if limit_count > limit_length:
                limit_count = 0
                break
    
            #New Line
            output.write('\n')    
            
    # Output policy numbers which are duplicates in the future state data set
    output.write('Policy numbers which are duplicates in the future state data set\n')
    if len(future_state_duplicates) > 0:
        for value in future_state_duplicates:
            output.write(str(value) + '\n')
            
            # Limit lenght of output when applicable
            limit_count = limit_count + 1
            if limit_count > limit_length:
                limit_count = 0
                break
  
        #New Line
        output.write('\n')        
    
    #Close output file
    output.close()
    
    
# Function which compares two metrics result sets and reports discrepencies
# Result is an object containing the pass/fail of metrics comparison
def file_metrics_compare(result_one, result_two):
    result = {}
    columnCountsResult = {}
    columnSumResult = {}
    result['columnsMatch'] = 'NO'
    result['sumMatch'] = 'NO'

    # Compare row counts
    if result_one['rowCount'] != result_two['rowCount']:   
        result['rowCountMatch'] = 'NO'
    else:
        result['rowCountMatch'] = 'YES'
        
        # Compare column counts
        for value in result_one['column_counts']:
            try:
                if result_one['column_counts'][value] != result_two['column_counts'][value]:   
                    columnCountsResult[value + '_match'] = 'NO' 
                    # Set global missmatch var
                    result['columnsMatch'] = 'NO'
                else:
                    columnCountsResult[value + '_match'] = 'YES'
            except:
                columnCountsResult[value + '_match'] = 'NO'
                pass
   
        # Compare sums
        for value in result_one['column_sum']:
            try:
                if result_one['column_sum'][value] != result_two['column_sum'][value]:   
                    columnSumResult[value + '_match'] = 'NO' 
                    result['sumMatch'] = 'NO'
                else:
                    columnSumResult[value + '_match'] = 'YES' 
            except:
                columnSumResult[value + '_match'] = 'NO'
                pass

    # Build result object
    result['columnCountsResult'] = columnCountsResult
    result['columnSumResult'] = columnSumResult
        
    return result

# Function which performs metrics analysis on an input data set
# result object returned:
#       Sum of all columns
def data_metrics(file_data):
    
    result = {}
    columSum = {}  
    nonHashSum = {}
    columnCounts = {}
    total_fields = 0
    null_number = 0
    field_count = 0
    
    # Static data metrics
    result['rowCount']        = len(file_data.index)    
    result['columnCount']     = len(file_data.columns)
    result['totalFieldCount'] = 0
     
    #Compute Sum of column data 
    column_list = list(file_data.columns.get_values())
    
    # Compute Counts of column data 
    for column in column_list:
        try:
            null_number = file_data[column].isnull().sum()
            field_count = len(file_data[column]) - null_number
            
            columnCounts[column + '_count'] = field_count
            # Get total number of non blank fields
            total_fields = total_fields + field_count
        except:
            pass
    result['column_counts'] = columnCounts
    result['totalFieldCount'] = total_fields

    # Compute column Sum metrics
    for column in column_list:
        try:     
            col_sum = pd.to_numeric(file_data[column]).sum()
            if round_sum == True:
                col_sum = round(col_sum, 2)
            columSum[column + '_sum'] = col_sum
            nonHashSum[column + '_nonhash_sum'] = col_sum
        except:
            col = file_data[column]                
            my_list = tuple(col)
            my_hash = hash(my_list)
            columSum[column + '_sum'] = my_hash
            pass
        
    result['column_sum'] = columSum
    result['nonhash_sum'] = nonHashSum

    return result
    
    

# Function which performs field by field comparison of two data sets
# result object is returned
def field_by_field_compare(current_state, future_state):
    
    result = {}
    miss_list = []

    
    row_count_current       = len(current_state.index)    
    column_count_current    = len(current_state.columns)
    
    row_match_count   = 0
    row_miss_count    = 0
    field_miss_total  = 0
    policy_found      = False
        
    total_field_count = row_count_current * column_count_current
    
    # to allow for correct string compares
    newIndex = future_state['policynumber']
    #newIndex = newIndex.str.lower()    
    future_state['NewIndex'] = newIndex
    future_state = future_state.set_index(['NewIndex'])

    #create a start time object
    start_time = time.time()

    #For each row in file A, compare data against matched row in file B
    for index, row_current in current_state.iterrows():
        # Reset policy found boolean
        policy_found = False
        
        try:
            #Build a row for comparison
            row_future = future_state.loc[row_current['policynumber']]
            # policy was found
            policy_found = True
            #Count number of matched policies
            row_match_count = row_match_count + 1
        except:
            # Count number of misses
            row_miss_count = row_miss_count + 1
            # Add total missed fields as all in row
            field_miss_total = field_miss_total + column_count_current
            pass

        # If policy found execute field by field compare
        if policy_found == True:
             #Compare the rows and report miss-matches
            for item in row_current.index:
                try:
                    # Compare the values
                    if row_current[item] != row_future[item]:                                       
                        if get_miss_list == True:
                            miss_list_string = row_current['policynumber'] + ',' + item + ',' + str(row_current[item]) + ',' + str(row_future[item])
                            miss_list.append(miss_list_string.split(','))
                        
                        field_miss_total = field_miss_total + 1
                except Exception as e:
                    field_miss_total = field_miss_total + 1
                    miss_list_string = row_current['policynumber'] + ',' + item + ',Error During Compare: N/A, N/A'
                    miss_list.append(miss_list_string.split(','))
                    # print('Warning: Error while executing field by field compare: policy number: ' + row_current['policynumber'])
                    # print(e)
                    pass
       
        if index % 5000 == 0:
            #Calculate and log total run time
            mid_time = time.time()
            deltaTime = compute_delta_time(start_time, mid_time)
            print('Total Time ' + str(index) +'/' + str(row_count_current) +  ' rows: ' + str(deltaTime))  

    #Compute Compare Metrics
    # Field Metrics
    field_match_total = total_field_count - field_miss_total
    field_match_percent = float(field_match_total) / float(total_field_count)
    field_miss_percent = float(field_miss_total) / float(total_field_count)
    field_match_percent = round(field_match_percent * 100, 2)
    field_miss_percent = round(field_miss_percent * 100, 2)

    result['fieldTotal']        = total_field_count
    result['fieldMatchTotal']   = field_match_total
    result['fieldMissTotal']    = field_miss_total
    result['fieldMatchPercent'] = field_match_percent
    result['fieldMissPercent']  = field_miss_percent

    # Row Metrics
    row_match_percent = float(row_match_count) / float(row_count_current)
    row_miss_percent = float(row_miss_count) / float(row_count_current)
    row_match_percent = round(row_match_percent * 100, 2) 
    row_miss_percent = round(row_miss_percent * 100, 2) 
    
    result['rowTotal']         = row_count_current
    result['rowMatchTotal']    = row_match_count
    result['rowMatchPercent']  = row_match_percent
    result['rowMissTotal']     = row_miss_count
    result['rowMissPercent']   = row_miss_percent
    
    #Calculate and log total run time
    total_time = time.time()
    deltaTime = compute_delta_time(start_time, total_time)
    print('Finished, Total Execution Time ' + str(index) +'/' + str(row_count_current) +  ' rows: ' + str(deltaTime))  
    
    # Reset future state index
    future_state = future_state.reset_index(drop=True)        
    
    return result, miss_list
    

# Function which builds and sends an email acting as a notification upstream
# showing which files were compared and attaching the output file
#
# File names are globals
def send_email_data(email_recipients, file_name, reported_current_location, reported_future_location, 
                    future_state_file_path, current_state_file_path, reported_output_location, metrics_summary_path):
    
    sender = "dataorgbsig@transamerica.com"    
    
    recipient_list = email_recipients.split(',')
    
    reported_current_path = reported_current_location + os.sep + file_name
    reported_future_path = reported_future_location + os.sep + file_name
    
    # Build email body text
    emailText = []
    emailText.append("BSIG Output File Compare Notification\n")
    emailText.append("\n")
    emailText.append("Execution Log:\n")
    # Read run log file and add it to the body of the email
    with open(output_directory + os.sep + 'run_log.txt', 'r') as f:
        for line in f:
            emailText.append(line)

    #Build the email body from the string list
    emailBody = ''.join(emailText)
    
    #Create email object with Subject, sender, and receipients
    emailObj = MIMEMultipart()
    emailObj['Subject'] = "BSIG Output File Compare Notification"
    emailObj['From'] = sender
    emailObj['To'] = ', '.join(recipient_list)
  
    #Write the body of the email
    emailObj.attach( MIMEText(emailBody, 'plain') )
    
    #Add the inforce file attachments
    fo = open(metrics_summary_path)
    fileContent = fo.read()
    fo.close()
    
    if send_summary == True:
        msg = MIMEBase('application', "octet-stream")
        msg.set_payload(fileContent)
        
        encoders.encode_base64(msg)
        msg.add_header('Content-Disposition', 'attachment', filename = os.path.basename(metrics_summary_path))
        emailObj.attach(msg)
    
    # Send the message via our own SMTP server.
    try:
        s = smtplib.SMTP('email1.aegonusa.com')
        s.sendmail(sender, email_recipients, emailObj.as_string())
        s.quit()
    except:
        print('Unable to send the email.  Error: ', sys.exc_info()[0])

       
# Function builds list of files in a directory by parsing the name and
# assembling the name to produce a unique file key associated with the full file name
# Files must be inforce files with '_' character as seperator
def build_input_file_list(directory_path):

    file_list = {}
        
    # Build list of files
    for filename in os.listdir(directory_path):
        extention_split = filename.split('.')
        file_array = extention_split[0].split('_')

        file_key = ''        
        
        for i in range(1, len(file_array)):
            if i == 1:
                file_key = file_array[i]
            else:
                file_key = file_key + '_' + file_array[i] 
        
        file_list[file_key] = directory_path + os.sep + filename
        
    return file_list

# Retuns the delta time between two time objects in string format
def compute_delta_time(start_time, stop_time):
    
    #Calculate and log total run time
    stopTimeStr = datetime.datetime.fromtimestamp(stop_time).strftime('%H:%M:%S')
    startTimeStr = datetime.datetime.fromtimestamp(start_time).strftime('%H:%M:%S')
    delta_time = ( datetime.datetime.strptime(stopTimeStr,'%H:%M:%S') - 
                 datetime.datetime.strptime(startTimeStr,'%H:%M:%S') )
                 
    return delta_time
    
def get_string_time(time):
    str_time = datetime.datetime.fromtimestamp(time).strftime('%Y-%m-%d_%H:%M:%S')
    return str_time

# Calls Main
if __name__ == '__main__':
    
    future_state_file_path = 'input_BSIG/Qqyy_P5_GEO.ail2'
    current_state_file_path = 'input_Q118/Q118_P5_GEO.Ail2'
    output_file_path = 'output.csv'
    output_miss_list_path = 'miss_list.csv'
    file_name = 'P5_GEO'
    
    # Execute file compare on sigle file
    #file_compare(file_name, future_state_file_path, current_state_file_path, output_file_path, 
    #             output_miss_list_path, reported_current_location, reported_future_location)
  
    # Execute file compare on directorys
    execute_file_compare_list()
