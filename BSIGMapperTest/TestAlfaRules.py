# -*- coding: utf-8 -*-
import os
import sys
sys.path.insert(0, '..' + os.sep + 'BSIGMapper' + os.sep)
print(sys.path)

import pandas
import unittest
from AlfaRules import AlfaRules

class TestAlfaRules(unittest.TestCase):
    def test_apply_rules(self):
        test_df = pandas.read_csv("mapping_test_data.csv")
        compare_df = pandas.read_csv("mapping_result_data.csv")
        alfa_rules_obj = AlfaRules()
        result_df = alfa_rules_obj.apply_rules(test_df)
        print(result_df)
        pandas.testing.assert_frame_equal(result_df, compare_df)
    
if __name__ == '__main__':
    unittest.main()
