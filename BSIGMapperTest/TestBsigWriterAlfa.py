###############################################
# TestBsigConfigAlfa.py:
#
# Unit Test for BsigConfigAlfa.py
#

import sys
sys.path.insert(0, 'C:\\Users\\erware\\BSIG\\BSIGMapper')

import pandas
import unittest

from BsigWriterAlfa import BsigWriterAlfa

#Path to application directory
mainDirPath = 'C:/Users/erware/BSIG/'
#Path to BsigConfig
bsigConfigPath = mainDirPath + 'BsigMapper/BsigConfig/'
#Path to Input Directory 
bsigInputPath = mainDirPath + 'BsigMapper/Input/'
#Path to Output Directory 
bsigOutputPath = mainDirPath + 'BsigMapper/Output/'
#Path to Unit Test Directory
bsigUnitTestPath = mainDirPath + 'BSIGMapperTest/'

        
###############################################
# Unit Tests
class TestBsigWriterAlfa(unittest.TestCase):

    ###############################################
    # Unit Tests: write_output_file()
    #Success Case
    def test_write_output_file(self):
        
        #read a test data frame
        testFrame = pandas.read_csv( bsigUnitTestPath + 'io_testing/test_dataFrame.csv' )
        
        testObj = BsigWriterAlfa()
        self.assertIsNone(testObj.write_output_file( testFrame, bsigUnitTestPath + 'io_testing/testOut.txt' ))
        
        
    ###############################################
    # Unit Tests: 
        


if __name__ == '__main__':
    unittest.main()


