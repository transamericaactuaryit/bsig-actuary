###############################################
# TestBsigAlfaMetrics.py:
#
# Unit Test for BsigAlfaMetrics.py
#

import os
import sys
sys.path.insert(0, '..' + os.sep + 'BSIGMapper' + os.sep)

import pandas as pd

import unittest
from BsigAlfaMetrics import BsigAlfaMetrics


outputFilePath = 'io_testing/'
        
###############################################
# Unit Tests
class TestBsigAlfaMetrics(unittest.TestCase):

    def test_record_row_count(self):
        
        testObj = BsigAlfaMetrics(outputFilePath, 'Alfa_Metrics_Unit_Test')
        self.assertIsNone(testObj.record_row_count( 10 ))
        
    def test_record_column_count(self):
 
        testObj = BsigAlfaMetrics(outputFilePath, 'Alfa_Metrics_Unit_Test')
        self.assertIsNone(testObj.record_column_count( 10 ))
    
    def test_record_found_in_name_key(self):
        
        testList = ['name_found_1', 'name_found_2', 'name_found_3']
       
        testObj = BsigAlfaMetrics(outputFilePath, 'Alfa_Metrics_Unit_Test')
        self.assertIsNone(testObj.record_found_in_name_key( testList ))

    
    def test_record_not_found_in_key(self):
        
        testList = ['name_not_found_1', 'name_not_found_2', 'name_not_found_3']
         
        testObj = BsigAlfaMetrics(outputFilePath, 'Alfa_Metrics_Unit_Test')
        self.assertIsNone(testObj.record_not_found_in_name_key( testList ))


    def test_sum_by_rider_type(self):
        test_frame = {'policynumber' : [1,2,3,4,5], 
                      'i_acctvalue': [1,2,3,4,5],
                      'gmab_gfv': [1,2,3,4,5], 
                      'GMIBContractOptionTypeGroup': ['GMIB',0,0,0,0], 
                      'GMDBContractOptionTypeGroup': [0,'GMDB',0,0,0], 
                      'GMWBContractOptionTypeGroup': [0,0,'GMWB',0,0], 
                      'GMLBContractOptionTypeGroup': [0,0,0,'GMLB',0], 
                      'TXPRContractOptionTypeGroup': [0,0,0,0,'TXRP']}
                      
        df = pd.DataFrame(test_frame)
  
        testObj = BsigAlfaMetrics(outputFilePath, 'Alfa_Metrics_Unit_Test')
          
        # GMIB Test
        resultObj = testObj.sum_by_rider_type(df, 'GMIB')
        self.assertEqual(resultObj['policyCount'], 1)
        self.assertEqual(resultObj['contract_cash_value_sum'], 1)
        self.assertEqual(resultObj['contract_option_guaranteed_future_value_sum'], 1)
        
        # GMDB Test
        resultObj = testObj.sum_by_rider_type(df, 'GMDB')
        self.assertEqual(resultObj['policyCount'], 1)
        self.assertEqual(resultObj['contract_cash_value_sum'], 2)
        self.assertEqual(resultObj['contract_option_guaranteed_future_value_sum'], 2)
        
        # GMWB Test
        resultObj = testObj.sum_by_rider_type(df, 'GMWB')
        self.assertEqual(resultObj['policyCount'], 1)
        self.assertEqual(resultObj['contract_cash_value_sum'], 3)
        self.assertEqual(resultObj['contract_option_guaranteed_future_value_sum'], 3)
        
        # GMLB Test
        resultObj = testObj.sum_by_rider_type(df, 'GMLB')
        self.assertEqual(resultObj['policyCount'], 1)
        self.assertEqual(resultObj['contract_cash_value_sum'], 4)
        self.assertEqual(resultObj['contract_option_guaranteed_future_value_sum'], 4)
        
        # TXRP Test
        resultObj = testObj.sum_by_rider_type(df, 'TXRP')
        self.assertEqual(resultObj['policyCount'], 1)
        self.assertEqual(resultObj['contract_cash_value_sum'], 5)
        self.assertEqual(resultObj['contract_option_guaranteed_future_value_sum'], 5)
      
    def test_gender_metrics(self):
        test_frame = {'policynumber' : [1,2,3], 
                      'i_acctvalue': [1,2,3],
                      'i_cashvalue': [1,2,3], 
                      'ck_sex': ['M','F','E']}
                      
        df = pd.DataFrame(test_frame)
          
        testObj = BsigAlfaMetrics(outputFilePath, 'Alfa_Metrics_Unit_Test')
          
        # M Test
        resultObj = testObj.gender_metrics(df, 'M')
        self.assertEqual(resultObj['policyCount'], 1)
        self.assertEqual(resultObj['contract_cash_value_sum'], 1)
        self.assertEqual(resultObj['contract_surrender_value_amount'], 1)
        
        # F Test
        resultObj = testObj.gender_metrics(df, 'F')
        self.assertEqual(resultObj['policyCount'], 1)
        self.assertEqual(resultObj['contract_cash_value_sum'], 2)
        self.assertEqual(resultObj['contract_surrender_value_amount'], 2)
        
        # E Test
        resultObj = testObj.gender_metrics(df, 'E')
        self.assertEqual(resultObj['policyCount'], 1)
        self.assertEqual(resultObj['contract_cash_value_sum'], 3)
        self.assertEqual(resultObj['contract_surrender_value_amount'], 3)
        
    def test_metrics_validation(self):
        test_frame_1 = [{'col1':[0,1,2]},{'col1':[0,1,2]},{'col1':[0,1,2]}]
        test_frame_2 = [{'col1':[0,1,2]},{'col1':[0,1,2]},{'col1':[0,1,2]}]
        test_frame_3 = [{'col1':[0,1,2]},{'col1':[5,5,5]},{'col1':[0,1,2]}]

        testObj = BsigAlfaMetrics(outputFilePath, 'Alfa_Metrics_Unit_Test')
        
        # Test frames are identical
        self.assertTrue(testObj.metrics_validation(test_frame_1, test_frame_2))
        # Test Frames are diffrent
        self.assertFalse(testObj.metrics_validation(test_frame_1, test_frame_3))
        
    

if __name__ == '__main__':
    unittest.main()


