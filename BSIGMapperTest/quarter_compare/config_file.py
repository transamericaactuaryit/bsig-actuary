# -*- coding: utf-8 -*-
###############################################
# config_file.py:
#
# Configuration file for BSIG infoce creation application contains
# static configuration information specific to the deployment
# enviornment.

import os
import sys
sys.path.insert(0, '.')

################################################
#
# Output file directory
output_directory = 'C:/<user path>/quarter_compare/output'

# Quarter One input directory
quarter_one_input_directory = 'C:/<user path>/quarter_compare/input_Q1'

# Quarter Two input Directory
quarter_two_future_directory = 'C:/<user path>/quarter_compare/input_Q2'
