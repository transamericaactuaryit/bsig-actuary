# -*- coding: utf-8 -*-
"""
Created on Tue Jul 31 11:49:25 2018

@author: erware
"""

import pandas as pd
import numpy as np
import time
import datetime
import csv
import re
import os

from config_file import quarter_one_input_directory, quarter_two_future_directory, output_directory


#allows for very large CSV files
csv.field_size_limit(1000000000)

# Executes file compare tool on list of files
def execute_file_compare_list():

    quarter_one_dir = quarter_one_input_directory + os.sep
    quarter_two_dir = quarter_two_future_directory + os.sep
    
    output_file_dir = output_directory
    extention_type = '.csv'
    
    # Get execution time stamp
    run_time_stamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d')   
    
    #output_files_path = output_file_dir + os.sep
    summary_file_path = str(output_file_dir + os.sep + 'result_summary_' + run_time_stamp + '.csv')
    run_log_file_path = output_file_dir + os.sep + 'run_log.txt'
    file_not_found_list = []
    file_found_list = []
    
    # Output File Setup
    # Create output directory if it is not already created
    try: 
        os.makedirs(output_file_dir)
    except OSError:
        if not os.path.isdir(output_file_dir):
            raise
    
    run_log = open(run_log_file_path, 'w')
    summary_file = open(summary_file_path, 'w')
    
    # Build list of future state files
    quarter_one_state_files = build_input_file_list(quarter_one_dir)
    
    if len(quarter_one_state_files) == 0:
        run_log.write('ERROR: Quarter One File Directory is empty')
        run_log.close()
        summary_file.write('ERROR: Quarter One File Directory is empty')
        summary_file.close()
        return 
    
    # Build list of current state files
    quarter_two_state_files = build_input_file_list(quarter_two_dir)
    
    if len(quarter_two_state_files) == 0:
        run_log.write('ERROR: Quarter Two File Directory is empty')
        run_log.close()
        summary_file.wrote('ERROR: Quarter Two File Directory is empty')
        summary_file.close()
        return
        
    # Exceute file compare for all files in directory
    for file_name in quarter_one_state_files:
        
        run_log.write('Execute File Compare: ' + file_name + '\n' )
        
        quarter_one_file_path = quarter_one_state_files[file_name]
        
        if 'PolicyID' in file_name:
            run_log.write('Skipping policyID file\n' )
            run_log.write('-------\n')
            continue
        try:
            quarter_two_file_path = quarter_two_state_files[file_name]  
            file_found_list.append(quarter_one_file_path)
            
            output_file_path = output_directory + os.sep + file_name + '_output' + extention_type
        except:
            run_log.write('Warning: Quarter One file match could not be established for file: ' + quarter_one_file_path + '\n')
            file_not_found_list.append(quarter_one_file_path)
            run_log.write('-------\n')
            continue
        try:
            
            run_log.write('Quarter One File: ' + quarter_one_file_path + '\n')
            run_log.write('Quarter Two File: ' + quarter_two_file_path + '\n')
            
            # Execute the file compare analysis            
            compare_result = execute_compare(file_name, quarter_one_file_path, quarter_two_file_path, output_file_path)

            if compare_result ==  None:
                run_log.write('Warning field compare result is empty, Future: ' + quarter_one_file_path + ', Current: ' + quarter_two_file_path + '\n')
                run_log.write('------\n')
                continue
            
            if len(compare_result) == 0:
                run_log.write('Warning field compare result is empty, Future: ' + quarter_one_file_path + ', Current: ' + quarter_two_file_path + '\n')
                run_log.write('------\n')
                continue

            print('Finished Quarter Compare, File: ' + quarter_one_file_path)
        except Exception as e:
            run_log.write('Warning: Exception caught in file compare, Future state: ' + quarter_one_file_path + ', Current State: ' + quarter_two_file_path + '\n')
            print(e)            
            run_log.write('-------\n')
            continue
        
        run_log.write('-------\n')

    summary_file.close()   
    run_log.close()
     

def execute_compare(file_name, quarter_one_file_path, quarter_two_file_path, output_file_path):


    start_time = time.time()
    
    print('Quarter Vs Quarter Compare Tool')
    print('Quarter One: ' + quarter_one_file_path)
    print('Quarter Two: ' + quarter_two_file_path)

    print("This is a file compare Tool, start time: " + get_string_time(start_time))
    
    # Read in inforce file data file and extract column list
    inforce_column_list = import_inforce_file_column_list('input_column_key.csv')
    
    # Read in files to be compared
    # Import current state inforce/AIL2 file
    print('Quarter One inforce file import')
    start_mark = time.time()
    quarter_one_data, invalid_data_q1 = import_inforce_file(quarter_one_file_path, inforce_column_list, False, 'current')
    stop_mark = time.time()
    print('Quarter One inforce file consumed, total time: ' + str(compute_delta_time(start_mark, stop_mark)))

    # Import future state inforce/AIL2 file
    print('Start Quarter Two inforce file import')
    start_mark = time.time()
    quarter_two_data, invalid_data_q1 = import_inforce_file(quarter_one_file_path, inforce_column_list, False, 'future')
    stop_mark = time.time()
    print('Quarter Two inforce file consumed, total time: ' + str(compute_delta_time(start_mark, stop_mark)))


    # Execute file metrics on file one
    start_mark = time.time()
    quarter_one_result = data_metrics(quarter_one_data)
    stop_mark = time.time()
    print('current state metrics: Done, total time: ' + str(compute_delta_time(start_mark, stop_mark)))
    
    # Execute file metrics on file one
    start_mark = time.time()
    quarter_two_result = data_metrics(quarter_two_data)
    stop_mark = time.time() 
    print('future state metrics: Done, total time: ' + str(compute_delta_time(start_mark, stop_mark)))
    
    # Attach any aditional metrics
    quarter_one_result['column_counts']['m_polcn_count'] = quarter_one_data['policynumber'].count()
    quarter_two_result['column_counts']['m_polcn_count'] = quarter_two_data['policynumber'].count()    
    
    # Execute file metrics compare
    start_mark = time.time()
    metrics_result = file_metrics_compare(quarter_one_result, quarter_two_result)
    stop_mark = time.time()
    print('file metrics compare: Done, total time: ' + str(compute_delta_time(start_mark, stop_mark)))

    # Execute the quarter vs quarter compare 
    start_mark = time.time()
    quarter_vs_quarter_compare(file_name, output_directory, quarter_one_result, 
                               quarter_two_result, metrics_result, inforce_column_list)
    stop_mark = time.time()                               
    print('quarter vs quarter compare: Done, total time: ' + str(compute_delta_time(start_mark, stop_mark)))


    
# Function which opens included inforce file info document and extracts the column list
# of the inforce file being analized
def import_inforce_file_column_list(inforce_file_config_doc):
    
    try:
         column_list = []
         inforce_data = pd.read_csv(inforce_file_config_doc)
    except Exception as e:
        print("Error: Failed to open inforce file configuration document")
        print(e)
        raise

    for value in inforce_data['Name Key']:
        value.replace('\n', '')
        column_list.append(value)
        
    return column_list
   
       
# Function builds list of files in a directory by parsing the name and
# assembling the name to produce a unique file key associated with the full file name
# Files must be inforce files with '_' character as seperator
def build_input_file_list(directory_path):

    file_list = {}
        
    # Build list of files
    for filename in os.listdir(directory_path):
        extention_split = filename.split('.')
        file_array = extention_split[0].split('_')

        file_key = ''        
        
        for i in range(1, len(file_array)):
            if i == 1:
                file_key = file_array[i]
            else:
                file_key = file_key + '_' + file_array[i] 
        
        file_list[file_key] = directory_path + os.sep + filename
        
    return file_list 
       
            
# Function will open and read in an inforce file (future or current state) and
# return a pandas data set ready for analysis
def import_inforce_file(file_name, input_column_list, use_file_columns, file_type):
    
    re_match_string = '^[0-9A-Za-z\.\t\-]*$'  
    invalid_data = {}
    invalid_lines = []
    column_mismatch_result = []

    # Establish which type of file is being created
    if 'current' in file_type:
        file_type_loc = 'Current State'
    elif 'future' in file_type:
        file_type_loc = 'Future State'
    else:
        print('WARNING: Failed to establish file type during data set creation, file_type: ' + file_type)
        raise
    
    try:
        in_file = open(file_name, 'r')
        lines = in_file.readlines()
        in_file.close()
    except Exception as e:
        print("Error: Failed to open Future State Inforce File")
        print(e)
        raise
                
    parsed_data = []
    for line in lines:
        # If line is not empty continue
        if len(line.strip()) > 0:
            if '//' in line:
                # Find the column line
                if ('PolicyNumber' in line) or ('policynumber' in line):
                    column_list_local = []
                    line = line.replace('//', '')
                    line = line.replace('\n', '')
                    line = line.replace('\r', '')
                    line = line.lower()
                    column_list_local = line.split('\t')
  
                    for i in range(len(input_column_list)):
                        # Test input key against inforce file value
                        try:
                            if column_list_local[i] != input_column_list[i].lower():
                                column_mismatch_result.append(column_list_local[i])
                        except Exception as e:
                            print(e)
                            print("WARNING: Field mismatch in column list key!!!, key file field: " + str(input_column_list[i]))
                            print("Please address column key list issues before file compare can be executed")
                            column_mismatch_result.append(input_column_list[i])
                            pass
                continue
            else:
                reg_check = bool(re.match(re_match_string, line))
                if reg_check == False:
                    invalid_lines.append(line)
                
                line = line.replace('\n', '')
                line = line.replace('\r', '')
                parsed_data.append(line.split('\t'))

    # scrub new line characters from column list
    if use_file_columns == True:
        data_frame = pd.DataFrame(parsed_data, columns=column_list_local)
    else:
        # Trim off any columns greater than the data set
        if len(column_list_local) < len(input_column_list):
            for i in range(len(column_list_local), len(input_column_list)):
                del(input_column_list[len(column_list_local)])
        elif len(column_list_local) > len(input_column_list):
            for i in range(len(input_column_list), len(column_list_local)):
                del(column_list_local[len(input_column_list)])
        else:
            pass
        
        # Create the data frame
        data_frame = pd.DataFrame(parsed_data, columns=input_column_list)
    
    # Replace all blanks with nan value
    data_frame[data_frame == ''] = np.nan

    invalid_data['invalid_ascii'] = invalid_lines
    invalid_data['column_mismatch_list'] = column_mismatch_result

    return data_frame, invalid_data


# Function which prints quarter vs quarter compare results in a seperate file
def quarter_vs_quarter_compare(file_name, output_directory, quarter_one_result, 
                               quarter_two_result, metrics_result, inforce_column_list):
    # Create Output folder for quarter vs quarter Setup
    try: 
        path_string = output_directory + os.sep + 'quarter_compare'
        os.makedirs(path_string)
    except OSError:
        if not os.path.isdir(path_string):
            raise

    column_names = []
    column_type = []
    column_pos_count = []
    quarter_one_sums = []
    quarter_two_sums = []
    sum_difference = []
    
    # Extract formatting data from key file
    quarter_compare_doc = 'quarter_compare_key.csv'
    try:
         column_list = []
         inforce_data = pd.read_csv(quarter_compare_doc)
    except Exception as e:
        print("Error: Failed to open inforce file configuration document")
        print(e)
        raise
    for value in inforce_data['ColumnName']:
        value.replace('\n', '')
        column_list.append(value)

    out_file = open(path_string + os.sep + file_name + '_quarter_compare.csv'    , 'w')
    
    # label formatting
    column_names.append(' ')
    column_type.append(' ')
    column_pos_count.append(file_name)
    quarter_one_sums.append('Q1')
    quarter_two_sums.append('Q2')
    sum_difference.append('%Difference')
    
    for index, row in inforce_data.iterrows():
        value = row['ColumnName']
        col_type = row['Type']
        pos_count = row['PosCount']
        
        column_names.append(value)
        column_type.append(col_type)
        column_pos_count.append(pos_count)
        
        try:
            quarter_one_val = quarter_one_result['nonhash_sum'][value.lower() + '_nonhash_sum']
            quarter_two_val = quarter_two_result['nonhash_sum'][value.lower() + '_nonhash_sum']
            diff_val = metrics_result['columnSumChangeResult'][value.lower() + '_nonhash_sum_change']
            
            # Build ordered lists of all 3 for printing
            quarter_one_sums.append(quarter_one_val)
            quarter_two_sums.append(quarter_two_val)
            sum_difference.append(diff_val)
        except:
            print("WARNING: non-summable column: " + value)
            try:
                quarter_one_val = quarter_one_result['column_counts'][value.lower() + '_count']
                quarter_two_val = quarter_two_result['column_counts'][value.lower() + '_count']
                
                
                quarter_one_sums.append(quarter_one_val)
                quarter_two_sums.append(quarter_two_val)
                
                diff_val = float(quarter_one_val / quarter_two_val)
                if diff_val != 1:
                    diff_val = 1 - diff_val
                else:
                    diff_val = diff_val - 1
                sum_difference.append(diff_val)
                
            except:
                print("WARNING: Unknown column name: " + value)
                quarter_one_sums.append('N/A')
                quarter_two_sums.append('N/A')
                sum_difference.append('N/A')
                pass
            pass
        
    # write position
    for value in column_pos_count:
        try:
            str_val = str(value)
            str_val = str_val.replace('\n', '')
            out_file.write(str(str_val) + ',')
            # File Two Sum
        except:
            out_file.write('Write Error')
            pass
        
    out_file.write('\n')   
        
    # write type, count or sum
    for value in column_type:
        try:
            str_val = str(value)
            str_val = str_val.replace('\n', '')
            out_file.write(str(str_val) + ',')
            # File Two Sum
        except:
            out_file.write('Write Error')
            pass
        
    out_file.write('\n')   
    
    # write column names
    for value in column_names:
        try:
            str_val = value.replace('\n', '')
            out_file.write(str(str_val) + ',')
            # File Two Sum
        except:
            out_file.write('Write Error')
            pass
        
    out_file.write('\n')   

    # Write current state sums
    for value in quarter_one_sums:
        try:
            out_file.write(str(value) + ',')
            # File Two Sum
        except:
            out_file.write('Write Error')
            pass
        
    out_file.write('\n')  
    
    # Write future state sums
    for value in quarter_two_sums:
        try:
            out_file.write(str(value) + ',')
            # File Two Sum
        except:
            out_file.write('Write Error')
            pass
        
    out_file.write('\n')  
    
    # Write sum diffrences
    for value in sum_difference:
        try:
            out_file.write(str(value) + ',')
            # File Two Sum
        except:
            out_file.write('Write Error')
            pass

    out_file.close()
        

# Function which compares two metrics result sets and reports discrepencies
# Result is an object containing the pass/fail of metrics comparison
def file_metrics_compare(result_one, result_two):
    result = {}
    columnCountsResult = {}
    columnSumResult = {}
    columnSumChangeResult = {}
    
    result['columnsMatch'] = 'NO'
    result['sumMatch'] = 'NO'

    # Compare row counts
    if result_one['rowCount'] != result_two['rowCount']:   
        result['rowCountMatch'] = 'NO'
    else:
        result['rowCountMatch'] = 'YES'
        
        # Compare column counts
        for value in result_one['column_counts']:
            try:
                if result_one['column_counts'][value] != result_two['column_counts'][value]:   
                    columnCountsResult[value + '_match'] = 'NO' 
                    # Set global missmatch var
                    result['columnsMatch'] = 'NO'
                else:
                    columnCountsResult[value + '_match'] = 'YES'
            except:
                columnCountsResult[value + '_match'] = 'NO'
                pass
   
        # Compare sums
        for value in result_one['column_sum']:
            try:
                if result_one['column_sum'][value] != result_two['column_sum'][value]:   
                    columnSumResult[value + '_match'] = 'NO' 
                    result['sumMatch'] = 'NO'
                else:
                    columnSumResult[value + '_match'] = 'YES' 
            except:
                columnSumResult[value + '_match'] = 'NO'
                pass
            
        # compute sum diffrences
        for value in result_one['nonhash_sum']:
            try:
                if result_one['nonhash_sum'][value] == result_two['nonhash_sum'][value]:
                    columnSumChangeResult[value + '_change'] = 0
                elif result_one['nonhash_sum'][value] == 0 or result_two['nonhash_sum'][value] == 0:
                    columnSumChangeResult[value + '_change'] = 'N/A'
                    print('Warning column sum is equal to zero')
                else:
                    val_one = float(result_one['nonhash_sum'][value])
                    val_two = float(result_two['nonhash_sum'][value])
                    if val_one > val_two:
                        percent_change = val_two / val_one
                        percent_change = 1 - percent_change
                        percent_change  = -percent_change
                    else:
                        percent_change = val_one / val_two
                        percent_change = 1 - percent_change
                    percent_change = percent_change * 100
                    columnSumChangeResult[value + '_change'] = str(float(percent_change))
            except:
                columnSumChangeResult[value + '_match'] = 'N/A'
                pass
    # Build result object
    result['columnCountsResult'] = columnCountsResult
    result['columnSumResult'] = columnSumResult
    result['columnSumChangeResult'] = columnSumChangeResult
        
    return result

# Function which performs metrics analysis on an input data set
# result object returned:
#       Sum of all columns
def data_metrics(file_data):
    
    result = {}
    columSum = {}  
    nonHashSum = {}
    columnCounts = {}
    total_fields = 0
    null_number = 0
    field_count = 0
    round_sum = True
    
    # Static data metrics
    result['rowCount']        = len(file_data.index)    
    result['columnCount']     = len(file_data.columns)
    result['totalFieldCount'] = 0
     
    #Compute Sum of column data 
    column_list = list(file_data.columns.get_values())
    
    # Compute Counts of column data 
    for column in column_list:
        try:
            null_number = file_data[column].isnull().sum()
            field_count = len(file_data[column]) - null_number
            
            columnCounts[column + '_count'] = field_count
            # Get total number of non blank fields
            total_fields = total_fields + field_count
        except:
            pass
    result['column_counts'] = columnCounts
    result['totalFieldCount'] = total_fields

    # Compute column Sum metrics
    for column in column_list:
        try:     
            col_sum = pd.to_numeric(file_data[column]).sum()
            if round_sum == True:
                col_sum = round(col_sum, 4)
            columSum[column + '_sum'] = col_sum
            nonHashSum[column + '_nonhash_sum'] = col_sum
        except:
            col = file_data[column]                
            my_list = tuple(col)
            my_hash = hash(my_list)
            columSum[column + '_sum'] = my_hash
            pass
        
    result['column_sum'] = columSum
    result['nonhash_sum'] = nonHashSum

    return result

# Retuns the delta time between two time objects in string format
def compute_delta_time(start_time, stop_time):
    
    #Calculate and log total run time
    stopTimeStr = datetime.datetime.fromtimestamp(stop_time).strftime('%H:%M:%S')
    startTimeStr = datetime.datetime.fromtimestamp(start_time).strftime('%H:%M:%S')
    delta_time = ( datetime.datetime.strptime(stopTimeStr,'%H:%M:%S') - 
                 datetime.datetime.strptime(startTimeStr,'%H:%M:%S') )
                 
    return delta_time
    
def get_string_time(time):
    str_time = datetime.datetime.fromtimestamp(time).strftime('%Y-%m-%d_%H:%M:%S')
    return str_time

    
# Calls Main
if __name__ == '__main__':
    
    # Execute file compare on directorys
    execute_file_compare_list()
