# -*- coding: utf-8 -*-
###############################################
# config_file.py:
#
# Configuration file for BSIG infoce creation application contains
# static configuration information specific to the deployment
# enviornment.

import os
import sys
sys.path.insert(0, '.')

################################################
# This is the location which will appear on the report, 
# not the actual location of the files. 
reported_current_location = '5 - BSI - Actuarial/Sample AIL2 Files/Ail2 files for Q218'
reported_future_location = '5 - BSI - Actuarial/BSIG_Test_Data/'
reported_output_location = '5 - BSI - Actuarial/BSIG_Test_Data'
#

# Enable to send email notification after file send
send_email = False
# Attach summary file to email
send_summary = False
# Enable filed by field compare for current state vs future state
field_compare_current_vs_future = True
# Enable miss list analysis
get_miss_list = True

# List of emails to send results email to
email_recipients = 'erik.ware@transamerica.com'

# Output file directory
output_directory = 'C:/Users/erware/development/field_compare/output'

# Future State Input Directory
input_future_directory = 'C:/Users/erware/development/field_compare/input_BSIG'

# Current State Input Directory
current_input_directory = 'C:/Users/erware/development/field_compare/input_Q218'
