# -*- coding: utf-8 -*-
import pandas as pd
import os
import sys
sys.path.insert(0, '..' + os.sep + 'BSIGMapper' + os.sep)

import AlfaRules
import unittest

class TestPlanCodeMap(unittest.TestCase):
    def test_get_live_alfa_code(self):
        old_record = pd.DataFrame.from_dict({'ck_plan': '23END2',	
                       'ck_age': '45',	
                       'ck_sex': 'F',	
                       'ck_smkstatus': '',
                       'ck_char1': '',
                       'ck_char2': '',	
                       'contractqualifiedindicator':'',
                       'ck_char4':'', 
                       'ck_char5':'',
                       'ck_char6':'',
                       'i_acctvalue': '42901.13',
                       'i_cashvalue': '43169.05',	
                       'sa1':'',
                       'dynsa1':'',
                       'shfsa1':'',	
                       'safe': '',
                       'gbdb_growth': 'NULL',
                       'gmdb_stepup': 'NULL',
                       'gbdb_rop': 'NULL',
                       'gmib_growth': 'NULL',
                       'gmib_ratchet': 'NULL',
                       'pb_rwa': 'NULL',
                       'pb_free': 'NULL'	}, orient='index')
        result_df = AlfaRules.get_alfa_plan_code(old_record)
        self.assertEqual(result_df.loc['ck_plan'].values, 'PEND')
        
    def test_get_live_alfa_code_no_value(self):
        old_record = pd.DataFrame.from_dict({'ck_plan': '23END1',	
                       'ck_age': '45',	
                       'ck_sex': 'F',	
                       'ck_smkstatus': '',
                       'ck_char1': '',
                       'ck_char2': '',	
                       'contractqualifiedindicator':'',
                       'ck_char4':'', 
                       'ck_char5':'',
                       'ck_char6':'',
                       'i_acctvalue': '42901.13',
                       'i_cashvalue': '43169.05',	
                       'sa1':'',
                       'dynsa1':'',
                       'shfsa1':'',	
                       'safe': '',
                       'gbdb_growth': 'NULL',
                       'gmdb_stepup': 'NULL',
                       'gbdb_rop': 'NULL',
                       'gmib_growth': 'NULL',
                       'gmib_ratchet': 'NULL',
                       'pb_rwa': 'NULL',
                       'pb_free': 'NULL'	}, orient='index')
        result_df = AlfaRules.get_alfa_plan_code(old_record)
        self.assertEqual(result_df.loc['ck_plan'].values, '23END1')
        
    def test_get_no_exist_key(self):
        old_record = pd.DataFrame.from_dict({'ck_plan': 'B1G6N1',	
                       'ck_age': '45',	
                       'ck_sex': 'F',	
                       'ck_smkstatus': '',
                       'ck_char1': '',
                       'ck_char2': '',	
                       'contractqualifiedindicator':'',
                       'ck_char4':'', 
                       'ck_char5':'',
                       'ck_char6':'',
                       'i_acctvalue': '42901.13',
                       'i_cashvalue': '43169.05',	
                       'sa1':'',
                       'dynsa1':'',
                       'shfsa1':'',	
                       'safe': '',
                       'gbdb_growth': 'NULL',
                       'gmdb_stepup': 'NULL',
                       'gbdb_rop': 'NULL',
                       'gmib_growth': 'NULL',
                       'gmib_ratchet': 'NULL',
                       'pb_rwa': 'NULL',
                       'pb_free': 'NULL'	}, orient='index')
        result_df = AlfaRules.get_alfa_plan_code(old_record)
        self.assertEqual(result_df.loc['ck_plan'].values, 'B1G6N1')

if __name__ == '__main__':
    unittest.main()