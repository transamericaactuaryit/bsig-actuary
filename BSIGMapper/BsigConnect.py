###############################################
# BsigConnector.py:
#
# Parent class for establishing connections to the hadoop environment 
# 
#

class BsigConnect:
    
    #Constructor
    def __init__(self):
        pass

    def build_query(self, inputString):
        pass
        
    #Connection function, used to establish connection 
    def run_query(self):
        pass
        #function to be over written at child level