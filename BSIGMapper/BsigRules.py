# -*- coding: utf-8 -*-

#######################################
#
#   BsigRules.py
#
#   Base class for apply rules to the data
from abc import ABC, abstractmethod

class BsigRules(ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def apply_rules(self, bsigObj):
        pass
