###############################################
# BsigConfiguration.py:
#
# Parent class for application configuration, main purpose of this class is to
# read in configuration files, process configuration data, and produce 
# lists of data which is to be processed, and information about which group
# the processed data relates to.
# 
#

class BsigConfiguration:
    
    #Constructor
    def __init__(self):
        pass
    
    # Function reads in a JSON formatted config file containing the folloing configuration information
    #
    #   Inforce File Grouping: Fair Value, DAC, STAT, MVN, MCVNB, ext. 
    #   Quarter
    #   Year
    #   Destination Directory
    #
    def read_config_file(self, inputString):
        pass
