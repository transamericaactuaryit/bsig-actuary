###############################################
# AlfaSparkConnect:
# Establishes a connection to Hadoop data lake using the spark library for 
# the AlfaMapper.
#
# Returns the connection object 

import logging
import json
from config_file import alfaLoggingPath
from config_file import spark_database 
from pyspark.sql import SparkSession
spark = SparkSession.builder.appName("AlfaMapper").getOrCreate()

#BSIG Specific libraries
from BsigConnect import BsigConnect 

class AlfaSparkConnect(BsigConnect):
    'child class used for management of all SPARK connections'
    
    #Constructor
    def __init__(self):
        self.logger = logging.getLogger('alfaLogger.AlfaSparkConnect')
        self.logger.debug('Creating an instance of AlfaSparkConnect')
        self.queryString = ''
        self.subqueryString = ''
        self.whereString = ''
        self.limitString = ''
    
    def _clean_full_query_format(self, query):
        #Spark uses nvl, ODBC uses isnull
        return query.replace("isnull(","nvl(")

    def _clean_pre_query_format(self, query):
        #Spark uses `, ODBC uses ' or "
        return query.replace('\'','`').replace('\"','`')
    
    def _clear_variables(self):
        self.queryString = ''
        self.subqueryString = ''
        self.whereString = ''
        self.limitString = ''
        return
    
    def _convert_quarter_to_months(self,values):
        if values == "1" or values == "Q1":
            return "('1','2','3')"
        elif values == "2" or values == "Q2":
            return "('4','5','6')"
        elif values == "3" or values == "Q3":
            return "('7','8','9')"
        elif values == "4" or values == "Q4":
            return "('10','11','12')"
    
    def query_combine_param_parts(self, column, operation, values):
        if operation == "IN":
            return str(column + " " + operation + " " + self.query_param_values(values))
        elif column == "Quarter":
            return str("ContractIssueMonth IN " + self._convert_quarter_to_months(values))
        else:
            return str(column + " " + operation + " '" + values + "'")
        
    def query_param_values(self, values):
        return "('" + "','".join(str(_) for _ in values) + "')"
    
    def add_query_return_limit(self, rowCount):
        self.logger.debug('Executing add_query_return_limit()')
        self.limitString = 'LIMIT ' + str(rowCount)
        
    def build_query_from_file(self, filePath):
        self.logger.debug('Executing build_query_from_file()')
        self.logger.debug('File Path: ' + filePath)
        #Open and read in the SQL file
        file = open(filePath, 'r')
        sqlFile = file.read()
        file.close()
               
        self.queryString = sqlFile
        
    def build_subquery_from_file(self, filePath):
        self.logger.debug('Executing build_subquery_from_file()')
        self.logger.debug('File Path: ' + filePath)
        #Open and read in the SQL file
        file = open(filePath, 'r')
        sqlFile = file.read()
        file.close()
               
        self.subqueryString = sqlFile
        
    def build_where(self, inputWhereString):
        self.logger.debug('Executing build_where()')
        self.whereString = inputWhereString
        
    def build_where_append_and(self, inputAppendString):
        self.logger.debug('Executing build_where_append_and()')
        if self.whereString == '':
            self.whereString = ' WHERE ' + inputAppendString
        else:
            self.whereString = self.whereString + ' AND ' + inputAppendString
    
    def build_where_from_file(self, filePath):
        self.logger.debug('Executing build_where_from_file()')
        self.logger.debug('File Path: ' + filePath)
        #Open and read in the SQL file
        file = open(filePath, 'r')
        sqlFile = file.read()
        file.close()
        
        if self.whereString == '':
            self.whereString = ' WHERE ' + sqlFile
        else:
            self.whereString = ' ' + sqlFile  
 
    def build_where_from_json_array(self, array):
        self.logger.debug('Executing build_where_from_json_array()')
        
        #Open and read in the json string array
        dataParams = json.loads(json.dumps(array)) 
            
        for whereParams in dataParams:
            if 'QueryParamColumn' in whereParams and 'QueryParamOperator' in whereParams and 'QueryParamValues' in whereParams:
                self.build_where_append_and(self.query_combine_param_parts(whereParams['QueryParamColumn'], whereParams['QueryParamOperator'], whereParams['QueryParamValues']))
 
    #Connection function, used to establish connection 
    def run_query(self, inforceName):
        self.logger.debug('Executing run_query()')
        
        #Convert single quote column aliases from single and double quotes to backticks
        #query = self._clean_pre_query_format(self.queryString)
        query = self.queryString
        #Convert query to local object
        
        if self.subqueryString != '':
            query = query + ' ' + self.subqueryString
        
        if self.whereString != '':
            query = query + self.whereString
        
        if self.limitString != '':
            query = query + ' ' + self.limitString
        
        #Clean query for database differences
        query = self._clean_full_query_format(query)
        
        self.logger.debug("Printing QUERY")
        self.logger.debug(query)
 
        # Print Query to log file
        query_file_path = alfaLoggingPath + inforceName + '_query.txt'
        self.logger.debug('Saving query to external log file: ' + query_file_path)
        out_file = open(query_file_path, 'w')
        out_file.write(query)
        out_file.close()
             
        dataFrame = None
             
        try:
             self.logger.debug("Executing Query")
             # Set spark database and execute the SQL query
             spark.sql("USE " + spark_database + " ")
             dataObj = spark.sql(query)
             
             #Convert to pandas data frame
             dataFrame = dataObj.toPandas()           
             
             # Log the result
             self.logger.debug("Query Finished! Printing Result")
             self.logger.debug(dataFrame)
        except Exception as e:
            dataObj = None
            self.logger.error("Failed to execute query, ConnectionError")
            self.logger.error(e)

        self._clear_variables()
        return(dataFrame)
        
        
        
        
        

