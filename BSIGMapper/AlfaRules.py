# -*- coding: utf-8 -*-
################################################
#
#    AlfaRules.py
#
#    Implemetation of rules class for mapping data for Alfa
import logging
import sys
sys.path.insert(0, '.')
from config_file import plan_code_file, legal_entity_num_file, stepup_corrections_file, fund_fees_file
import pandas

#Alfa code data frame to refer to
alfa_code_data_frame = ""
with open(plan_code_file, 'rb') as fh:
    alfa_code_data_frame = pandas.read_excel(fh)
    alfa_code_data_frame.set_index('PLAN_CD', inplace=True)

#Legal entity data frame to refer to
legal_entity_num_df = ""
with open(legal_entity_num_file, 'rb') as fh:
    legal_entity_num_df = pandas.read_excel(fh)
    legal_entity_num_df['STAT_CO_CD'] =  legal_entity_num_df['STAT_CO_CD'].apply(str)
    legal_entity_num_df.set_index('STAT_CO_CD', inplace=True)
     
#Legal entity data frame to refer to
stepup_corrections_df = ""
with open(stepup_corrections_file, 'rb') as fh:
    stepup_corrections_df = pandas.read_excel(fh)
    stepup_corrections_df['CNTRT_NMBR'] =  stepup_corrections_df['CNTRT_NMBR'].apply(str)
    stepup_corrections_df.set_index('CNTRT_NMBR', inplace=True)
     
#Legal entity data frame to refer to
fund_fees_df = ""
with open(fund_fees_file, 'rb') as fh:
    fund_fees_df = pandas.read_excel(fh)
    fund_fees_df['FUND_INFRC_DT'] =  fund_fees_df['FUND_INFRC_DT'].apply(str)
    fund_fees_df.set_index('FUND_INFRC_DT', inplace=True)

logger = logging.getLogger('bsigLogger.AlfaRules')
   
# grab the alfa code if available
def get_alfa_plan_code(sourceDf):
    destDf = sourceDf
    try:
        # P75 plan code selection
        if destDf['inforceName'] == 'Qqyy_P75':
            alfa_code_set = alfa_code_data_frame.loc[sourceDf['ck_plan']]
            if destDf['contractsourcemarketingorganizationcode'] == 'WR15':
                for index, code in alfa_code_set.iterrows():
                    if code['MKT_ORG'] == 'Y' :
                        alfa_code = code['ALFA_NM']
                        break
            else:
                for index, code in alfa_code_set.iterrows():
                    if code['MKT_ORG'] == 'N':
                        alfa_code = code['ALFA_NM']
                        break
            #If the alfa_code returns a nan, no alfa_code exists so return original row
            if(pandas.isnull(alfa_code)): #If value is null, drop the row entirely
                return destDf
        else:
            alfa_code = alfa_code_data_frame.loc[sourceDf['ck_plan']].values.item(0)
            #If the alfa_code returns a nan, no alfa_code exists so return original row
            if(pandas.isnull(alfa_code)): #If value is null, drop the row entirely
                return destDf
        return alfa_code 
    except KeyError:
        #If key is not found, drop the row entirely then
        logger.debug('Warning Plan Code Error: policynumber: ' + sourceDf['policynumber'] + ', ck_plan: ' +  sourceDf['ck_plan'])
        return destDf

#Grab the legal entity code if available
def get_alfa_legal_entity_num(row):
    try:
        legal_entity_num = legal_entity_num_df.loc[row].values.item(0)
        #If the legal entity returns a nan, no legal entity exists so return original row
        if(pandas.isnull(legal_entity_num)):
            return row
        return legal_entity_num
    except KeyError:
        #If key is not found, return original row
        logger.debug("Legal entity key not found.")
        return row
    
#Grab the fund revenue sharing fee
def get_fund_rev_shrg_fee(row):
    try:
        fund_fee = fund_fees_df.loc[row].values.item(0)
        #If the legal entity returns a nan, no legal entity exists so return original row
        if(pandas.isnull()):
            return row
        return fund_fee
    except KeyError:
        #If key is not found, return original row
        logger.debug("Fund fee key not found.")
        return row
    
#convert glb_sj
def equate_glb_sj(sourceDf):
    destDf = sourceDf
    
    inforce_by_cotg_list = {'Qqyy_P6_ML_Legacy'}
    
    if destDf['inforceName'] not in inforce_by_cotg_list \
        and destDf['GMLBContractOptionTypeGroup'] == 'GMLB' :
        destDf['glb_sj'] = destDf['GMLBContractOptionSingleJointIndicator']
    elif destDf['inforceName'] in inforce_by_cotg_list \
        and destDf['GMIBContractOptionTypeGroup'] == 'GMIB' :
        destDf['glb_sj'] = destDf['GMIBContractOptionSingleJointIndicator']
    else:
        destDf['glb_sj'] = 'S'
    return destDf

#convert gmdb_growth
def equate_gmdb_growth(sourceDf):
    destDf = sourceDf
    
    inforce_by_cotg_list = {'Qqyy_P65_DIASPL_CQIssues',
                            'Qqyy_P65_P5_VANG_CQIssues', 
                            'Qqyy_P65_ML_IVC', 
                            'Qqyy_P6_ML_Legacy',
                            'Qqyy_P75_CQIssues', 
                            'Qqyy_P65_DIASPL'}
    
    if destDf['policynumber'] == '101872E98' \
    and destDf['GMDBContractOptionTypeGroup'] == 'GMDB':  #Specifically for David Sun, even though results are the same
        destDf['gmdb_growth'] = destDf['cogbv']
    elif destDf['inforceName'] in inforce_by_cotg_list \
    and destDf['GMDBContractOptionTypeGroup'] == 'GMDB':
        destDf['gmdb_growth'] = destDf['cogbv']
    else:
        destDf['gmdb_growth'] = '0.00'
        
    return destDf

#convert gmdb_step_up
def equate_gmdb_stepup(sourceDf):
    destDf = sourceDf
    
    inforce_by_cotg_list = {'Qqyy_P65_DIASPL_CQIssues',
                            'Qqyy_P65_ML_IVC', 
                            'Qqyy_P65_DIASPL', 
                            'Qqyy_P75_CQIssues', 
                            'Qqyy_P6_ML_Legacy', 'Qqyy_P75'}
    inforce_by_vang_list = {'Qqyy_P65_P5_VANG_CQIssues', 
                            'Qqyy_P65_P5_VANG'}
    
    if destDf['policynumber'] == '101872E98' and destDf['GMDBContractOptionTypeGroup'] == 'GMDB':  #Specifically for David Sun
        destDf['gmdb_stepup'] = destDf['cosv']
    elif destDf['inforceName'] in inforce_by_vang_list:
        if destDf['css_name'] == 'VantageP5' and destDf['GMDBContractOptionTypeGroup'] == 'GMDB':
            destDf['gmdb_stepup'] = destDf['cosv']
        elif destDf['css_name'] == 'VantageP65' and destDf['GMDBContractOptionTypeGroup'] == 'GMDB':
            try:
                destDf['gmdb_stepup'] = stepup_corrections_df.loc[destDf['policynumber']].values.item(0)
            except KeyError:
                pass
            if destDf['gmdb_stepup'] < destDf['cosv']:
                destDf['gmdb_stepup'] = destDf['cosv']
        else:
            destDf['gmdb_stepup'] = '0.00'
    elif destDf['inforceName'] in inforce_by_cotg_list and destDf['GMDBContractOptionTypeGroup'] == 'GMDB':
        destDf['gmdb_stepup'] = destDf['cosv']
    else:
        destDf['gmdb_stepup'] = '0.00'
    return destDf

#convert gmdb_rop
def equate_gmdb_rop(sourceDf):
    destDf = sourceDf
    
    inforce_by_cotg_list = {'Qqyy_P65_DIASPL_CQIssues',
                            'Qqyy_P65_P5_VANG_CQIssues', 
                            'Qqyy_P75_CQIssues', 
                            'Qqyy_P65_P5_VANG', 
                            'Qqyy_P65_ML_IVC', 
                            'Qqyy_P6_ML_Legacy', 
                            'Qqyy_P75',
                            'Qqyy_P65_DIASPL'}
    
    if destDf['policynumber'] == '101872E98' \
    and destDf['GMDBContractOptionTypeGroup'] == 'GMDB':  #Specifically for David Sun
        destDf['gmdb_rop'] = destDf['corop']
    elif destDf['inforceName'] in inforce_by_cotg_list \
    and destDf['GMDBContractOptionTypeGroup'] == 'GMDB':
        destDf['gmdb_rop'] = destDf['corop']
    else:
        destDf['gmdb_rop'] = '0.00'
    return destDf

#convert gmib_growth
def equate_gmib_growth(sourceDf):
    destDf = sourceDf
    
    inforce_by_cotg_list = {'Qqyy_P65_DIASPL_CQIssues',
                            'Qqyy_P65_P5_VANG_CQIssues', 
                            'Qqyy_P75_CQIssues', 
                            'Qqyy_P65_ML_IVC', 
                            'Qqyy_P65_DIASPL', 
                            'Qqyy_P75'}
    inforce_by_cotg_plus_list = {'Qqyy_P6_ML_Legacy'}
    
    if destDf['policynumber'] == '101872E98' \
    and destDf['GMIBContractOptionTypeGroup'] == 'GMIB':  #Specifically for David Sun
        destDf['gmib_growth'] = destDf['cogbv']
    elif destDf['inforceName'] in inforce_by_cotg_list \
    and destDf['GMIBContractOptionTypeGroup'] == 'GMIB':
        destDf['gmib_growth'] = destDf['cogbv']
    #This will be changed to account for contracttotalaccountvalue, currently unavailable or need to know correct value to pull
    elif destDf['inforceName'] in inforce_by_cotg_plus_list \
    and destDf['GMIBContractOptionTypeGroup'] == 'GMIB':
        if destDf['cogbv'] == '0':
            destDf['gmib_growth'] = destDf['i_acctvalue']
        else:
            destDf['gmib_growth'] == destDf['cogbv']
    else:
        destDf['gmib_growth'] = '0.00'
    return destDf

#convert gmib_ratchet
def equate_gmib_ratchet(sourceDf):
    destDf = sourceDf
    
    inforce_by_cotg_list = {'Qqyy_P65_DIASPL_CQIssues',
                            'Qqyy_P65_P5_VANG_CQIssues', 
                            'Qqyy_P75_CQIssues', 
                            'Qqyy_P65_ML_IVC', 
                            'Qqyy_P65_DIASPL', 
                            'Qqyy_P75'}
    inforce_by_cotg_plus_list = {'Qqyy_P6_ML_Legacy'}
    
    if destDf['policynumber'] == '101872E98' \
    and destDf['GMIBContractOptionTypeGroup'] == 'GMIB':  #Specifically for David Sun
        destDf['gmib_ratchet'] = destDf['cosv']
    elif destDf['inforceName'] in inforce_by_cotg_list \
    and destDf['GMIBContractOptionTypeGroup'] == 'GMIB':
        destDf['gmib_ratchet'] = destDf['cosv']
    #This will be changed to account for contracttotalaccountvalue, currently unavailable or need to know correct value to pull
    elif destDf['inforceName'] in inforce_by_cotg_plus_list \
    and destDf['GMIBContractOptionTypeGroup'] == 'GMIB': 
        if destDf['cosv'] == '0':
            destDf['gmib_ratchet'] = destDf['i_acctvalue']
        else:
            destDf['gmib_ratchet'] = destDf['cosv']
    else:
        destDf['gmib_growth'] = '0.00'
    return destDf

#convert jl_age
def equate_jl_age(sourceDf):
    destDf = sourceDf
    inforce_by_cotg_list = {'Qqyy_P65_DIASPL_CQIssues',
                            'Qqyy_P65_DIASPL', 
                            'Qqyy_P65_ML_IVC',
                            'Qqyy_P65_DavidSun'}
        
    inforce_by_cotg_plus_list = {'Qqyy_P65_P5_VANG_CQIssues',
                                'Qqyy_P65_P5_VANG'}
    
    if destDf['inforceName'] in inforce_by_cotg_list :
        destDf['jl_age'] = destDf['GMLBContractOptionSecondaryLifeIssueAge']
    elif destDf['inforceName'] in inforce_by_cotg_plus_list \
        and destDf['css_name'] == 'VantageP65':
        destDf['jl_age'] = destDf['GMLBContractOptionSecondaryLifeIssueAge']
    else:
        destDf['jl_age'] = '0'
    return destDf

#convert jl_sex
def equate_jl_sex(sourceDf):
    destDf = sourceDf
    
    inforce_by_cotg_list = {'Qqyy_P65_DIASPL_CQIssues',
                            'Qqyy_P65_DIASPL', 
                            'Qqyy_P65_ML_IVC',
                            'Qqyy_P65_DavidSun'}
    
    inforce_by_cotg_plus_list = {'Qqyy_P65_P5_VANG_CQIssues',
                                'Qqyy_P65_P5_VANG'}
        
    if destDf['inforceName'] in inforce_by_cotg_list :
        destDf['jl_sex'] = destDf['SPSGender']
    elif destDf['inforceName'] in inforce_by_cotg_plus_list \
        and destDf['css_name'] == 'VantageP65':
        destDf['jl_sex'] = destDf['SPSGender']
    else:
        destDf['jl_sex'] = 'M'
    return destDf
        
def equate_varrs(sourceDf, fee_conversion_date):
    destDf = sourceDf
    try:
        if destDf['varrs'] == 0:
            destDf['varrs'] = fund_fees_df.loc[fee_conversion_date].values.item(0)
        return destDf
    except KeyError:
        logger.debug("Warning: failed to find fee conversion date: " + fee_conversion_date)
        return destDf
    
def equate_varimf(sourceDf, fee_conversion_date):
    destDf = sourceDf
    
    inforce_by_cotg_list = {'Qqyy_P65_DIASPL_CQIssues',
                            'Qqyy_P65_DIASPL'}
    
    try:
        if destDf['varimf'] == 0:
            if destDf['inforceName'] in inforce_by_cotg_list :
                destDf['varimf'] = 0.002
            else:
                destDf['varimf'] = fund_fees_df.loc[fee_conversion_date].values.item(1)
        return destDf
    except KeyError:
        logger.debug("Warning: failed to find fee conversion date: " + fee_conversion_date)
        return destDf
    
#If file is P5_GEO, default WDYTDAIL, VarRS and WDAIL values to 0, no matter what.
#Plus, ck_Plan for P5_GEO must default to GEO1, fvqtr to 99
def default_values_for_p5_geo(sourceDf):
    destDf = sourceDf
    if destDf['inforceName'] == 'Qqyy_P5_GEO':
        destDf['accumwdail'] = '0'
        destDf['accumwdytdail'] = '0'
        destDf['varrs'] = '0'
        destDf['varimf'] = '0'
        destDf['dac_id'] = 'GEO'
        destDf['ck_plan'] = 'GEO1'
        destDf['fvqtr'] = '99'
    return destDf
    
class AlfaRules():
    
     #Constructor
    def __init__(self):
        self.logger = logging.getLogger('alfaLogger.AlfaRules')
        self.logger.debug('Creating an instance of AlfaRules')
    
    #Method for apply plan codes for ALFA
    def convert_plan_code(self, bsigDf):
        self.logger.debug('Executing convert_plan_code()')
        resultDf = bsigDf
        if 'ck_plan' in resultDf:
            resultDf['ck_plan'] = resultDf.apply(get_alfa_plan_code, axis=1)
        return resultDf
    
    #Method call for legal entity num
    def convert_legal_entity_num(self, alfaDf):
        self.logger.debug('Executing convert_legal_entity_num()')
        resultDf = alfaDf
        if 'ck_char2' in resultDf:
            resultDf = resultDf[resultDf['ck_char2'].notnull()]
            resultDf['ck_char2'] = resultDf['ck_char2'].apply(get_alfa_legal_entity_num)
        return resultDf
    
    #Method call for convert glb_sj
    def convert_glb_sj(self, alfaDf):
        self.logger.debug('Executing convert_glb_sj()')
        resultDf = alfaDf
        if 'glb_sj' in resultDf:
            resultDf = resultDf.apply(equate_glb_sj, axis=1)
        return resultDf
    
    #Method call for convert gmdb_growth
    def convert_gmdb_growth(self, alfaDf):
        self.logger.debug('Executing convert_gmdb_growth()')
        resultDf = alfaDf
        if 'gmdb_growth' in resultDf:
            resultDf = resultDf.apply(equate_gmdb_growth, axis=1)
        return resultDf
    
    #Method call for convert gmdb_step_up
    def convert_gmdb_stepup(self, alfaDf):
        self.logger.debug('Executing convert_gmdb_stepup()')
        resultDf = alfaDf
        if 'gmdb_stepup' in resultDf:
            resultDf = resultDf.apply(equate_gmdb_stepup, axis=1)
        return resultDf
    
    #Method call for convert gmdb_rop
    def convert_gmdb_rop(self, alfaDf):
        self.logger.debug('Executing convert_gmdb_rop()')
        resultDf = alfaDf
        if 'gmdb_rop' in resultDf:
            resultDf = resultDf.apply(equate_gmdb_rop, axis=1)
        return resultDf
    
    #Method call for convert gmib_growth
    def convert_gmib_growth(self, alfaDf):
        self.logger.debug('Executing convert_gmib_growth()')
        resultDf = alfaDf
        if 'gmib_growth' in resultDf:
            resultDf = resultDf.apply(equate_gmib_growth, axis=1)
        return resultDf
    
    #Method call for convert gmib_ratchet
    def convert_gmib_ratchet(self, alfaDf):
        self.logger.debug('Executing convert_gmib_ratchet()')
        resultDf = alfaDf
        if 'gmib_ratchet' in resultDf:
            resultDf = resultDf.apply(equate_gmib_ratchet, axis=1)
        return resultDf
    
    #P5 GEO files have select values that default to zero
    def convert_geo_values(self, alfaDf):
        self.logger.debug('Executing convert_geo_values()')
        resultDf = alfaDf
        if {'accumwdail', 'accumwdytdail', 'varrs', 'varimf', 'dac_id', 'ck_plan', 'fvqtr'}.issubset(resultDf.columns):
            resultDf = resultDf.apply(default_values_for_p5_geo, axis=1)
        return resultDf 

    #Method call for convert jl_age
    def convert_jl_age(self, alfaDf):
        self.logger.debug('Executing convert_jl_age()')
        resultDf = alfaDf
        if 'jl_age' in resultDf:
            resultDf = resultDf.apply(equate_jl_age, axis=1)
        return resultDf
    
    #Method call for convert jl_sex
    def convert_jl_sex(self, alfaDf):
        self.logger.debug('Executing convert_jl_sex()')
        resultDf = alfaDf
        if 'jl_sex' in resultDf:
            resultDf = resultDf.apply(equate_jl_sex, axis=1)
        return resultDf

    #Method call for convert varrs
    def convert_varrs(self, alfaDf, fee_conversion_date):
        self.logger.debug('Executing convert_varrs()')
        resultDf = alfaDf
        if 'varrs' in resultDf:
            resultDf = resultDf.apply(equate_varrs, fee_conversion_date=fee_conversion_date, axis=1)
        return resultDf
        
    #Method call for convert varimf
    def convert_varimf(self, alfaDf, fee_conversion_date):
        self.logger.debug('Executing convert_varimf()')
        resultDf = alfaDf
        if 'varimf' in resultDf:
            resultDf = resultDf.apply(equate_varimf, fee_conversion_date=fee_conversion_date, axis=1)
        return resultDf
    
    #High level method for applying all rules
    def apply_rules(self, sourceDf):
        self.logger.debug('Executing apply_rules()')
        
        # date for fees conversion
        fee_conversion_date = '2018-07-31 00:00:00'

        destDf = sourceDf
        if ('ck_plan' in destDf) and (len(destDf.index) != 0):
            destDf = self.convert_plan_code(destDf)
            destDf = self.convert_legal_entity_num(destDf)
            destDf = self.convert_glb_sj(destDf)
            destDf = self.convert_gmdb_growth(destDf)
            destDf = self.convert_gmdb_stepup(destDf)
            destDf = self.convert_gmdb_rop(destDf)
            destDf = self.convert_gmib_growth(destDf)
            destDf = self.convert_gmib_ratchet(destDf)
            destDf = self.convert_jl_age(destDf)
            destDf = self.convert_jl_sex(destDf)
            destDf = self.convert_varrs(destDf, fee_conversion_date)
            destDf = self.convert_varimf(destDf, fee_conversion_date)
            #File Specific Overrides
            destDf = self.convert_geo_values(destDf)
        return destDf 