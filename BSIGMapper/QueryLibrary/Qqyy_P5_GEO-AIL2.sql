inner join (select distinct Contract.contractnumber as GEOContractNumber from Contract
            left join contractoptionfund 
                on contract.ContractNumber = contractoptionfund.ContractNumber
            where contract.SourceSystemName in ('VantageP5')
                and contract.ContractStatusCode in ('A', 'E', 'G', 'Q', 'Z', '3', '4', '5')
                and nvl(contractoptionfundclassindicator, '') <> 'L'
                and contractoptionfundnumber in ('013','023','064','094') ) geo on ContractNumber = GEOContractNumber