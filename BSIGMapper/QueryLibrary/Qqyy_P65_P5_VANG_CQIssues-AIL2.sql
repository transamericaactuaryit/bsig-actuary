ContractOptionStatusCode in ('A', 'U', 'I')
and SourceSystemName in ('VantageP65', 'VantageP5', 'Vanguard')
and ContractStatusCode in ('A', 'E', 'G', 'Q', 'Z', '3', '4', '5')
and nvl(ContractNumber, 'NULL') not in ('101872E98')
and ContractNumber not like '%IVC%' 
and nvl(ContractOptionFundClassIndicator, 'NULL') <> 'L'
and centernumber <> '03528A00'
and centernumber not like '006B%'
and nvl(contractoptionfundnumber, 'NULL') not in ('013','023','064','094')
-- and nvl(ContractPayoutOptionIndicator1, 'NULL') not in ('G','N')
-- and nvl(ContractPayoutOptionIndicator2, 'NULL') not in ('G','N')