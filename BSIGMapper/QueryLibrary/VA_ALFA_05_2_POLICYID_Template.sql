SELECT 
ContractNumber as contractnumber,
ContractNumberNumericID as I_PolNum,
SourceSystemName as css_name, 
--For calculating GMB_Growth, GMDB_StepUp, GMDB_ROP, GMIB_Growth, GMIB_Ratcher, and glb_sj
contractoptiongrowthbasevalue as cogbv,
contractoptionstepvalue as cosv,
contractoptionreturnofpremiumvalue as corop,
GMIBContractOptionSingleJointIndicator as GMIBContractOptionSingleJointIndicator,
GMLBContractOptionSingleJointIndicator as GMLBContractOptionSingleJointIndicator,
case when rtrim(cast(GMLBContractOptionSecondaryLifeIssueAge as string)) = '' then '0' else nvl(GMLBContractOptionSecondaryLifeIssueAge, '0') end as GMLBContractOptionSecondaryLifeIssueAge,
case when rtrim(SPSGender) = '' then 'M' else nvl(SPSGender, "M") end as SPSGender,
--End calculation
GMIBContractOptionTypeGroup, 
GMDBContractOptionTypeGroup,
GMWBContractOptionTypeGroup,
GMLBContractOptionTypeGroup,
TXPRContractOptionTypeGroup 
from (select PlanCode, ContractIssueAge,ContractModelingBusinessUnitGroup,SourceLegalEntityCode,ContractQualifiedIndicator,
            ContractModelingReinsuranceIdentifier,ContractOptionModelingPWWBSegment,ContractIssueYear,ContractIssueMonth,
            ContractTotalAccountValue,ContractPayoutAmount1,ContractPayoutAmount2,ContractSurrenderValueAmount,ContractInitialPremium,
            ContractModelingRenewalPremium,ContractCumulativeWithdrawals,StateCode,ContractDACGroup,ContractStatusCode,ContractNumberNumericID,
            ContractNumber, ContractChargeRemaining,ContractChargePercent,ContractChargeAmortizationRemainingTerm,
            ContractPolicyYeartoDateCumulativeWithdrawals, SourceSystemName, ContractOptionStatusCode, 
            ContractOptionGrowthBaseValue, ContractOptionStepValue, ContractOptionReturnOfPremiumValue, contractoptiondurationinmonths, 
            ContractOptionFundClassIndicator, CenterNumber, contractoptionfundnumber,
row_number() over (Partition by PlanCode, ContractIssueAge,ContractModelingBusinessUnitGroup,SourceLegalEntityCode,ContractQualifiedIndicator,
                    ContractModelingReinsuranceIdentifier,ContractOptionModelingPWWBSegment,ContractIssueYear,ContractIssueMonth,
                    ContractTotalAccountValue,ContractSurrenderValueAmount,ContractInitialPremium,ContractModelingRenewalPremium,
                    ContractCumulativeWithdrawals,StateCode,ContractDACGroup,ContractStatusCode,ContractNumberNumericID,
                    ContractNumber, ContractChargeRemaining,ContractChargePercent,ContractChargeAmortizationRemainingTerm,
                    ContractPolicyYeartoDateCumulativeWithdrawals, SourceSystemName, CenterNumber order by pointofviewstopdate, pointofviewstartdate desc) UniqueContractRecord
from financebusinessviewsmodel.actuarial_historical
where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()) ah
--PAR GENDER
    left join
    (select ContractNumber as PARContractNumber,
    Gender as PARGender,
    row_number() over (Partition by ContractNumber order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where partysourcerelationshiptypecode = 'PAR'
    and pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()) par on ah.ContractNumber = PARContractNumber and par.rownum = 1
--SPS GENDER
    left join
    (select ContractNumber as SPSContractNumber,
    gender as SPSGender,
    row_number() over (Partition by ContractNumber order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where partysourcerelationshiptypecode = 'SPS'
    and ContractOptionTypeGroup = 'GMLB'
    and pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()) sps on ah.ContractNumber = SPSContractNumber and sps.rownum = 1
--SAFE
    left join
    (select ContractNumber as SAFEContractNumber,
    sum(contractoptionfundsafefundvalue) as safecontractoptionfundsafefundvalue,
    row_number() over (Partition by ContractNumber order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE() and fundclass = 'SAFE'
    group by ContractNumber, pointofviewstopdate, pointofviewstartdate) safe on ah.ContractNumber = SAFEContractNumber and safe.rownum = 1
--contractoptioninvestmentmethodindicator
    left join
    (select ContractNumber as InvestmentMethodContractNumber,
    contractoptioninvestmentmethodindicator as OAM_ID,
    row_number() over (Partition by ContractNumber order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE() and contractoptionsourcesystemordinalposition = 1
    ) InvestmentMethod on ah.ContractNumber = InvestmentMethodContractNumber and InvestmentMethod.rownum = 1
--VAR 
    left join
    (select ContractNumber as VARContractNumber, 
    SUM(contractoptionfundbundledrevenueshareamount) as VarRS,
    SUM(ContractOptionFundBundledInvestmentManagementFeeAmount) as VarIMF
    from financebusinessviewsmodel.actuarial_historical 
    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE() 
    and ContractOptionFundClassIndicator = 'V'
    group by ContractNumber
    ) VAR on ah.ContractNumber = VARContractNumber
--NON-VAR
--    left join
--    (select ContractNumber as NONVARContractNumber, 
--    SumSAV/ as VarDefault
--    from financebusinessviewsmodel.actuarial_historical ah
--       INNER JOIN (select ContractNumber, SUM(ContractFundSeperateAccountValue) as SumSAV from financebusinessviewsmodel.actuarial_historical                
--                    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()  
--                    group by ContractNumber
--                    ) SAV
--        on ah.ContractNumber = SAV.ContractNumber
--    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE() 
--    and ContractOptionFundClassIndicator = 'V'
--    group by ContractNumber
--    ) VAR on ah.ContractNumber = NONVARContractNumber
--SPO
    left join
    (select ContractNumber as SPOContractNumber, 
--SPO_ind
    case when nvl(ContractPayoutOptionIndicator2, '') <> '' then
        case 
            when ContractPayoutOptionIndicator1 = ContractPayoutOptionIndicator2
                then ContractPayoutOptionIndicator1
            when ContractPayoutOptionIndicator1 in ('6','7') 
                or ContractPayoutOptionIndicator2 in ('6','7') 
                then 'X'
            when ContractPayoutOptionIndicator1 <> ContractPayoutOptionIndicator2
                then 'Z'
        end
        else ContractPayoutOptionIndicator1
        end as SPO_ind,
--SPO_PWD
    case when nvl(ContractPayoutPartialWithdrawalMethod2, '') <> '' then 
         case 
            when ContractPayoutPartialWithdrawalMethod1 = ContractPayoutPartialWithdrawalMethod2 
                then ContractPayoutPartialWithdrawalMethod1
            when ContractPayoutPartialWithdrawalMethod1 <> ContractPayoutPartialWithdrawalMethod2 
                then
                    case 
                        when ContractPayoutPartialWithdrawalMethod1 > ContractPayoutPartialWithdrawalMethod2 
                            then ContractPayoutPartialWithdrawalMethod1
                        else ContractPayoutPartialWithdrawalMethod2
                    end
            when ContractPayoutPartialWithdrawalMethod1 >= ContractPayoutPartialWithdrawalMethod2 then
                 ContractPayoutPartialWithdrawalMethod1
            else ContractPayoutPartialWithdrawalMethod2
        end
        else ContractPayoutPartialWithdrawalMethod1
        end as SPO_PWD,
--SPO_StartYr
    case when nvl(ContractPayoutStartYear2, '') <> '' then
        case 
            when ContractPayoutStartYear1 = ContractPayoutStartYear2 
                then ContractPayoutStartYear1
            when ContractPayoutStartYear1 > ContractPayoutStartYear2 
                then ContractPayoutStartYear1   
            else ContractPayoutStartYear2
        end
    else ContractPayoutStartYear1
    end as SPO_StartYr, 
--SPO_StartMo
    case when nvl(ContractPayoutStartMonth2, '') <> '' then
            case 
                when ContractPayoutStartMonth1 = ContractPayoutStartMonth2 
                    then ContractPayoutStartMonth1
                when ContractPayoutStartMonth1 > ContractPayoutStartMonth2 
                    then ContractPayoutStartMonth1   
                else ContractPayoutStartMonth2
            end
        else ContractPayoutStartMonth1
        end as SPO_StartMo,

    row_number() over (Partition by ContractNumber, contractpayoutoptionindicator1, contractpayoutoptionindicator2, 
                        ContractPayoutMode1, ContractPayoutMode2, ContractPayoutAmount1, ContractPayoutAmount2,
                        contractpayoutmemocode1, contractpayoutmemocode2, ContractPayoutStartYear1, ContractPayoutStartYear2, 
                        ContractPayoutStartMonth1, ContractPayoutStartMonth2 order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE() 
    ) SPO on ah.ContractNumber = SPOContractNumber and SPO.rownum = 1 
--SPO_Mode
    left join --Done
        (select ContractNumber as SPOModeContractNumber,
        case when nvl(ContractPayoutMode2, '') <> '' then
                case 
                    when ContractPayoutMode1 in ('6', '7')
                        or ContractPayoutMode2 in ('6', '7')
                        then 1
                    when ContractPayoutMode1 <> ContractPayoutMode2
                        and ContractPayoutStartDate1 > ContractPayoutStartDate2
                        then ContractPayoutMode2
                end
        else ContractPayoutMode1
        end as SPO_Mode,
    row_number() over (Partition by ContractNumber, ContractPayoutMode1, ContractPayoutMode2
                        order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE() 
    ) SPO_mode_table on ah.ContractNumber = SPOModeContractNumber and SPO_mode_table.rownum = 1 
--GMWB
    left join --Done
    (select ContractNumber as GMWBContractNumber,
    ContractOptionTypeGroup as GMWBContractOptionTypeGroup, 
    ContractOptionModelingTypeGroupIndicator as GMWBContractOptionModelingTypeGroupIndicator,  
   row_number() over (Partition by ContractNumber order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where ContractOptionTypeGroup = 'GMWB'
    and pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()) gmwb on ah.ContractNumber = GMWBContractNumber and gmwb.rownum = 1
--GMDB
    left join --Done
    (select ContractNumber as GMDBContractNumber,
    ContractOptionTypeGroup as GMDBContractOptionTypeGroup, 
    ContractOptionModelingTypeGroupIndicator as GMDBContractOptionModelingTypeGroupIndicator,  
    ContractOptionInvestmentMethodIndicator as GMDBContractOptionInvestmentMethodIndicator,
   row_number() over (Partition by ContractNumber order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where ContractOptionTypeGroup = 'GMDB'
    and pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()) gmdb on ah.ContractNumber = GMDBContractNumber and gmdb.rownum = 1
--GMIB
    left join --Done
    (select ContractNumber as GMIBContractNumber,
    ContractOptionTypeGroup as GMIBContractOptionTypeGroup, 
    case when rtrim(ContractOptionSingleJointIndicator) = '' then 'S' else nvl(ContractOptionSingleJointIndicator,'S') end as GMIBContractOptionSingleJointIndicator,
    ContractOptionModelingTypeGroupIndicator as GMIBContractOptionModelingTypeGroupIndicator,  
    ContractOptionFeePercentage as GMIBContractOptionFeePercentage,
   row_number() over (Partition by ContractNumber order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where ContractOptionTypeGroup = 'GMIB'
    and pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()) gmib on ah.ContractNumber = GMIBContractNumber and gmib.rownum = 1
--GMLB
    left join --Done
    (select ContractNumber as GMLBContractNumber,
    ContractOptionTypeGroup as GMLBContractOptionTypeGroup, 
    ContractOptionModelingTypeGroupIndicator as GMLBContractOptionModelingTypeGroupIndicator,
    ContractOptionPrincipalBackWithdrawalBaseValue as PB_TWB,
    ContractOptionPrincipalBackRemainingWithdrawalAmount as PB_RWA,
    ContractOptionBenefitPrincipalBackFreeAmountRemaining as PB_FREE,
    ContractOptionLifetimeWithdrawalBaseValue as LF_TWB,
    ContractOptionLifetimeRemainingWithdrawalAmount as LF_RWA,
    ContractOptionBenefitLifetimeFreeAmountRemaining as LF_FREE,
    case when rtrim(cast(ContractOptionLifetimeTotalFreePercentage as string)) = '' then 0 else nvl(ContractOptionLifetimeTotalFreePercentage,0) * 100 end as LF_FREE_PCT,
    ContractOptionGuaranteedFutureValue as GMAB_GFV,
    ContractOptionHighMonthiversaryValue as HiMnthly_Val,
    ContractOptionFeePercentage as GMLBContractOptionFeePercentage, 
    case when rtrim(ContractOptionSingleJointIndicator) = '' then 'S' else nvl(ContractOptionSingleJointIndicator,'S') end as GMLBContractOptionSingleJointIndicator,
    nvl(ContractOptionDeathIndicator,'N') as glb_db,
    contractoptionincomeenhancementindicator as GLB_NH,
    ContractOptionGenerationID as RIDER_GEN_ID,
    ContractOptionFutureFeePercentage as GLB_RDR_FUTURE_FEE,
    lpad(nvl(ContractOptionSecondaryLifeIssueAge, '0'),3,'0') as GMLBContractOptionSecondaryLifeIssueAge,
    ContractOptionBenefitJointDeathIndicator as JL_STATUS_IND,
    ContractOptionWithdrawalMultiplierBenefitAmount as GMWBBonusFaceAIL,
    ContractOptionBenefitStopIndicator as GMWBGrowthStop,
    ContractOptionBenefitStopDate as GMWBGrowthStopYr,
    '' as GMWBMAWALockYr,--ContractOptionBenefitSelectionDate
    case when rtrim(cast(ContractOptionLifetimeTotalFreePercentage as string)) = '' then 0 else nvl(ContractOptionLifetimeTotalFreePercentage, 0) * 100 end as BASE_LF_FREE_PCT,
    row_number() over (Partition by ContractNumber order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where ContractOptionTypeGroup = 'GMLB'
    and pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()) gmlb on ah.ContractNumber = GMLBContractNumber and gmlb.rownum = 1 
--TXPR	
    left join --Done
    (select ContractNumber as TXPRContractNumber,
    ContractOptionTypeGroup as TXPRContractOptionTypeGroup, 
    ContractOptionTypeGroup as TXPRContractOptionModelingTypeGroupIndicator,
    ContractOptionModelingTypeGroupIndicator as ck_SmkStatus,
    ContractOptionBenefitFactor as EEB_BenPct,
    ContractOptionDurationInMonths as EEB_Dur,
    ContractOptionTaxableEarnings as EEB_Basis,
    ContractOptionBenefitAccumFeePayment as EEB_AcumFees,
    ContractOptionTaxableInitialEarnings as EEB_InitBasis,
    row_number() over (Partition by ContractNumber order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from financebusinessviewsmodel.actuarial_historical 
    where ContractOptionTypeGroup = 'TXPR'
    and pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()) txpr on ah.ContractNumber = TXPRContractNumber and txpr.rownum = 1
----SHFSA
left join 
    (select
    contractnumber as SHFSAContractNumber,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '1' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA1,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '2' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA2,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '3' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA3,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '4' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA4,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '5' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA5,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '6' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA6,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '7' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA7,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '8' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA8,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '9' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA9,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '10' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA10
    from financebusinessviewsmodel.actuarial_historical 
    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()
    group by contractnumber) shfsa on ah.ContractNumber = SHFSAContractNumber
--SA
left join 
    (select ContractNumber as SAContractNumber,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex1 ,0)) SA1,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex2 ,0)) SA2,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex3 ,0)) SA3,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex4 ,0)) SA4,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex5 ,0)) SA5,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex6 ,0)) SA6,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex7 ,0)) SA7,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex8 ,0)) SA8
    from financebusinessviewsmodel.actuarial_historical 
    where pointofviewstartdate <= CURRENT_DATE() 
    and pointofviewstopdate >= CURRENT_DATE() 
    group by contractnumber) sa on ah.ContractNumber = SAContractNumber
--DYNSA
left join 
    (select contractnumber as DYNSAContractNumber,
    SUM(case when FundDynamicSeparateAccountFlag = '1' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA1,
    SUM(case when FundDynamicSeparateAccountFlag = '2' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA2,
    SUM(case when FundDynamicSeparateAccountFlag = '3' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA3,
    SUM(case when FundDynamicSeparateAccountFlag = '4' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA4,
    SUM(case when FundDynamicSeparateAccountFlag = '5' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA5,
    SUM(case when FundDynamicSeparateAccountFlag = '6' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA6,
    SUM(case when FundDynamicSeparateAccountFlag = '7' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA7,
    SUM(case when FundDynamicSeparateAccountFlag = '8' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA8,
    SUM(case when FundDynamicSeparateAccountFlag = '9' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA9,
    SUM(case when FundDynamicSeparateAccountFlag = '10' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA10
    from financebusinessviewsmodel.actuarial_historical 
    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()
    group by contractnumber ) DYNSA  on ah.ContractNumber = DYNSAContractNumber
-- End of Sub-Queries 

where UniqueContractRecord = 1 