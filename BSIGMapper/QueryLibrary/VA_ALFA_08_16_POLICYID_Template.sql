SELECT 
ContractNumber as contractnumber,
ContractNumberNumericID as I_PolNum,
GMIBContractOptionTypeGroup, 
GMDBContractOptionTypeGroup,
GMWBContractOptionTypeGroup,
GMLBContractOptionTypeGroup,
TXPRContractOptionTypeGroup 
from Contract
--PAR GENDER
    left join
    (select ContractNumber as PARContractNumber,
    Gender as PARGender
    from party
    where partysourcerelationshiptypecode = 'PAR') par on Contract.ContractNumber = PARContractNumber 
--SPS GENDER
    left join
    (select ContractOption.ContractNumber as SPSContractNumber,  
        sps.Gender as SPSGender
        from ContractOption
        left join (select ContractNumber, gender
                        from party
                        where partysourcerelationshiptypecode = 'SPS'
                        ) sps on ContractOption.ContractNumber = sps.ContractNumber 
        where ContractOptionTypeGroup = 'GMLB'
    ) sps on Contract.ContractNumber = SPSContractNumber
--SAFE
    left join
    (select ContractNumber as SAFEContractNumber,
    sum(contractoptionfundsafefundvalue) as safecontractoptionfundsafefundvalue 
    from contractoptionfund
    group by ContractNumber) safe on Contract.ContractNumber = SAFEContractNumber
--contractoptioninvestmentmethodindicator
    left join
    (select distinct ContractNumber as InvestmentMethodContractNumber,
    contractoptioninvestmentmethodindicator as OAM_ID
    from contractoption 
    ) InvestmentMethod on Contract.ContractNumber = InvestmentMethodContractNumber
--VAR 
    left join
    (select ContractNumber as VARContractNumber, 
    SUM(contractoptionfundbundledrevenueshareamount) as VarRS,
    SUM(ContractOptionFundBundledInvestmentManagementFeeAmount) as VarIMF
    from ContractOptionFund
    where ContractOptionFundClassIndicator = 'V'
    group by ContractNumber
    ) VAR on Contract.ContractNumber = VARContractNumber
--NON-VAR
--    left join
--    (select ContractNumber as NONVARContractNumber, 
--    SumSAV/ as VarDefault
--    from financebusinessviewsmodel.actuarial_historical ah
--       INNER JOIN (select ContractNumber, SUM(ContractFundSeperateAccountValue) as SumSAV from financebusinessviewsmodel.actuarial_historical                
--                    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()  
--                    group by ContractNumber
--                    ) SAV
--        on ah.ContractNumber = SAV.ContractNumber
--    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE() 
--    and ContractOptionFundClassIndicator = 'V'
--    group by ContractNumber
--    ) VAR on ah.ContractNumber = NONVARContractNumber
--SPO
    left join
    (select ContractNumber as SPOContractNumber, 
--SPO_ind
    case when nvl(ContractPayoutOptionIndicator2, '') <> '' then
        case 
            when ContractPayoutOptionIndicator1 = ContractPayoutOptionIndicator2
                then ContractPayoutOptionIndicator1
            when ContractPayoutOptionIndicator1 in ('6','7') 
                or ContractPayoutOptionIndicator2 in ('6','7') 
                then 'X'
            when ContractPayoutOptionIndicator1 <> ContractPayoutOptionIndicator2
                then 'Z'
        end
        else ContractPayoutOptionIndicator1
        end as SPO_ind,
--SPO_PWD
    case when nvl(ContractPayoutPartialWithdrawalMethod2, '') <> '' then 
         case 
            when ContractPayoutPartialWithdrawalMethod1 = ContractPayoutPartialWithdrawalMethod2 
                then ContractPayoutPartialWithdrawalMethod1
            when ContractPayoutPartialWithdrawalMethod1 <> ContractPayoutPartialWithdrawalMethod2 
                then
                    case 
                        when ContractPayoutPartialWithdrawalMethod1 > ContractPayoutPartialWithdrawalMethod2 
                            then ContractPayoutPartialWithdrawalMethod1
                        else ContractPayoutPartialWithdrawalMethod2
                    end
            when ContractPayoutPartialWithdrawalMethod1 >= ContractPayoutPartialWithdrawalMethod2 then
                 ContractPayoutPartialWithdrawalMethod1
            else ContractPayoutPartialWithdrawalMethod2
        end
        else ContractPayoutPartialWithdrawalMethod1
        end as SPO_PWD,
--SPO_StartYr
    case when nvl(ContractPayoutStartYear2, '') <> '' then
        case 
            when ContractPayoutStartYear1 = ContractPayoutStartYear2 
                then ContractPayoutStartYear1
            when ContractPayoutStartYear1 > ContractPayoutStartYear2 
                then ContractPayoutStartYear1   
            else ContractPayoutStartYear2
        end
    else ContractPayoutStartYear1
    end as SPO_StartYr, 
--SPO_StartMo
    case when nvl(ContractPayoutStartMonth2, '') <> '' then
            case 
                when ContractPayoutStartMonth1 = ContractPayoutStartMonth2 
                    then ContractPayoutStartMonth1
                when ContractPayoutStartMonth1 > ContractPayoutStartMonth2 
                    then ContractPayoutStartMonth1   
                else ContractPayoutStartMonth2
            end
        else ContractPayoutStartMonth1
        end as SPO_StartMo 
    from Contract
    ) SPO on Contract.ContractNumber = SPOContractNumber
--SPO_Mode
    left join --Done
        (select ContractNumber as SPOModeContractNumber,
        case when nvl(ContractPayoutMode2, '') <> '' then
                case 
                    when ContractPayoutMode1 in ('6', '7')
                        or ContractPayoutMode2 in ('6', '7')
                        then 1
                    when ContractPayoutMode1 <> ContractPayoutMode2
                        and ContractPayoutStartDate1 > ContractPayoutStartDate2
                        then ContractPayoutMode2
                end
        else ContractPayoutMode1
        end as SPO_Mode,
    row_number() over (Partition by ContractNumber, ContractPayoutMode1, ContractPayoutMode2
                        order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from contract 
    ) SPO_mode_table on Contract.ContractNumber = SPOModeContractNumber and SPO_mode_table.rownum = 1 
--GMWB
    left join --Done
    (select ContractNumber as GMWBContractNumber,
    ContractOptionTypeGroup as GMWBContractOptionTypeGroup, 
    ContractOptionModelingTypeGroupIndicator as GMWBContractOptionModelingTypeGroupIndicator
    from ContractOption
    where ContractOptionTypeGroup = 'GMWB'
    ) gmwb on Contract.ContractNumber = GMWBContractNumber
--GMDB
    left join --Done
    (select ContractNumber as GMDBContractNumber,
    ContractOptionTypeGroup as GMDBContractOptionTypeGroup, 
    ContractOptionGrowthBaseValue as GMDBContractOptionGrowthBaseValue,
    ContractOptionStepValue as GMDBContractOptionStepValue,
    ContractOptionReturnOfPremiumValue as GMDBContractOptionReturnOfPremiumValue,
    ContractOptionModelingTypeGroupIndicator as GMDBContractOptionModelingTypeGroupIndicator,
    ContractOptionInvestmentMethodIndicator as GMDBContractOptionInvestmentMethodIndicator
    from ContractOption
    where ContractOptionTypeGroup = 'GMDB') gmdb on Contract.ContractNumber = GMDBContractNumber
--GMIB
    left join --Done
    (select ContractNumber as GMIBContractNumber,
    ContractOptionTypeGroup as GMIBContractOptionTypeGroup, 
    ContractOptionGrowthBaseValue as GMIBContractOptionGrowthBaseValue,
    ContractOptionStepValue as GMIBContractOptionStepValue,
    case when rtrim(ContractOptionSingleJointIndicator) = '' then 'S' else nvl(ContractOptionSingleJointIndicator,'S') end as GMIBContractOptionSingleJointIndicator,
    ContractOptionDurationInMonths as GMIBContractOptionDurationInMonths,
    ContractOptionModelingTypeGroupIndicator as GMIBContractOptionModelingTypeGroupIndicator,
    ContractOptionFeePercentage as GMIBContractOptionFeePercentage
    from ContractOption
    where ContractOptionTypeGroup = 'GMIB'
    ) gmib on Contract.ContractNumber = GMIBContractNumber
--GMLB
    left join --Done
    (select ContractNumber as GMLBContractNumber,
    ContractOptionTypeGroup as GMLBContractOptionTypeGroup, 
    ContractOptionModelingTypeGroupIndicator as GMLBContractOptionModelingTypeGroupIndicator,
    ContractOptionModelingPWWBSegment as GMLBContractOptionModelingPWWBSegment,
    ContractOptionPrincipalBackWithdrawalBaseValue as PB_TWB,
    ContractOptionPrincipalBackRemainingWithdrawalAmount as PB_RWA,
    ContractOptionBenefitPrincipalBackFreeAmountRemaining as PB_FREE,
    ContractOptionLifetimeWithdrawalBaseValue as LF_TWB,
    ContractOptionLifetimeRemainingWithdrawalAmount as LF_RWA,
    ContractOptionBenefitLifetimeFreeAmountRemaining as LF_FREE,
    case when rtrim(cast(ContractOptionLifetimeTotalFreePercentage as string)) = '' then 0 else nvl(ContractOptionLifetimeTotalFreePercentage,0) * 100 end as LF_FREE_PCT,
    ContractOptionGuaranteedFutureValue as GMAB_GFV,
    ContractOptionHighMonthiversaryValue as HiMnthly_Val,
    ContractOptionFeePercentage as GMLBContractOptionFeePercentage, 
    case when rtrim(ContractOptionSingleJointIndicator) = '' then 'S' else nvl(ContractOptionSingleJointIndicator,'S') end as GMLBContractOptionSingleJointIndicator,
    nvl(ContractOptionDeathIndicator,'N') as glb_db,
    contractoptionincomeenhancementindicator as GLB_NH,
    ContractOptionGenerationID as RIDER_GEN_ID,
    ContractOptionFutureFeePercentage as GLB_RDR_FUTURE_FEE,
    lpad(nvl(ContractOptionSecondaryLifeIssueAge, '0'),3,'0') as GMLBContractOptionSecondaryLifeIssueAge,
    ContractOptionBenefitJointDeathIndicator as JL_STATUS_IND,
    ContractOptionWithdrawalMultiplierBenefitAmount as GMWBBonusFaceAIL,
    ContractOptionBenefitStopIndicator as GMWBGrowthStop,
    ContractOptionBenefitStopDate as GMWBGrowthStopYr,
    '' as GMWBMAWALockYr,--ContractOptionBenefitSelectionDate
    case when rtrim(cast(ContractOptionLifetimeTotalFreePercentage as string)) = '' then 0 else nvl(ContractOptionLifetimeTotalFreePercentage, 0) * 100 end as BASE_LF_FREE_PCT
    from ContractOption
    where ContractOptionTypeGroup = 'GMLB') gmlb on Contract.ContractNumber = GMLBContractNumber
--TXPR	
    left join --Done
    (select ContractNumber as TXPRContractNumber,
    ContractOptionTypeGroup as TXPRContractOptionTypeGroup, 
    ContractOptionTypeGroup as TXPRContractOptionModelingTypeGroupIndicator,
    ContractOptionModelingTypeGroupIndicator as ck_SmkStatus,
    ContractOptionBenefitFactor as EEB_BenPct,
    ContractOptionDurationInMonths as EEB_Dur,
    ContractOptionTaxableEarnings as EEB_Basis,
    ContractOptionBenefitAccumFeePayment as EEB_AcumFees,
    ContractOptionTaxableInitialEarnings as EEB_InitBasis
    from ContractOption
    where ContractOptionTypeGroup = 'TXPR'
    ) txpr on Contract.ContractNumber = TXPRContractNumber
----SHFSA
left join 
    (select
    contractnumber as SHFSAContractNumber,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '1' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA1,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '2' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA2,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '3' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA3,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '4' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA4,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '5' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA5,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '6' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA6,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '7' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA7,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '8' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA8,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '9' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA9,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '10' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA10
    from Fund
        inner join ContractOptionFund
            on Fund.FundNumber = ContractOptionFund.ContractOptionFundNumber
            and Fund.fundregion = ContractOptionFund.ContractAdministrationLocationCode
    group by contractnumber) shfsa on Contract.ContractNumber = SHFSAContractNumber
--SA
left join 
    (select ContractNumber as SAContractNumber,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex1 ,0)) SA1,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex2 ,0)) SA2,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex3 ,0)) SA3,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex4 ,0)) SA4,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex5 ,0)) SA5,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex6 ,0)) SA6,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex7 ,0)) SA7,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex8 ,0)) SA8
    from ContractOptionFund
    group by contractnumber) sa on Contract.ContractNumber = SAContractNumber
--DYNSA
left join 
    (select contractnumber as DYNSAContractNumber,
    SUM(case when FundDynamicSeparateAccountFlag = '1' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA1,
    SUM(case when FundDynamicSeparateAccountFlag = '2' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA2,
    SUM(case when FundDynamicSeparateAccountFlag = '3' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA3,
    SUM(case when FundDynamicSeparateAccountFlag = '4' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA4,
    SUM(case when FundDynamicSeparateAccountFlag = '5' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA5,
    SUM(case when FundDynamicSeparateAccountFlag = '6' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA6,
    SUM(case when FundDynamicSeparateAccountFlag = '7' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA7,
    SUM(case when FundDynamicSeparateAccountFlag = '8' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA8,
    SUM(case when FundDynamicSeparateAccountFlag = '9' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA9,
    SUM(case when FundDynamicSeparateAccountFlag = '10' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA10
    from Fund
        inner join ContractOptionFund
            on Fund.FundNumber = ContractOptionFund.ContractOptionFundNumber
            and Fund.fundregion = ContractOptionFund.ContractAdministrationLocationCode
    group by contractnumber) DYNSA  on Contract.ContractNumber = DYNSAContractNumber
    