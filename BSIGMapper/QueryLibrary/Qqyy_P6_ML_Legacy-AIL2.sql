inner join (select distinct Contract.contractnumber as P6MLContractNumber from Contract
            left join contractoption
                on Contract.contractnumber = contractoption.contractnumber
                and contractoption.ContractOptionStatusCode in ('A', 'U', 'I') 
            left join contractoptionfund 
                on contract.ContractNumber = contractoptionfund.ContractNumber
                and nvl(ContractOptionFundClassIndicator, 'NULL') <> 'L'
            where contract.SourceSystemName in ('VantageP6')
                and nvl(ContractTotalAccountValue, 0) > 0
                and contract.ContractStatusCode in ('A', 'E', 'G', 'Q', 'Z', '3', '4', '5')       
                and (nvl(ContractPayoutOptionIndicator1, '') not in ('G','N')
                    or nvl(ContractPayoutOptionIndicator2, '') not in ('G','N'))
                and (nvl(ContractPayoutEndDate1, to_utc_timestamp("1999-12-31", "YYYY-MM-DD")) > to_utc_timestamp("2018-08-31", "YYYY-MM-DD")
                    or nvl(ContractPayoutEndDate2, to_utc_timestamp("1999-12-31", "YYYY-MM-DD")) > to_utc_timestamp("2018-08-31", "YYYY-MM-DD"))
                and (nvl(ContractPayoutNextDate1, to_utc_timestamp("1999-12-31", "YYYY-MM-DD")) > to_utc_timestamp("2018-08-31", "YYYY-MM-DD")
                    or nvl(ContractPayoutNextDate2, to_utc_timestamp("1999-12-31", "YYYY-MM-DD")) > to_utc_timestamp("2018-08-31", "YYYY-MM-DD"))
            ) p6ml on ContractNumber = P6MLContractNumber