SELECT 
ContractPlanCode as ck_plan,
case when ContractIssueAge is null or rtrim(cast(ContractIssueAge as string)) = '' then 100 
        when ContractIssueAge > 100 then 100 
        else ContractIssueAge end as ck_age,
case when rtrim(PARGender) = '' then 'M' else nvl(PARGender, 'M') end as ck_sex,
case when rtrim(ck_SmkStatus) = '' then 0 else nvl(ck_SmkStatus, 0) end as ck_smkstatus,
ContractModelingBusinessUnitGroup as ck_char1,
SourceLegalEntityCode as ck_char2,
case when rtrim(ContractQualifiedIndicator) = '' then 'Q' else nvl(ContractQualifiedIndicator,'Q') end as ck_char3, 
case when rtrim(GMDBContractOptionModelingTypeGroupIndicator) = '' then 'B' else nvl(GMDBContractOptionModelingTypeGroupIndicator, 'B') end as ck_char4,
case when rtrim(GMIBContractOptionModelingTypeGroupIndicator) = '' then 0 else nvl(GMIBContractOptionModelingTypeGroupIndicator, 0) end as ck_char5,
case when rtrim(contractmodelingreinsuranceidentifier) = '' then 'N' else nvl(contractmodelingreinsuranceidentifier, 'N') end as ck_char6,
case when rtrim(cast(GMLBContractOptionModelingPWWBSegment as string)) = '' then 0 else nvl(GMLBContractOptionModelingPWWBSegment,0) end as ck_char7, 
case when rtrim(GMLBContractOptionModelingTypeGroupIndicator) = '' then 0 else nvl(GMLBContractOptionModelingTypeGroupIndicator, 0) end as ck_char8,
ContractIssueYear as ck_issueyear,
lpad(ContractIssueMonth,2,'0') as ck_issuemth,
'E' as ck_newbus,--not needed on lake
'1' as m_polcnt,--not needed on lake
case when SourceSystemName = 'VantageP5' 
    then
        case when rtrim(cast(gaapvalue as string)) = '' 
            then 0 
        else nvl(gaapvalue, 0) end
    else
        case when rtrim(cast(ContractTotalAccountValue as string)) = '' 
            then 0 
        else nvl(ContractTotalAccountValue, 0) end
end as i_acctvalue,
case when rtrim(cast(SurrenderValueAmount as string)) = '' then 0 else nvl(SurrenderValueAmount, 0) end as i_cashvalue,
case when rtrim(cast(sa1 as string)) = '' then 0 else sa1 end as sa1,
case when rtrim(cast(sa2 as string)) = '' then 0 else sa2 end as sa2,
case when rtrim(cast(sa3 as string)) = '' then 0 else sa3 end as sa3,
case when rtrim(cast(sa4 as string)) = '' then 0 else sa4 end as sa4,
case when rtrim(cast(sa5 as string)) = '' then 0 else sa5 end as sa5,
case when rtrim(cast(sa6 as string)) = '' then 0 else sa6 end as sa6,
case when rtrim(cast(sa7 as string)) = '' then 0 else sa7 end as sa7,
case when rtrim(cast(sa8 as string)) = '' then 0 else sa8 end as sa8,
case when rtrim(cast(dynsa1 as string)) = '' then 0 else dynsa1 end as dynsa1,
case when rtrim(cast(dynsa2 as string)) = '' then 0 else dynsa2 end as dynsa2,
case when rtrim(cast(dynsa3 as string)) = '' then 0 else dynsa3 end as dynsa3,
case when rtrim(cast(dynsa4 as string)) = '' then 0 else dynsa4 end as dynsa4,
case when rtrim(cast(dynsa5 as string)) = '' then 0 else dynsa5 end as dynsa5,
case when rtrim(cast(dynsa6 as string)) = '' then 0 else dynsa6 end as dynsa6,
case when rtrim(cast(dynsa7 as string)) = '' then 0 else dynsa7 end as dynsa7, 
case when rtrim(cast(dynsa8 as string)) = '' then 0 else dynsa8 end as dynsa8,
case when rtrim(cast(dynsa9 as string)) = '' then 0 else dynsa9 end as dynsa9,
case when rtrim(cast(dynsa10 as string)) = '' then 0 else dynsa10 end as dynsa10,
case when rtrim(cast(SHFSA1 as string)) = '' then 0 else SHFSA1 end as shfsa1,
case when rtrim(cast(SHFSA2 as string)) = '' then 0 else SHFSA2 end as shfsa2,
case when rtrim(cast(SHFSA3 as string)) = '' then 0 else SHFSA3 end as shfsa3,
case when rtrim(cast(SHFSA4 as string)) = '' then 0 else SHFSA4 end as shfsa4,
case when rtrim(cast(SHFSA5 as string)) = '' then 0 else SHFSA5 end as shfsa5,
case when rtrim(cast(SHFSA6 as string)) = '' then 0 else SHFSA6 end as shfsa6,
case when rtrim(cast(SHFSA7 as string)) = '' then 0 else SHFSA7 end as shfsa7,
case when rtrim(cast(SHFSA8 as string)) = '' then 0 else SHFSA8 end as shfsa8,
case when rtrim(cast(SHFSA9 as string)) = '' then 0 else SHFSA9 end as shfsa9,
case when rtrim(cast(SHFSA10 as string)) = '' then 0 else SHFSA10 end as shfsa10,
case when rtrim(cast(safecontractoptionfundsafefundvalue as string)) = '' then 0 else nvl(safecontractoptionfundsafefundvalue, 0) end as safe, 
case when rtrim(cast(GMDBContractOptionGrowthBaseValue as string)) = '' then 0 else nvl(GMDBContractOptionGrowthBaseValue, 0) end as gmdb_growth,
case when rtrim(cast(GMDBContractOptionStepValue as string)) = '' then 0 else nvl(GMDBContractOptionStepValue, 0) end as gmdb_stepup,
case when rtrim(cast(GMDBContractOptionReturnOfPremiumValue as string)) = '' then 0 else nvl(GMDBContractOptionReturnOfPremiumValue, 0) end as gmdb_rop,
case when GMIBContractOptionTypeGroup = 'GMIB' then 
         case when rtrim(cast(GMIBContractOptionGrowthBaseValue as string)) = '' then 0 else nvl(GMIBContractOptionGrowthBaseValue, 0) end
     when GMLBContractOptionTypeGroup = 'GMLB' then
         case when rtrim(cast(GMLBContractOptionGrowthBaseValue as string)) = '' then 0 else nvl(GMLBContractOptionGrowthBaseValue, 0) end
    else 0 end as dur,
case when rtrim(cast(GMIBContractOptionGrowthBaseValue as string)) = '' then 0 else nvl(GMIBContractOptionGrowthBaseValue, 0) end as gmib_growth, 
case when rtrim(cast(GMIBContractOptionStepValue as string)) = '' then 0 else nvl(GMIBContractOptionStepValue, 0) end as gmib_ratchet,
case when rtrim(cast(PB_TWB as string)) = '' then 0 else nvl(PB_TWB, 0) end as pb_twb,
case when rtrim(cast(PB_RWA as string)) = '' then 0 else nvl(PB_RWA, 0) end as pb_rwa,
case when rtrim(cast(PB_FREE as string)) = '' then 0 else nvl(PB_FREE, 0) end as pb_free,
case when rtrim(cast(LF_TWB as string)) = '' then 0 else nvl(LF_TWB, 0) end as lf_twb,
case when rtrim(cast(LF_RWA as string)) = '' then 0 else nvl(LF_RWA, 0) end as lf_rwa,
case when rtrim(cast(LF_FREE as string)) = '' then 0 else nvl(LF_FREE, 0) end as lf_free,
case when rtrim(cast(LF_FREE_PCT as string)) = '' then 0 else nvl(LF_FREE_PCT * 100, 0) end as lf_free_pct,
case when rtrim(cast(GMAB_GFV as string)) = '' then 0 else nvl(GMAB_GFV, 0) end as gmab_gfv,
case when rtrim(cast(EEB_BenPct as string)) = '' then 0 else nvl(EEB_BenPct, 0) end as eeb_benpct,
case when rtrim(cast(EEB_Dur as string)) = '' then 0 else nvl(EEB_Dur, 0) end as eeb_dur,
case when rtrim(cast(EEB_Basis as string)) = '' then 0 else nvl(EEB_Basis, 0) end as eeb_basis,
case when rtrim(cast(EEB_AcumFees as string)) = '' then 0 else nvl(EEB_AcumFees, 0) end as eeb_acumfees,
case when rtrim(cast(EEB_InitBasis as string)) = '' then 0 else nvl(EEB_InitBasis, 0) end as eeb_initbasis,
case when rtrim(cast(InitialPremium as string)) = '' then 0 else nvl(InitialPremium, 0) end as premium,
case when rtrim(cast(modelingrenewalpremium as string)) = '' then 0 else nvl(modelingrenewalpremium, 0) end as renewal,
case when rtrim(cast(contractcumulativewithdrawals as string)) = '' then 0 else nvl(contractcumulativewithdrawals, 0) end as accumwdail, 
case when rtrim(cast(HiMnthly_Val as string)) = '' then 0 else nvl(HiMnthly_Val, 0) end as himnthly_val,
case when rtrim(cast(GMLBContractOptionFeePercentage as string)) = '' or rtrim(cast(GMIBContractOptionFeePercentage as string)) = '' then 0 else coalesce(GMLBContractOptionFeePercentage, GMIBContractOptionFeePercentage, 0) end as glbrdrfee,
case when rtrim(GMDBContractOptionInvestmentMethodIndicator) = '' then 'N' else nvl(OAM_ID, 'N') end as oam_id,
'S' as glb_sj,
case when rtrim(glb_db) = '' then 'N' else nvl(glb_db, 'N') end as glb_db, 
case when rtrim(glb_nh) = '' then 'N' else nvl(glb_nh, 'N') end as glb_nh,
case when rtrim(cast(varrs as string)) = '' then 0 else nvl(varrs, 0) end as varrs,
case when rtrim(cast(varimf as string)) = '' then 0 else nvl(varimf, 0) end as varimf,
ContractResidentStateCode as state,
case when rtrim(cast(spo_ind as string)) = '' then 0 else nvl(spo_ind, 0) end as spo_ind,
case when rtrim(cast(spo_mode as string)) = '' then 0 else nvl(spo_mode, 0) end as spo_mode,
nvl(case when ContractPayoutAmount2 is not null then ContractPayoutAmount1 + ContractPayoutAmount2
    else ContractPayoutAmount1 * SPO_Mode end, 0) spo_amt,
case when rtrim(cast(spo_pwd as string)) = '' then 0 else nvl(spo_pwd, 0) end as spo_pwd,
case when rtrim(cast(spo_startyr as string)) = '' then 9999 else nvl(spo_startyr, 9999) end as spo_startyr,
case when rtrim(cast(spo_startmo as string)) = '' then 99 else nvl(spo_startmo, 99) end as spo_startmo,
contractdacgroup as dac_id,
ContractStatusCode as status,
contractnumbernumericid as i_polnum,
Contract.ContractNumber as policynumber,
case when GMDBContractOptionModelingTypeGroupIndicator is not null or GMIBContractOptionModelingTypeGroupIndicator is not null 
    then  
        case 
            when GMIBContractOptionDurationInMonths = 0 
                then '99'
            when GMIBContractOptionDurationInMonths in (1,2,3) 
                then 'CQ'
            else 'PQ' 
        end
    else 'PQ' 
end as fvqtr,
case when rtrim(cast(contractchargeremaining as string)) = '' then 0 else nvl(contractchargeremaining, 0) end as pbc_remain,
case when rtrim(cast(contractchargepercent as string)) = '' then 0 else nvl(contractchargepercent, 0) end as pbc_pct_last_prem,
case when rtrim(cast(ContractChargeAmortizationRemainingTerm as string)) = '' then 0 else nvl(ContractChargeAmortizationRemainingTerm, 0) end as pbc_wgt_ave_modes_remain,
case when rtrim(cast(RIDER_GEN_ID as string)) = '' then 1 else nvl(RIDER_GEN_ID, 1) end as rider_gen_id,
case when rtrim(cast(GLB_RDR_FUTURE_FEE as string)) = '' then 0 else nvl(GLB_RDR_FUTURE_FEE, 0) end as glb_rdr_future_fee,
'0' as jl_age,
'M' as jl_sex,
case when rtrim(cast(jl_status_ind as string)) = '' then 0 else nvl(jl_status_ind,0) end as jl_status_ind,
case when rtrim(cast(contractpolicyyeartodatecumulativewithdrawals as string)) = '' then 0 else nvl(contractpolicyyeartodatecumulativewithdrawals, 0) end as accumwdytdail,
case when rtrim(cast(GMWBBonusFaceAIL as string)) = '' then 0 else nvl(GMWBBonusFaceAIL, 0) end  as gmwbbonusfaceail,
case when rtrim(cast(GMWBGrowthStop as string)) = '' then 0 else nvl(GMWBGrowthStop, 0) end  as gmwbgrowthstop,
case when rtrim(cast(GMWBGrowthStopYr as string)) = '' then 9999 else nvl(YEAR(GMWBGrowthStopYr), 9999) end  as gmwbgrowthstopyr, 
case when rtrim(cast(GMWBMAWALockYr as string)) = '' then 9999 else nvl(YEAR(GMWBMAWALockYr), 9999) end  as gmwbmawalockyr,
case when rtrim(cast(BASE_LF_FREE_PCT as string)) = '' then 0 else nvl(BASE_LF_FREE_PCT * 100, 0) end  as base_lf_free_pct,
'0' as retropct,
'0' as fixedimffee,
'0' as varriskyallocinit,
SourceSystemName as css_name, 
case when rtrim(cast(contractsourcemarketingorganizationcode as string)) = '' then 0 else nvl(contractsourcemarketingorganizationcode, 0) end as contractsourcemarketingorganizationcode, 
--For calculating GMB_Growth, GMDB_StepUp, GMDB_ROP, GMIB_Growth, GMIB_Ratcher, glb_sj
GMDBcontractoptiongrowthbasevalue as cogbv,
GMDBcontractoptionstepvalue as cosv,
GMDBcontractoptionreturnofpremiumvalue as corop,
GMIBContractOptionSingleJointIndicator as GMIBContractOptionSingleJointIndicator,
GMLBContractOptionSingleJointIndicator as GMLBContractOptionSingleJointIndicator,
case when rtrim(cast(GMLBContractOptionSecondaryLifeIssueAge as string)) = '' then '0' else nvl(GMLBContractOptionSecondaryLifeIssueAge, '0') end as GMLBContractOptionSecondaryLifeIssueAge,
case when rtrim(SPSGender) = '' then 'M' else nvl(SPSGender, "M") end as SPSGender,
--End calculation
GMIBContractOptionTypeGroup, 
GMDBContractOptionTypeGroup,
GMWBContractOptionTypeGroup,
GMLBContractOptionTypeGroup,
TXPRContractOptionTypeGroup 
from Contract
--PAR GENDER
    left join
    (select ContractNumber as PARContractNumber,
    Gender as PARGender
    from party
    where partysourcerelationshiptypecode = 'PAR') par on Contract.ContractNumber = PARContractNumber 
--SPS GENDER
    left join
    (select ContractOption.ContractNumber as SPSContractNumber,  
        sps.Gender as SPSGender
        from ContractOption
        left join (select ContractNumber, gender
                        from party
                        where partysourcerelationshiptypecode = 'SPS'
                        ) sps on ContractOption.ContractNumber = sps.ContractNumber 
        where ContractOptionTypeGroup = 'GMLB'
    ) sps on Contract.ContractNumber = SPSContractNumber
--SAFE
    left join
    (select ContractNumber as SAFEContractNumber,
    sum(contractoptionfundsafefundvalue) as safecontractoptionfundsafefundvalue 
    from contractoptionfund
    group by ContractNumber) safe on Contract.ContractNumber = SAFEContractNumber
--I_ACCTVALUE for VantageP5
    left join 
    (select ContractNumber as P5ContractNumber,
    sum(contractoptionfundgaapvalue) as gaapvalue
    from contractoptionfund
    where ContractOptionFundGAAPValue > 0
    group by ContractNumber) i_acctvalue_p5 on Contract.ContractNumber = P5ContractNumber
--contractoptioninvestmentmethodindicator
    left join
    (select distinct ContractNumber as InvestmentMethodContractNumber,
    contractoptioninvestmentmethodindicator as OAM_ID
    from contractoption 
    ) InvestmentMethod on Contract.ContractNumber = InvestmentMethodContractNumber
--VAR 
    left join
    (select ContractNumber as VARContractNumber, 
    SUM(contractoptionfundbundledrevenueshareamount) as VarRS,
    SUM(ContractOptionFundBundledInvestmentManagementFeeAmount) as VarIMF
    from ContractOptionFund
    where ContractOptionFundClassIndicator = 'V'
    group by ContractNumber
    ) VAR on Contract.ContractNumber = VARContractNumber
--NON-VAR
--    left join
--    (select ContractNumber as NONVARContractNumber, 
--    SumSAV/ as VarDefault
--    from financebusinessviewsmodel.actuarial_historical ah
--       INNER JOIN (select ContractNumber, SUM(ContractFundSeperateAccountValue) as SumSAV from financebusinessviewsmodel.actuarial_historical                
--                    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE()  
--                    group by ContractNumber
--                    ) SAV
--        on ah.ContractNumber = SAV.ContractNumber
--    where pointofviewstartdate <= CURRENT_DATE() and pointofviewstopdate >= CURRENT_DATE() 
--    and ContractOptionFundClassIndicator = 'V'
--    group by ContractNumber
--    ) VAR on ah.ContractNumber = NONVARContractNumber
--SPO
    left join
    (select ContractNumber as SPOContractNumber, 
--SPO_ind
    case when nvl(ContractPayoutOptionIndicator2, '') <> '' then
        case 
            when ContractPayoutOptionIndicator1 = ContractPayoutOptionIndicator2
                then ContractPayoutOptionIndicator1
            when ContractPayoutOptionIndicator1 in ('6','7') 
                or ContractPayoutOptionIndicator2 in ('6','7') 
                then 'X'
            when ContractPayoutOptionIndicator1 <> ContractPayoutOptionIndicator2
                then 'Z'
        end
        else ContractPayoutOptionIndicator1
        end as SPO_ind,
--SPO_PWD
    case when nvl(ContractPayoutPartialWithdrawalMethod2, '') <> '' then 
         case 
            when ContractPayoutPartialWithdrawalMethod1 = ContractPayoutPartialWithdrawalMethod2 
                then ContractPayoutPartialWithdrawalMethod1
            when ContractPayoutPartialWithdrawalMethod1 <> ContractPayoutPartialWithdrawalMethod2 
                then
                    case 
                        when ContractPayoutPartialWithdrawalMethod1 > ContractPayoutPartialWithdrawalMethod2 
                            then ContractPayoutPartialWithdrawalMethod1
                        else ContractPayoutPartialWithdrawalMethod2
                    end
            when ContractPayoutPartialWithdrawalMethod1 >= ContractPayoutPartialWithdrawalMethod2 then
                 ContractPayoutPartialWithdrawalMethod1
            else ContractPayoutPartialWithdrawalMethod2
        end
        else ContractPayoutPartialWithdrawalMethod1
        end as SPO_PWD,
--SPO_StartYr
    case when nvl(ContractPayoutStartYear2, '') <> '' then
        case 
            when ContractPayoutStartYear1 = ContractPayoutStartYear2 
                then ContractPayoutStartYear1
            when ContractPayoutStartYear1 > ContractPayoutStartYear2 
                then ContractPayoutStartYear1   
            else ContractPayoutStartYear2
        end
    else ContractPayoutStartYear1
    end as SPO_StartYr, 
--SPO_StartMo
    case when nvl(ContractPayoutStartMonth2, '') <> '' then
            case 
                when ContractPayoutStartMonth1 = ContractPayoutStartMonth2 
                    then ContractPayoutStartMonth1
                when ContractPayoutStartMonth1 > ContractPayoutStartMonth2 
                    then ContractPayoutStartMonth1   
                else ContractPayoutStartMonth2
            end
        else ContractPayoutStartMonth1
        end as SPO_StartMo 
    from Contract
    ) SPO on Contract.ContractNumber = SPOContractNumber
--SPO_Mode
    left join --Done
        (select ContractNumber as SPOModeContractNumber,
        case when nvl(ContractPayoutMode2, '') <> '' then
                case 
                    when ContractPayoutMode1 in ('6', '7')
                        or ContractPayoutMode2 in ('6', '7')
                        then 1
                    when ContractPayoutMode1 <> ContractPayoutMode2
                        and ContractPayoutStartDate1 > ContractPayoutStartDate2
                        then ContractPayoutMode2
                end
        else ContractPayoutMode1
        end as SPO_Mode,
    row_number() over (Partition by ContractNumber, ContractPayoutMode1, ContractPayoutMode2
                        order by pointofviewstopdate, pointofviewstartdate desc) rownum
    from contract 
    ) SPO_mode_table on Contract.ContractNumber = SPOModeContractNumber and SPO_mode_table.rownum = 1   
--GMWB
    left join --Done
    (select ContractNumber as GMWBContractNumber,
    ContractOptionTypeGroup as GMWBContractOptionTypeGroup, 
    ContractOptionModelingTypeGroupIndicator as GMWBContractOptionModelingTypeGroupIndicator
    from ContractOption
    where ContractOptionTypeGroup = 'GMWB'
    ) gmwb on Contract.ContractNumber = GMWBContractNumber
--GMDB
    left join --Done
    (select ContractNumber as GMDBContractNumber,
    ContractOptionTypeGroup as GMDBContractOptionTypeGroup, 
    ContractOptionGrowthBaseValue as GMDBContractOptionGrowthBaseValue,
    ContractOptionStepValue as GMDBContractOptionStepValue,
    ContractOptionReturnOfPremiumValue as GMDBContractOptionReturnOfPremiumValue,
    ContractOptionModelingTypeGroupIndicator as GMDBContractOptionModelingTypeGroupIndicator,
    ContractOptionInvestmentMethodIndicator as GMDBContractOptionInvestmentMethodIndicator
    from ContractOption
    where ContractOptionTypeGroup = 'GMDB') gmdb on Contract.ContractNumber = GMDBContractNumber
--GMIB
    left join --Done
    (select ContractNumber as GMIBContractNumber,
    ContractOptionTypeGroup as GMIBContractOptionTypeGroup, 
    ContractOptionGrowthBaseValue as GMIBContractOptionGrowthBaseValue,
    ContractOptionStepValue as GMIBContractOptionStepValue,
    case when rtrim(ContractOptionSingleJointIndicator) = '' then 'S' else nvl(ContractOptionSingleJointIndicator,'S') end as GMIBContractOptionSingleJointIndicator,
    ContractOptionDurationInMonths as GMIBContractOptionDurationInMonths,
    ContractOptionModelingTypeGroupIndicator as GMIBContractOptionModelingTypeGroupIndicator,
    ContractOptionFeePercentage as GMIBContractOptionFeePercentage
    from ContractOption
    where ContractOptionTypeGroup = 'GMIB'
    ) gmib on Contract.ContractNumber = GMIBContractNumber
--GMLB
    left join --Done
    (select ContractNumber as GMLBContractNumber,
    ContractOptionTypeGroup as GMLBContractOptionTypeGroup, 
    ContractOptionModelingTypeGroupIndicator as GMLBContractOptionModelingTypeGroupIndicator,
    ContractOptionModelingPWWBSegment as GMLBContractOptionModelingPWWBSegment,
    ContractOptionPrincipalBackWithdrawalBaseValue as PB_TWB,
    ContractOptionPrincipalBackRemainingWithdrawalAmount as PB_RWA,
    ContractOptionBenefitPrincipalBackFreeAmountRemaining as PB_FREE,
    ContractOptionLifetimeWithdrawalBaseValue as LF_TWB,
    ContractOptionLifetimeRemainingWithdrawalAmount as LF_RWA,
    ContractOptionBenefitLifetimeFreeAmountRemaining as LF_FREE,
    case when rtrim(cast(ContractOptionLifetimeTotalFreePercentage as string)) = '' then 0 else nvl(ContractOptionLifetimeTotalFreePercentage, 0) end as LF_FREE_PCT,
    ContractOptionGuaranteedFutureValue as GMAB_GFV,
    ContractOptionHighMonthiversaryValue as HiMnthly_Val,
    ContractOptionFeePercentage as GMLBContractOptionFeePercentage, 
    case when rtrim(ContractOptionSingleJointIndicator) = '' then 'S' else nvl(ContractOptionSingleJointIndicator,'S') end as GMLBContractOptionSingleJointIndicator,
    nvl(ContractOptionDeathIndicator,'N') as glb_db,
    contractoptionincomeenhancementindicator as GLB_NH,
    ContractOptionGenerationID as RIDER_GEN_ID,
    ContractOptionFutureFeePercentage as GLB_RDR_FUTURE_FEE,
    lpad(nvl(ContractOptionSecondaryLifeIssueAge, '0'),3,'0') as GMLBContractOptionSecondaryLifeIssueAge,
    ContractOptionBenefitJointDeathIndicator as JL_STATUS_IND,
    ContractOptionWithdrawalMultiplierBenefitAmount as GMWBBonusFaceAIL,
    ContractOptionBenefitStopIndicator as GMWBGrowthStop,
    ContractOptionBenefitStopDate as GMWBGrowthStopYr,
    '' as GMWBMAWALockYr,--ContractOptionBenefitSelectionDate
    case when rtrim(cast(ContractOptionLifetimeTotalFreePercentage as string)) = '' then 0 else nvl(ContractOptionLifetimeTotalFreePercentage, 0) end as BASE_LF_FREE_PCT,
    ContractOptionGrowthBaseValue as GMLBContractOptionGrowthBaseValue
    from ContractOption
    where ContractOptionTypeGroup = 'GMLB') gmlb on Contract.ContractNumber = GMLBContractNumber
--TXPR	
    left join --Done
    (select ContractNumber as TXPRContractNumber,
    ContractOptionTypeGroup as TXPRContractOptionTypeGroup, 
    ContractOptionTypeGroup as TXPRContractOptionModelingTypeGroupIndicator,
    ContractOptionModelingTypeGroupIndicator as ck_SmkStatus,
    ContractOptionBenefitFactor as EEB_BenPct,
    ContractOptionDurationInMonths as EEB_Dur,
    ContractOptionTaxableEarnings as EEB_Basis,
    ContractOptionBenefitAccumFeePayment as EEB_AcumFees,
    ContractOptionTaxableInitialEarnings as EEB_InitBasis
    from ContractOption
    where ContractOptionTypeGroup = 'TXPR'
    ) txpr on Contract.ContractNumber = TXPRContractNumber
----SHFSA
left join 
    (select
    contractnumber as SHFSAContractNumber,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '1' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA1,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '2' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA2,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '3' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA3,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '4' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA4,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '5' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA5,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '6' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA6,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '7' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA7,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '8' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA8,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '9' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA9,
    SUM(case when FundSelfHedgingSeparateAccountFlag = '10' then nvl(contractoptionfundselfhedgingseparateaccountvalue,0) else 0 end) SHFSA10
    from Fund
        inner join ContractOptionFund
            on Fund.FundNumber = ContractOptionFund.ContractOptionFundNumber
            and Fund.fundregion = ContractOptionFund.ContractAdministrationLocationCode
    group by contractnumber) shfsa on Contract.ContractNumber = SHFSAContractNumber
--SA
left join 
    (select ContractNumber as SAContractNumber,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex1 ,0)) SA1,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex2 ,0)) SA2,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex3 ,0)) SA3,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex4 ,0)) SA4,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex5 ,0)) SA5,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex6 ,0)) SA6,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex7 ,0)) SA7,
    SUM(nvl(ContractOptionFundSeparateAccountValueIndex8 ,0)) SA8
    from ContractOptionFund
    group by contractnumber) sa on Contract.ContractNumber = SAContractNumber
--DYNSA
left join 
    (select contractnumber as DYNSAContractNumber,
    SUM(case when FundDynamicSeparateAccountFlag = '1' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA1,
    SUM(case when FundDynamicSeparateAccountFlag = '2' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA2,
    SUM(case when FundDynamicSeparateAccountFlag = '3' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA3,
    SUM(case when FundDynamicSeparateAccountFlag = '4' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA4,
    SUM(case when FundDynamicSeparateAccountFlag = '5' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA5,
    SUM(case when FundDynamicSeparateAccountFlag = '6' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA6,
    SUM(case when FundDynamicSeparateAccountFlag = '7' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA7,
    SUM(case when FundDynamicSeparateAccountFlag = '8' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA8,
    SUM(case when FundDynamicSeparateAccountFlag = '9' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA9,
    SUM(case when FundDynamicSeparateAccountFlag = '10' then nvl(ContractOptionFundDynamicSeparateAccountValue,0) else 0 end) DYNSA10
    from Fund
        inner join ContractOptionFund
            on Fund.FundNumber = ContractOptionFund.ContractOptionFundNumber
            and Fund.fundregion = ContractOptionFund.ContractAdministrationLocationCode
    group by contractnumber) DYNSA  on Contract.ContractNumber = DYNSAContractNumber
