# ! /usr/local/bin/python
# -*- coding: utf-8 -*-

###############################################
# AlfaMapper.py:
# 
# Main caller class for the Alfa Mapper application
#
# Application will be used to pull information from the Hadoop data lake,
# collate that data into an input object usable by analysis software
# and export the information to a target system.
# 

import sys
sys.path.insert(0, '.')

import logging
import logging.config
import os

#Libraries
from AlfaMapping import AlfaMapping
from AlfaConfig import AlfaConfig
from AlfaWriter import AlfaWriter 
from AlfaMetrics import AlfaMetrics

from BsigNotification import BsigNotification

from config_file import alfaInputPath, alfaMetricsPath, path

from AlfaLogConfig import alfalogger

def AlfaMapper():
    'Main class used for starting the ALFA mapping application'
    
    #Lists used to manage output files and email attachments
    metricsFileList = []
    attachmentList  = []
    
    #Metricts objects
    metrics_results = []
    
    #Get an instance of the logger
    logger = logging.getLogger('alfaLogger.AlfaMapper')
    logger.info("Executing AlfaMapper")
    
    #Read in 1MDLC Configuration File
    configObj = AlfaConfig()
    
    #Import configuration inforamtion
    configFileDataList = configObj.read_config_file(alfaInputPath)
    
    # Create inforce file creation list
    inforceFileList = configObj.get_inforce_file_list(configFileDataList)
    
    # Create output file mapping list
    output_file_mapping = configObj.assemble_output_file_location(configFileDataList)

    connectionType = 'spark'
    #connectionType = 'odbc'

    if connectionType == 'spark':
        ##### SPARK Connection #####
        from AlfaSparkConnect import AlfaSparkConnect
        alfaConnectionObj = AlfaSparkConnect()
        
    elif connectionType == 'odbc':
        ##### IMPALA ODBC Connection #####
        from AlfaOdbcConnect import AlfaOdbcConnect
        alfaConnectionObj = AlfaOdbcConnect()
        
    logger.debug("Connection Type: " + connectionType)
    
    # We now have our list of files, Perform extract, Transform Load, for each one
    # Loop through all entries in the inforce file list
    for inforceFileObj in inforceFileList:
        
        #Build Inforce File Name
        inforceName = inforceFileObj['Name']
        
        logger.debug("Building Inforce File: " + inforceName)
        
        #Build inforce Metrics file
        metricsObj = AlfaMetrics(alfaMetricsPath, inforceName)
        
        logger.debug("Metrics File Path: " + metricsObj.get_metrics_file_path())
        
        # Build the query 
        alfaConnectionObj.build_query_from_file(path + os.sep + inforceFileObj['QueryTemplate'])
        if 'SubQueryPath' in inforceFileObj:
            alfaConnectionObj.build_subquery_from_file(path + os.sep + inforceFileObj['SubQueryPath'])
        if 'QueryPath' in inforceFileObj:
            alfaConnectionObj.build_where_from_file(path + os.sep + inforceFileObj['QueryPath'])
        if 'QueryParams' in inforceFileObj:
            alfaConnectionObj.build_where_from_json_array(inforceFileObj['QueryParams'])
        #alfaConnectionObj.add_query_return_limit(10)
        
        # begin timing the query
        startTime = metricsObj.record_start_time()
        # Execute the query
        dataFrame = alfaConnectionObj.run_query(inforceName)
       
        #Add the inforce name to the data frame, we'll checking it later for our conversions
        dataFrame['inforceName'] = inforceName
        
        # record the total query time
        metricsObj.record_stop_time(startTime)
        
        #Execute Pre Transformation Metrics
        pre_transform_metrics = metricsObj.execute_metrics_suite(dataFrame, metricsObj)
    
        #Transform and map data for ALFA
        alfaMapping = AlfaMapping()
      
        #Execute alfa mapping
        dataFrame = alfaMapping.map_data(dataFrame)   

        #Create a MG-ALFA AIL2 formatted Inforce File from the data frame
        alfaWriter = AlfaWriter()
    
        #Convert to AIL2 format
        logger.debug("Converting data to AIL2 formatted Inforce File")
        outputFileData = alfaWriter.convert_to_ail2_format(dataFrame, metricsObj)
        
        # Execute post Transformation Metrics
        post_transform_metrics = metricsObj.execute_metrics_suite(outputFileData, metricsObj)
        
        # Compare Metric results to verify they are the same
        metrics_results.extend(metricsObj.metrics_validation(pre_transform_metrics, post_transform_metrics))
        
        #Create output file,
        logger.debug("Creating output file: " + inforceFileObj['Name'])
        alfaWriter.write_output_file(outputFileData, inforceFileObj['Name'], output_file_mapping)
        
        #If file was created succesfully add it to the output file list
        metricsFileList.append(metricsObj.get_metrics_file_path())


    # If there was a failure in the result metrics issue a notification if not exit
    if any(metrics_results):
        logger.debug("WARNING: Metrics missmatches found, sending notification email")
    
        #Build the Email Notification and attach Inforce, Metrics, and Logging files
        notificationObj = BsigNotification()
        
        #Create the Email Body for the inforce file list
        logger.debug("Building Notification Email Body")
        emailBody = notificationObj.build_email_body(inforceFileList)
        
        #Add output file list to be attached to email
        attachmentList.extend(metricsFileList)
        
        #Build and send the email with attachments
        logger.debug("Sending Email")
        notificationObj.send_bsig_email_data(emailBody, attachmentList)
    else:
        logger.debug("No metrics miss matches found")

    logger.info("Finished AlfaMapper")
    
def main():
    try:
        alfalogger.info('Executing Alfa Mapper')
        
        #Call Main AlfaMapper class
        AlfaMapper()
        
    finally:
        logging.shutdown()

if __name__ == '__main__':
    main()


