###############################################
# BsigNotification.py:
#
# The Notifcation class will handle sending emails containing file processing
# metrics
#
#
import os
import smtplib
import sys
import logging

from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart


sender = "dataorgbsig@transamerica.com"
receipients = ["dataorgbsig@transamerica.com"]



class BsigNotification:

    #Constructor
    def __init__(self):
        self.logger = logging.getLogger('bsigLogger.BsigNotification')
        self.logger.debug('Creating an instance of BsigNotification')

    # Function builds the email body based on input which are defined by the
    # file build process
    #
    def build_email_body(self, inforceFileList):
        self.logger.debug('Executing build_email_body()')
        emailText = []

        emailText.append("-- BSIG Automated Notification --\n")
        emailText.append("Processed Files: \n")

        for i in range(len(inforceFileList)):
            emailText.append(inforceFileList[i]['Name'] + '\n')

        #Build the email body from the string list
        emailBody = ''.join(emailText)

        return emailBody


    # Function which builds and sends an email acting as a notification upstream
    # showing which files were processed and if any of them passed or failed
    # the Data email will contain data revelevnt to inforce file creation
    #
    def send_bsig_email_data(self, emailBody, attachmentList):
      self.logger.debug('Executing send_bsig_email_data()')

      #Create email object with Subject, sender, and receipients
      emailObj = MIMEMultipart()
      emailObj['Subject'] = "BSIG Process Notification Status"
      emailObj['From'] = sender
      emailObj['To'] = ', '.join(receipients)

      #Write the body of the email
      emailObj.attach( MIMEText(emailBody, 'plain') )

      #Add the inforce file attachments
      for i in range(len(attachmentList)):
          fo = open(attachmentList[i])
          fileContent = fo.read()
          fo.close()

          msg = MIMEBase('application', "octet-stream")
          msg.set_payload(fileContent)

          encoders.encode_base64(msg)
          msg.add_header('Content-Disposition', 'attachment',
                         filename = os.path.basename(attachmentList[i]))
          emailObj.attach(msg)

      # Send the message via our own SMTP server.
      try:
         s = smtplib.SMTP('email1.aegonusa.com')
         s.sendmail(sender, receipients, emailObj.as_string())
         s.quit()
      except:
         self.logger.error('Unable to send the email.  Error: ', sys.exc_info()[0])
         raise


    # Function which builds and sends an email acting as a notification upstream
    # The Error email will be sent as a failure case
    #
    def send_bsig_email_error(self, errorFile, attachmentList):
        self.logger.debug('Executing send_bsig_email_error()')
        
        #If error file exists
        if os.path.isfile(errorFile):
        
            failure = MIMEMultipart()
            failure['Subject'] = "BSIG Report Status - Failure"
            failure['From'] = sender
            failure['To'] = ', '.join(receipients)
            
            #Create text body of email
            with open(errorFile) as fp:
                text = fp.read()
                failure.attach(MIMEText(text, 'plain'))
            
            #Open and build attachements
            with open(attachmentList[0]) as fp:
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read())
                
            #Attach attachments
            encoders.encode_base64(msg)
            msg.add_header('Content-Disposition', 'attachment', 
                           filename = os.path.basename(errorFile))
            failure.attach(msg)
            
            #Send the email
            try:
                s = smtplib.SMTP('email1.aegonusa.com')
                s.sendmail(sender, receipients, failure.as_string())
                s.quit()
            except:
                self.logger.error('Unable to send the email.  Error: ', sys.exc_info()[0])
            raise
        else:
            self.logger.warning("Error file not found")


