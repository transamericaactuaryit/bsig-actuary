###############################################
# AlfaOdbcConnect:
# Establishes a connection to Hadoop data lake using and ODBC connection.
#
# Returns the connection object 

import pyodbc
import pandas
import logging
import json

#BSIG Specific libraries
from BsigConnect import BsigConnect 

class AlfaOdbcConnect(BsigConnect):
    'child class used for management of all ODBC connections'
    
    #Constructor
    def __init__(self):
        self.logger = logging.getLogger('alfaLogger.AlfaOdbcConnect')
        self.logger.debug('Creating an instance of AlfaOdbcConnect')
        self.queryString = ''
        self.whereString = ''
        self.limitString = ''
        
    def _get_where_string(self):
        return self.whereString
    
    def _clean_full_query_format(self,query):
        #Spark uses nvl, ODBC uses isnull
        return  query.replace("nvl(","isnull(")
    
    def _clean_pre_query_format(self,query):
        #Spark uses `, ODBC uses ' or "
        return query.replace("`","'")
    
    def _clear_variables(self):
        self.queryString = ''
        self.whereString = ''
        self.limitString = ''
        return
    
    def _convert_quarter_to_months(self,values):
        if values == "1" or values == "Q1":
            return "('1','2','3')"
        elif values == "2" or values == "Q2":
            return "('4','5','6')"
        elif values == "3" or values == "Q3":
            return "('7','8','9')"
        elif values == "4" or values == "Q4":
            return "('10','11','12')"
    
    def query_combine_param_parts(self, column, operation, values):
        if operation == "IN":
            return str(column + " " + operation + " " + self.query_param_values(values))
        elif column == "Quarter":
            return str("ContractIssueMonth IN " + self._convert_quarter_to_months(values))
        else:
            return str(column + " " + operation + " '" + values + "'")
        
    def query_param_values(self, values):
        return "('" + "','".join(str(_) for _ in values) + "')"
        
    def add_query_return_limit(self, rowCount):
        self.logger.debug('Executing add_query_return_limit()')
        self.limitString = ' LIMIT ' + str(rowCount)
        
    def build_query(self, inputString):
        self.logger.debug('Executing build_query()')
        self.queryString = inputString
    
    def build_query_from_file(self, filePath):
        self.logger.debug('Executing build_query()')
        #Open and read in the SQL file
        file = open(filePath, 'r')
        sqlFile = file.read()
        file.close()
               
        self.queryString = sqlFile
        
    def build_where(self, inputWhereString):
        self.logger.debug('Executing build_where()')
        self.whereString = inputWhereString
        
    def build_where_append_and(self, inputAppendString):
        self.logger.debug('Executing build_where_append_and()')
        if self.whereString == '':
            self.whereString = ' and ' + inputAppendString
        else:
            self.whereString = self.whereString + ' AND ' + inputAppendString
    
    def build_where_from_file(self, filePath):
        self.logger.debug('Executing build_where_from_file()')
        #Open and read in the SQL file
        file = open(filePath, 'r')
        sqlFile = file.read()
        file.close()
        
        if self.whereString == '':
            self.whereString = ' and ' + sqlFile
        else:
            self.whereString = sqlFile  
            
    def build_where_from_json_array(self, array):
        self.logger.debug('Executing build_where_from_json_array()')
        
        #Open and read in the json string array
        dataParams = json.loads(json.dumps(array)) 
            
        for whereParams in dataParams:
            self.build_where_append_and(self.query_combine_param_parts(whereParams['QueryParamColumn'], whereParams['QueryParamOperator'], whereParams['QueryParamValues']))
        
    #Connection function, used to establish connection 
    def run_query(self):
        self.logger.debug('Executing run_query()')
        
        #Establish a connection with the Impala ODBC driver
        #NOTE: the connection object Test_Impala64, must be configured with win ODBC connection
        # manager before this connection will work.
        con = pyodbc.connect("DSN=A_Test_Impala64",autocommit=True)
        
        #Execute SQL statement
        #cursor = con.cursor()
        #cursor.execute(self.queryString)
        #result = cursor.fetchone()
        
        #Convert single quote column aliases from backticks to single quotes
        query = self._clean_pre_query_format(self.queryString)
        
        if self.whereString != '':
            query = query + self.whereString
            
        
        if self.limitString != '':
            query = query + ' ' + self.limitString
        
        query = self._clean_full_query_format(query)
        
        self.logger.debug("Printing QUERY")
        self.logger.debug(query)
        
        #Pandas Execution of SQL
        result = pandas.read_sql(query, con)        
        
        self._clear_variables()
        return(result)
        
        
        
        
        

