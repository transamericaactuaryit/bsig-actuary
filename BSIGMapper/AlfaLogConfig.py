# -*- coding: utf-8 -*-
################################################
#
#    BsigLogConfig.py
#
#    Implemetation of bsig log class for configuring logging
import logging
import sys
import os
sys.path.insert(0, '.')

from config_file import alfaLoggingPath, alfaLoggingFile

if not os.path.isdir(alfaLoggingPath):
    os.makedirs(alfaLoggingPath)
        
#logging.config.fileConfig(path + os.sep + 'logging_config.ini')        
logging.root.setLevel(logging.DEBUG)
logConsoleHandler = logging.StreamHandler(sys.stdout)
logConsoleHandler.setLevel(logging.DEBUG)
logConsoleHandler.setFormatter(logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s"))
logging.root.addHandler(logConsoleHandler)  
    
#Create logger Object
alfalogger = logging.getLogger('alfaLogger')  
    
logFileHandler = logging.FileHandler(alfaLoggingFile, mode="w")
logFileHandler.setLevel(logging.DEBUG)
logFileHandler.setFormatter(logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s"))
alfalogger.addHandler(logFileHandler)