# -*- coding: utf-8 -*-

###############################################
#
#   AlfaMapping.py
#
#   Class used for mapping data from source to resulting data for
#   actuary tools
#

from AlfaRules import AlfaRules
#from BsigMapping import BsigMapping

class AlfaMapping():
    #method for apply set of rules to dataframe
    def apply_rules(self, bsigDf):
        bsig_rules = AlfaRules()
        resultDf = bsig_rules.apply_rules(bsigDf)
        return resultDf
    
    #main mapping method
    def map_data(self, bsigDf):
        resultDf = bsigDf
        resultDf = self.apply_rules(resultDf)
        return resultDf
