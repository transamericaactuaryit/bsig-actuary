###############################################
# AlfaWriter.py:
# 
# The ALFA writer contains functions to take a data frame and
# produce a MG-ALFA formated AIL2 file.
# 
#

import json
import logging
import pandas as pd
import numpy as np
import os

from BsigSink import BsigSink

from config_file import alfaConfigPath, alfaOutputPath, inforceListFileName

#extra column list
extra_columns = ['css_name',
                 'cogbv',
                 'cosv',
                 'corop',
                 'GMIBContractOptionSingleJointIndicator',
                 'GMLBContractOptionSingleJointIndicator',
                 'GMLBContractOptionSecondaryLifeIssueAge',
                 'SPSGender',
                 'GMIBContractOptionTypeGroup', 
                 'GMDBContractOptionTypeGroup', 
                 'GMWBContractOptionTypeGroup', 
                 'GMLBContractOptionTypeGroup', 
                 'TXPRContractOptionTypeGroup',
                 'inforceName',
                 'contractsourcemarketingorganizationcode']

model33FileName = "Qqyy_Total.ail2"

class AlfaWriter(BsigSink):
    
    #Constructor
    def __init__(self):
        self.logger = logging.getLogger('alfaLogger.AlfaWriter')
        self.logger.debug('Creating an instance of AlfaWriter')
    
    # Function which writes the final data frame to an output file location
    # the output file location will be provided by the 1MDLC team and configured
    # on a external network location, this function will establish the connection and
    # write the file in the correct location
    def write_output_file(self, dataFrame, fileName, configFileData):
        self.logger.debug('Executing write_output_file()')
               
        # Drop extra columns
        dataFrame = dataFrame.drop(extra_columns, axis=1, errors='ignore')
          
        # Find the correct locations to write the output file
        #
        # Create inforce files for appropreate groups based on input request
        for inforce_grouping in configFileData:

            for file_info in configFileData[inforce_grouping]:

                if fileName == file_info['name']:
                    self.logger.info('Writing Inforce File, Inforce Group: ' + inforce_grouping + ', File Name: ' + fileName)
                    destinationPath = alfaOutputPath + file_info['out_path']
                    
                    if not os.path.isdir(destinationPath):
                        os.makedirs(destinationPath)
                        
                    # Changeing ck_Plan column name to start with a comment, allowing column names to be written to inforce file
                    dataFrame = dataFrame.rename(columns={'ck_plan' : '//ck_plan'}) 
                    
                    if inforce_grouping == "Model33":
                        dataFrame.to_csv(destinationPath + model33FileName, mode='a', sep = '\t', 
                                         line_terminator ='\r\n', index = False, header = True) 
                    else:
                        dataFrame.to_csv(destinationPath + fileName + '.ail2', sep = '\t', 
                                         line_terminator ='\r\n', index = False, header = True)
                     #shutil.move(destinationPath + fileName + '.ail2', bsigRemoteDir + os.sep + fileName + '.txt')
                else:
                    pass


    # Function which takes a data frame and performs analysis and conversion to 
    # an AIL2 file formatted file.
    #
    # Returned object can be written as an AIL2 file for use with MG-ALFA software
    def convert_to_ail2_format(self, dataFrame, metricsObj):
        self.logger.debug('Executing convert_to_ail2_format()')
        
        #Create a local copy of input Obj
        dataFrameLocal = dataFrame
            
        try:
            
            #Open the conversion key file (must be provied by DGO Team/ALFA Experts)
            keyFrame = pd.read_csv(alfaConfigPath + 'AIL2_ConversionKey.csv', sep='\s*,\s*',
                               header=0, encoding='ascii', engine='python')
            
            #Add an extra colum to track which fields were checked and which were not
            keyFrame.insert(loc = 0, column = 'checked', value = False)
            
            foundInKey = []
            notFoundInKey = []
    
            #Set the table index to the fields column
            #NOTE: impala/hive return lowercase column names, must convert input file key to lower case
            # to allow for correct string compares
            newIndex = keyFrame['Field']
            newIndex = newIndex.str.lower()
            
            keyFrame['NewIndex'] = newIndex
            keyFrame['ColumnNameLookup'] = newIndex
            keyFrame = keyFrame.set_index(['NewIndex'])
    
            #For all columns in the data frame look up the column name and verify it exists
            #in the key file
            
            
            #Create a list of column names in the input object
            columnList = dataFrameLocal.columns.get_values()

            for name in columnList:
                if keyFrame['ColumnNameLookup'].str.contains(name).any():
                    
                    #Update list of found vs not found variables
                    foundInKey.append(name)
                    
                    #Update key checked column
                    keyFrame.at[name,'checked'] = True
                    
                    typeCheck = keyFrame.at[name, 'Type']
                    
                    if typeCheck == 'Key':
                        pass
                    else:
                        #Find the precision assigned by the key and assign it to the output frame
                        precision = keyFrame.at[name, 'Dec']  
                        
                        buildPrecisionNum = '0' 
                        for i in range(int(precision)):
                            if i == 0:
                                buildPrecisionNum = buildPrecisionNum + '.0'
                            else: 
                                buildPrecisionNum = buildPrecisionNum + '0'
                        
                        #Convert all NAN and Null values to correctly mapped defalt values  
                        dataFrameLocal[[name]] = dataFrameLocal[[name]].replace(r'\s+', np.nan, regex=True).replace('',np.nan)
                        dataFrameLocal[[name]] = dataFrameLocal[[name]].fillna(buildPrecisionNum)
                  
                else:
                    self.logger.debug('AIL2 Conversion Variable Name NOT Found in Key')
                    #Update list of found vs not found variables
                    notFoundInKey.append(name)
            
            # Record names found list
            self.logger.debug('--- List of columns found in key ---\n')
            for i in range(len(foundInKey)):        
                self.logger.debug(foundInKey[i]) 
            
            #Metrics: Record names not found list
            self.logger.debug('--- List of columns not found in key ---\n')
            for i in range(len(notFoundInKey)):        
                self.logger.debug(notFoundInKey[i]) 
                
        except Exception as e:
            self.logger.error("Error: Failure to convert to AIL2 format")
            self.logger.error(e)
            
        return dataFrameLocal
    