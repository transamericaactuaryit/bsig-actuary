###############################################
# BsigFileRead.py:
#
# Provides an interface to allow for files to be ingested by the  BSIG system
#
# Returns the file object 

import re 
import sys

#BSIG Specific libraries
import BsigConnect

#class bsigConnectorSpark(bsigConnect):
class BsigFileRead():
    'Allows for files to be read by the BSIG system'
    
    #Constructor
    def __init__(self):
        self.filePath = ''
        
    def set_file_path(self, inputFilePath):
        self.filePath = inputFilePath
        
    def open_file(self):
        file fileObj = open(self.filePath, 'r')
        
        return(fileObj)
        
        
        
        
        

