###############################################
# BsigSink.py:
# Builds the body of the email which will be sent as a notification
#
# 

class BsigSink:
   actuarialFileContent = list()

   def addLine(self, line):
      self.actuarialFileContent.append(line)
      
   def addTransformedData(self, lines):
      self.actuarialFileContent.extend(lines)

   def writeFile(self, filename):
      fout = open(filename, 'a')
      fout.writelines(self.actuarialFileContent)
      fout.close()
      
   def writeJson(self, filename):
      print("Json writing coming soon.")
   
   def writeXml(self, filename):
      print("XML wirting coming soon.")

def main():      
   bs = BsigSink()
   bs.addLine("This here's the wattle,")
   bs.addLine("the emblem of our land.")
   bs.writeFile('symbol.txt')
   