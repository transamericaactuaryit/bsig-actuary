###############################################
# AlfaConfig.py:
#
# Configuration class related specifically to MG-ALFA analysis software, some 
# hard coded values here which are specific to inforce files created for the software
#
# Class is used to configure the BSIG mapper tool for extracting and creating 
# MG-ALFA specific inforce files.
# 
#

import json
import logging
import os

from BsigConfiguration import BsigConfiguration
from config_file import alfaConfigPath, inforceListFileName

class AlfaConfig(BsigConfiguration):
    
    #Constructor
    def __init__(self):
        self.logger = logging.getLogger('alfaLogger.BsigAlfaConfig')
        self.logger.debug('Creating an instance of AlfaConfig')
    
    #Function reads in a JSON formatted config file containing the folloing configuration information
    #
    #   Inforce File Grouping: Fair Value, DAC, STAT, MVN, MCVNB. 
    #   Quarter
    #   Year
    #   Destination Directory
    #
    def read_config_file(self, filePath):
        self.logger.debug("Executing AlfaConfig.read_config_file()")
        
        configDataList = []
        #Open and extract config data     
        try:
            self.logger.debug("Config File Path: " + filePath)
            for file in os.listdir(filePath):
                if file.endswith(".json"):
                    with open(filePath + os.sep + file) as configFile:
                        configDataList.append(json.load(configFile)) 
        except IOError:
            self.logger.error("Error: read_config_file(): file not found")

        return configDataList
        
    # Function determines which files will be pulled during inforce file creation process
    # Input variable is output object of AlfaConfig::read_config_file() function
    #
    # Determination is made based on Inforce File Grouping type
    #   ie. FairValue, DAC, STAT, MVN, MCVNB, Model33
    #
    def get_inforce_file_list(self, configDataList):   
        self.logger.debug('Executing AlfaConfig.get_inforce_file_list()')
        
        requestList       = []
        fileCreationNames = []
        fileCreationObjs  = []
        
        #read in inforce file list
        self.logger.debug("Inforce File List: " + alfaConfigPath + inforceListFileName)
        inforceFileList = json.load(open(alfaConfigPath + inforceListFileName))

        # Build list of files to be created:
        # The list of inforce files must not contain repeates, this section 
        # checks a list of file names against a list of 'group names' (DAC, FairValue, STAT, ext).
        # each group name coresponds to a list of associated files in a seperate
        # configuration file (BSIGMapper/BsigConfig/InforceFileList.json)
        # Each group in the list is checked against the live list built in memory
        # and repeates will be thrown out.

        self.logger.debug('Building Inforce File List')
        for model_type in configDataList:
            
            for item in model_type['requests']:
                requestList.append(model_type['requests'][item]['Model'])
                
                self.logger.debug('Adding files for: ' + model_type['requests'][item]['Model'])
                for requestItem in inforceFileList['InforceGroups'][model_type['requests'][item]['Model']]:
                    
                    if requestItem['Name'] in fileCreationNames:
                        pass
                    else:
                        self.logger.debug('Adding inforce File to list: ' + requestItem['Name'])
                        fileCreationNames.append(requestItem['Name'])
                        fileCreationObjs.append(requestItem)
                        
        # Log files to be created
        self.logger.debug('Inforce files to be created:')
        for file in fileCreationNames:
            self.logger.debug(file)
            
        return fileCreationObjs
    
    # Function assembles list to direct created inforce files to correct output directories
    # Input variable is output object of AlfaConfig::read_config_file() function
    #
    # Determination is made based on Inforce File Grouping type
    #   ie. FairValue, DAC, STAT, MVN, MCVNB, Model33
    #
    def assemble_output_file_location(self, configDataList):
        self.logger.debug('Executing AlfaConfig.assemble_output_file_location()')
        
        requestList       = []
        output_list_obj  = {}
        
        #read in inforce file list
        self.logger.debug("Inforce File List: " + alfaConfigPath + inforceListFileName)
        inforceFileList = json.load(open(alfaConfigPath + inforceListFileName))

        # Build list of file output locations based on input request
        # the files associated with each model is configured via AlfaConfig/InforceFileList.json
        # each model executed has an associated set of output files to be created.
        # This function will create the mapping for directing created files to the correct
        # output locations.

        self.logger.debug('Building Inforce File Output destination list')
        for model_type in configDataList:
            
            model_file_list  = []

            for item in model_type['requests']:
                requestList.append(model_type['requests'][item]['Model'])

                
                self.logger.debug('Adding files for: ' + model_type['requests'][item]['Model'])
                for requestItem in inforceFileList['InforceGroups'][model_type['requests'][item]['Model']]:
                    
                    # Reset model names list
                    file_obj = {}
                    
                    # Build list of associated attrabutes 
                    file_obj['name'] = requestItem['Name']
                    file_obj['toEnviornment'] = model_type['header']['ToEnvironment']  
                    file_obj['out_path'] = model_type['requests'][item]['DestPath']

                    model_file_list.append(file_obj)

            # Build list of all files associated with each model
            output_list_obj[model_type['requests'][item]['Model']] = model_file_list
            
        return output_list_obj
        
        
        
        
        
        