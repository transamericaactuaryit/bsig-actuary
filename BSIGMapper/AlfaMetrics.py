###############################################
# AlfaMetrics.py:
# 
# The Alfa Metrics class handles MG-ALFA specific inforce file creation and 
# collects and reports metrics specific to the AIL2 file type
# 
#

import logging
import time
import datetime
import os

from BsigMetrics import BsigMetrics

class AlfaMetrics(BsigMetrics):
    
    # Constructor
    def __init__(self, metricsFilePath, inforceFileName):
        self.logger = logging.getLogger('alfaLogger.AlfaMetrics')
        self.logger.debug('Creating an instance of AlfaMetrics')
        self.metricsFile = inforceFileName
        self.metricsFilePath = metricsFilePath + inforceFileName + '_metrics.txt'
        
        #Create the file if it dosent exist
        if not os.path.isdir(metricsFilePath):
           os.makedirs(metricsFilePath)
        file = open(self.metricsFilePath, 'w')
        file.write( 'Metrics File for inforce File: ' + inforceFileName)
        file.write('\n\n')
        file.close()
    
    # Function returns the metrics file path tied to the object
    def get_metrics_file_path(self):
        self.logger.debug('Executing get_metrics_file_path()')
        return self.metricsFilePath    
    
    # Function used to create and log a time object for timing metrics through out
    # the application
    #
    # return variable is a time object not a string
    def record_start_time(self):
        self.logger.debug('Executing record_start_time()')
        
        #create a start time object
        timeStamp = time.time()
        startTime = datetime.datetime.fromtimestamp(timeStamp).strftime('%Y-%m-%d_%H:%M:%S')

        #Log the time        
        file = open(self.metricsFilePath, 'a')
        self.logger.debug('Start Time: ' + startTime)
        file.write('Start Time: ' + startTime + '\n') 
        file.close()
        return timeStamp
        
    # Function used to create and log a time object for timing metrics through out
    # the application
    #
    # return variable is a time object not a string
    def record_stop_time(self, inputStartTime):
        self.logger.debug('Executing record_stop_time()')
        
        #create a start time object
        stopTime = time.time()
        
        stopTimeStr = datetime.datetime.fromtimestamp(stopTime).strftime('%H:%M:%S')
        
        startTimeStr = datetime.datetime.fromtimestamp(inputStartTime).strftime('%H:%M:%S')
        
        deltaTime = ( datetime.datetime.strptime(stopTimeStr,'%H:%M:%S') - 
                     datetime.datetime.strptime(startTimeStr,'%H:%M:%S') )
        
        #Log the time        
        file = open(self.metricsFilePath, 'a')
        self.logger.debug('Stop Time: ' + datetime.datetime.fromtimestamp(stopTime).strftime('%Y-%m-%d_%H:%M:%S'))
        self.logger.debug('Total Time: ' + str(deltaTime))    
        file.write('Stop Time: ' + datetime.datetime.fromtimestamp(stopTime).strftime('%Y-%m-%d_%H:%M:%S') + '\n') 
        file.write('Total Time: ' + str(deltaTime) + '\n') 
        file.close()


    # Function which records the row count of the data frame being processed
    def record_row_count(self, rowCount):
        self.logger.debug('Executing Metric record_row_count()')
        file = open(self.metricsFilePath, 'a')
        file.write("Row Count: " + str(rowCount) + '\n') 
        file.close()
        
    # Function which records the number of columns of the data frame being processed
    def record_column_count(self, columnCount):
        self.logger.debug('Executing Metric record_column_count()')
        file = open(self.metricsFilePath, 'a')
        file.write("Column Count: " + str(columnCount) + '\n') 
        file.close()    
        
    # Function which records a list of the columns which are part of the query 
    # being processed but are also found in the column key
    # Note: Column Key is a file which contains a list of 'acceptable' column 
    # names and types
    def record_found_in_name_key(self, nameFoundList):
        self.logger.debug('Executing Metric record_found_in_name_key()')
        file = open(self.metricsFilePath, 'a')

        file.write('\n')
        file.write('--- List of columns found in key ---\n')
        for i in range(len(nameFoundList)):        
            file.write(nameFoundList[i] + '\n') 

        file.close()    
        
    # Function which records a list of the columns which are part of the query
    # but are not found in the key file with a list of 'acceptable' column
    # names and types
    # 
    def record_not_found_in_name_key(self, nameNotFoundList):
        self.logger.debug('Executing Metric record_not_found_in_name_key()')
        file = open(self.metricsFilePath, 'a')

        file.write('\n')
        file.write('--- List of columns not found in key ---\n')
        for i in range(len(nameNotFoundList)):        
            file.write(nameNotFoundList[i] + '\n') 

        file.close()   
        
    # PlanCode - Sum count of all plan codes- Issues to be fixed on this field. 
    def sum_plan_codes(self, data_frame):
        self.logger.debug('Executing Metric sum_plan_codes()')
        pass
    
    # ContractNumber - Count on contract number 
    def count_contract_number(self, data_frame):
        self.logger.debug('Executing Metric count_contract_number()')
        pass
    
    # Gender Metrics
    # From Jira: DDEV-39349
    #1)	Group by the following fields Gender, ContractIssueAge, ContractIssueYear and then Sum the following fields for these groups
    # ContractNumber - Count on contract number 
    # ContractCashValueAmount - Sum total of values
    # ContractSurrenderValueAmount - Sum total of values
    # Do the above for the compare between Consumption view and the Inforce File 
    def gender_metrics(self, data_frame, gender):
        self.logger.debug('Executing Metric gender_metrics()')
        
        try:
            # Groub by gender
            if gender == 'M':
                typeData = data_frame[(data_frame.ck_sex == 'M')]
            elif gender == 'F':
                typeData = data_frame[(data_frame.ck_sex == 'F')]
            elif gender == 'E':
                typeData = data_frame[(data_frame.ck_sex == 'E')]
            else:
                self.logger.warning('WARNING: Invalid gender type present in metrics call gender_metrics(), Type: ' + gender )
            
            result = {'policyCount' : 0,
               'contract_cash_value_sum': 0,
               'contract_surrender_value_amount_sum': 0
               }
               
             # Get the policy count (ContractNumber -> policynumber)
            result['policyCount'] = typeData['policynumber'].count()  
    
            # Get the Sum of the account values (ContractCashValueAmount -> i_accountvalue)
            result['contract_cash_value_sum'] = typeData['i_acctvalue'].sum()        
            
            # Get the sum of the guarentee Values (ContractSurrenderValueAmount -> i_cashvalue)             
            result['contract_surrender_value_amount'] = typeData['i_cashvalue'].sum()   
            
            # Report metrics in result file
            file = open(self.metricsFilePath, 'a')
    
            file.write('\n')
            file.write('--- Sum By Gender: ' + gender + '\n')
            file.write('Policy Count: ' +  str(result['policyCount']) + '\n')
            file.write('Contract Cash Value: ' +  str(result['contract_cash_value_sum']) + '\n')
            file.write('Contract Surrender Value Amount: ' +  str(result['contract_surrender_value_amount']) + '\n')
            file.close()
            
            return result
            
        except Exception as e:
            self.logger.error('ERROR: Failed to locate Gender: ' + gender )
            self.logger.error(e)
            pass
             
    # ContractCashValueAmount - Sum total of values
     
    # ContractSurrenderValueAmount - Sum total of values
     
    # ContractIssueAge - Sum of contract issue ages
     
    # ContractIssueYear - Sum of the values
     
    # Get rider type metrics
    # From Jira: DDEV-39349
    # Group by the following fields ContractOptionTypeGroup and then sum the following fields for these groups
    def sum_by_rider_type(self, data_frame, rider_type):
        self.logger.debug('Executing Metric sum_by_rider_type()')
        
        try:
            if rider_type == 'GMIB':
                typeData = data_frame[(data_frame.GMIBContractOptionTypeGroup == 'GMIB')]
            elif rider_type == 'GMDB':
                typeData = data_frame[(data_frame.GMDBContractOptionTypeGroup == 'GMDB')]
            elif rider_type == 'GMWB':
                typeData = data_frame[(data_frame.GMWBContractOptionTypeGroup == 'GMWB')]
            elif rider_type == 'GMLB':
                typeData = data_frame[(data_frame.GMLBContractOptionTypeGroup == 'GMLB')]
            elif rider_type == 'TXRP':
                typeData = data_frame[(data_frame.TXPRContractOptionTypeGroup == 'TXRP')]
            else:
                self.logger.warning('WARNING: Invalid rider type present in metrics call sum_by_rider_type(), Type: ' + rider_type )

            result = {'policyCount' : 0,
                       'contract_cash_value_sum': 0,
                       'contract_option_guaranteed_future_value_sum': 0
                       }
                       
            # Get the policy count (ContractNumber -> policynumber)
            result['policyCount'] = typeData['policynumber'].count()  
    
            # Get the Sum of the account values (ContractCashValueAmount -> i_accountvalue)
            result['contract_cash_value_sum'] = typeData['i_acctvalue'].sum()        
            
            # Get the sum of the guarentee Values (ContractOptionGuaranteedFutureValue -> gmab_gfv)             
            result['contract_option_guaranteed_future_value_sum'] = typeData['gmab_gfv'].sum()   
            
            # Report metrics in result file
            file = open(self.metricsFilePath, 'a')
    
            file.write('\n')
            file.write('--- Sum By Rider Type: ' + rider_type + '\n')
            file.write('Policy Count: ' +  str(result['policyCount']) + '\n')
            file.write('Contract Cash Value: ' +  str(result['contract_cash_value_sum']) + '\n')
            file.write('Contract Option Guaranteed Future Value: ' +  str(result['contract_option_guaranteed_future_value_sum']) + '\n')
    
            file.close()   
            
            return result 
            
        except Exception as e:
            self.logger.error('ERROR: Failed to locate elements of the ryder type: ' + rider_type )
            self.logger.error(e)
            pass
    
    # Validate that the metrics data sets are identical, otherwise return TRUE inside the return 
    # list
    def metrics_validation(self, pre_transform_metrics, post_transform_metrics):
        self.logger.debug('Executing Metric metrics_validation()')

        ret_val = []
        
        for i in range(len(pre_transform_metrics)):
            
            if(pre_transform_metrics[i] != post_transform_metrics[i]):
                self.logger.debug('WARNING: metrics result compare missmatch')
                self.logger.debug('Pre Transform: ')
                self.logger.debug(pre_transform_metrics[i])
                self.logger.debug('Post Transform: ')
                self.logger.debug(post_transform_metrics[i])
                # If one element is true, notification email will be sent
                ret_val.append(True)
            else:
                # If all elements are false, notification email will NOT be sent
                ret_val.append(False)
          
        return ret_val

    # Function which executes all the desired metrics on the input data set
    # and returns the result
    def execute_metrics_suite(self, data_frame, metrics_obj):
        self.logger.debug('Executing Metric execute_metrics_suite()')
        
        test_result = []
        
        # Execute rider type sum by type
        test_result.append(metrics_obj.sum_by_rider_type( data_frame, 'GMIB'))
        test_result.append(metrics_obj.sum_by_rider_type( data_frame, 'GMDB'))
        test_result.append(metrics_obj.sum_by_rider_type( data_frame, 'GMWB'))
        test_result.append(metrics_obj.sum_by_rider_type( data_frame, 'GMLB'))
        test_result.append(metrics_obj.sum_by_rider_type( data_frame, 'TXRP'))
        
        # Execute Gender Metrics
        test_result.append(metrics_obj.gender_metrics( data_frame, 'M'))
        test_result.append(metrics_obj.gender_metrics( data_frame, 'F'))
        test_result.append(metrics_obj.gender_metrics( data_frame, 'E'))
        
        # Execute Plan Code Sum Metrics
        # = metrics_obj.sum_plan_codes(data_frame)
        
        return test_result
     
    
