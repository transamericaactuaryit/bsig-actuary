# -*- coding: utf-8 -*-
###############################################
# config_file.py:
#
# Configuration file for BSIG infoce creation application contains
# static configuration information specific to the deployment
# enviornment.

import os
import sys
import inspect
sys.path.insert(0, '.')

path = os.path.dirname(os.path.abspath(inspect.getframeinfo(inspect.currentframe()).filename))

#Consumption Database to use
spark_database = "financebusinessviewsmodel"
#File used as a key/value pair for converting plan codes for alfa
plan_code_file = path + os.sep + 'plan_code_file.xlsx'
#File used for converting legal entity numbers
legal_entity_num_file = path + os.sep + 'legal_entity.xlsx'
#File used for stepup corrections
stepup_corrections_file = path + os.sep + 'stepup_corrections.xlsx'
#File used for fund fees
fund_fees_file = path + os.sep + 'fund_fees.xlsx'
#Path to Configuration Directory
alfaConfigPath = path + os.sep + 'AlfaConfig' + os.sep
#Path to Query Library
alfaQueryLibraryPath = path + os.sep + 'QueryLibrary' + os.sep
#Path to Input Directory 
alfaInputPath = path + os.sep + '1MdlcInput' + os.sep
#Path to Local Output Directory 
alfaOutputPath = path + os.sep + 'Output' + os.sep 
#Path to Remote Directory
alfaRemoteDir = r"//us.aegon.com/ait/Actuary/TCMActuary/Valtest/Inforce_File"
#Path to Metrics Directory
alfaMetricsPath = path + os.sep + 'Metrics' + os.sep
#Path to Logging Directory
alfaLoggingPath = path + os.sep + 'Logging' + os.sep
#Logging File
alfaLoggingFile = path + os.sep + "Logging" + os.sep + "alfaLogFile.log"
#Inforce File List Name
inforceListFileName = 'InforceFileList.json'

        

