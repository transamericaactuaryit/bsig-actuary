###############################################
# BsigMapping.py:
# 
# The Transform/Mapping class will handle the tools needed to update 
# and transform data objects  
# 
#
from abc import ABC, abstractmethod

class BsigMapping(ABC):
    
    def __init__(self):
        super().__init__()
        
    @abstractmethod
    def map_data(self, sourceObj):
        pass
