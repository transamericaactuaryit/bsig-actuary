import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Database.FileFormat;
import com.healthmarketscience.jackcess.DatabaseBuilder;

public class JackcessLibrary {

    public static void main(String[] args) {
        String dbFile = "/mnt/BSIGDrop/test/accessDB.mdb";
        Path dbPath = Paths.get(dbFile);
        // using try-with-resources is recommended to ensure that 
        //   the Database object will be closed properly
        
        if (Files.notExists(dbPath, LinkOption.NOFOLLOW_LINKS)) {
            try (Database db = DatabaseBuilder.create(FileFormat.V2010, new File(dbFile))) {
                System.out.println("The database file has been created.");
            } catch (IOException ioe) {
                ioe.printStackTrace(System.err);
            }
        } else {
            System.out.println("The database file already exists.");
        }
    }
}
